> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

<!--
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
-->

# Light Node Servive

The Light Node Service provides an interface for managing the Authorization Module of the Digital Folder.
The Light Node Service uses Nestjs as a framework and therefore requires Typescript and npm.

## Documentation

Please take a look at the documentation in the root directory of this Projekt

## Licenses

For information about licenses of third-party dependencies, please refer to the `LICENSE` files of the corresponding components.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
## License

Nest is [MIT licensed](LICENSE).
