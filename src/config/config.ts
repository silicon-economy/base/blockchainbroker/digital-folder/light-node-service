// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

export const ADDRESS_PREFIX = process.env.ADDRESS_PREFIX || "cosmos";
export const MNEMONIC = process.env.MNEMONIC || "";
export const BLOCKCHAIN_RPC_ENDPOINT = process.env.API_BLOCKCHAIN_RPC || "http://0.0.0.0:26657";

export const DEFAULT_FEE = {
  amount: [
    {
      denom: "token",
      amount: "0"
    }
  ],
  gas: "180000"
};

export enum Queues {
  TENDERMINT_SERVICE = "tendermintService",
}

export default () => ({
  queuePrefix: process.env.DEPLOYMENT_ENVIRONMENT || "",
  amqpUrl: process.env.AMQP_URL || "amqp://guest:guest@0.0.0.0:5672"
});
