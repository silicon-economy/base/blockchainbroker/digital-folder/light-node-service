// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

export interface TokenHistoryGlobal {
  creator: string;
  id: string;
  walletId: string[];
}

const baseTokenHistoryGlobal: object = { creator: "", id: "", walletId: "" };

export const TokenHistoryGlobal = {
  encode(
    message: TokenHistoryGlobal,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.walletId) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): TokenHistoryGlobal {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseTokenHistoryGlobal } as TokenHistoryGlobal;
    message.walletId = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.walletId.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TokenHistoryGlobal {
    const message = { ...baseTokenHistoryGlobal } as TokenHistoryGlobal;
    message.walletId = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      for (const e of object.walletId) {
        message.walletId.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: TokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.walletId) {
      obj.walletId = message.walletId.map((e) => e);
    } else {
      obj.walletId = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<TokenHistoryGlobal>): TokenHistoryGlobal {
    const message = { ...baseTokenHistoryGlobal } as TokenHistoryGlobal;
    message.walletId = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      for (const e of object.walletId) {
        message.walletId.push(e);
      }
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
