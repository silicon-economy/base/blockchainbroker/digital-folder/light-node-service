// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";
import { PageRequest, PageResponse } from "../cosmos/pagination";
import { Segment } from "./segment";
import { SegmentHistory } from "./segmentHistory";
import { TokenHistoryGlobal } from "./tokenHistoryGlobal";
import { Wallet } from "./wallet";
import { WalletHistory } from "./walletHistory";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgRemoveTokenRefFromSegment {
  creator: string;
  tokenRefId: string;
  segmentId: string;
}

export interface MsgMoveTokenToSegment {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}

export interface MsgMoveTokenToWallet {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetWalletId: string;
}

export interface MsgCreateTokenHistoryGlobal {
  creator: string;
  tokenId: string;
  walletIds: string[];
}

export interface MsgUpdateTokenHistoryGlobal {
  creator: string;
  id: string;
}

export interface MsgCreateSegmentHistory {
  creator: string;
  id: string;
  history: Segment[];
}

export interface MsgUpdateSegmentHistory {
  creator: string;
  id: string;
}

export interface MsgCreateWalletHistory {
  creator: string;
  id: string;
  history: Wallet[];
}

export interface MsgUpdateWalletHistory {
  creator: string;
  id: string;
}

export interface MsgCreateSegment {
  creator: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgCreateSegmentWithId {
  creator: string;
  Id: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgUpdateSegment {
  creator: string;
  id: string;
  name: string;
  info: string;
}

export interface MsgCreateWallet {
  creator: string;
  name: string;
}

export interface MsgCreateWalletWithId {
  creator: string;
  Id: string;
  name: string;
}

export interface MsgAssignCosmosAddressToWallet {
  creator: string;
  cosmosAddress: string;
  walletId: string;
}

export interface MsgUpdateWallet {
  creator: string;
  id: string;
  name: string;
}

export interface MsgCreateTokenRef {
  creator: string;
  id: string;
  moduleRef: string;
  segmentId: string;
}

export interface MsgEmptyResponse {
}

export interface MsgIdResponse {
  id: string;
}

/** this line is used by starport scaffolding # 3 */
export interface MsgFetchGetTokenHistoryGlobal {
  creator: string;
  id: string;
}

export interface MsgFetchGetTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal | undefined;
}

export interface MsgFetchAllTokenHistoryGlobal {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetSegmentHistory {
  creator: string;
  id: string;
}

export interface MsgFetchGetSegmentHistoryResponse {
  SegmentHistory: SegmentHistory | undefined;
}

export interface MsgFetchAllSegmentHistory {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllSegmentHistoryResponse {
  SegmentHistory: SegmentHistory[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetSegment {
  creator: string;
  id: string;
}

export interface MsgFetchGetSegmentResponse {
  Segment: Segment | undefined;
}

export interface MsgFetchAllSegment {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllSegmentResponse {
  Segment: Segment[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetWalletHistory {
  creator: string;
  id: string;
}

export interface MsgFetchGetWalletHistoryResponse {
  WalletHistory: WalletHistory | undefined;
}

export interface MsgFetchAllWalletHistory {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllWalletHistoryResponse {
  WalletHistory: WalletHistory[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetWallet {
  creator: string;
  id: string;
}

export interface MsgFetchGetWalletResponse {
  Wallet: Wallet | undefined;
}

export interface MsgFetchAllWallet {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllWalletResponse {
  Wallet: Wallet[];
  pagination: PageResponse | undefined;
}

const baseMsgRemoveTokenRefFromSegment: object = {
  creator: "",
  tokenRefId: "",
  segmentId: ""
};

export const MsgRemoveTokenRefFromSegment = {
  encode(
    message: MsgRemoveTokenRefFromSegment,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.segmentId !== "") {
      writer.uint32(26).string(message.segmentId);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgRemoveTokenRefFromSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgRemoveTokenRefFromSegment
    } as MsgRemoveTokenRefFromSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRemoveTokenRefFromSegment {
    const message = {
      ...baseMsgRemoveTokenRefFromSegment
    } as MsgRemoveTokenRefFromSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = String(object.tokenRefId);
    } else {
      message.tokenRefId = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    return message;
  },

  toJSON(message: MsgRemoveTokenRefFromSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgRemoveTokenRefFromSegment>
  ): MsgRemoveTokenRefFromSegment {
    const message = {
      ...baseMsgRemoveTokenRefFromSegment
    } as MsgRemoveTokenRefFromSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = object.tokenRefId;
    } else {
      message.tokenRefId = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    return message;
  }
};

const baseMsgMoveTokenToSegment: object = {
  creator: "",
  tokenRefId: "",
  sourceSegmentId: "",
  targetSegmentId: ""
};

export const MsgMoveTokenToSegment = {
  encode(
    message: MsgMoveTokenToSegment,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetSegmentId !== "") {
      writer.uint32(34).string(message.targetSegmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgMoveTokenToSegment } as MsgMoveTokenToSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetSegmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToSegment {
    const message = { ...baseMsgMoveTokenToSegment } as MsgMoveTokenToSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = String(object.tokenRefId);
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = String(object.sourceSegmentId);
    } else {
      message.sourceSegmentId = "";
    }
    if (
      object.targetSegmentId !== undefined &&
      object.targetSegmentId !== null
    ) {
      message.targetSegmentId = String(object.targetSegmentId);
    } else {
      message.targetSegmentId = "";
    }
    return message;
  },

  toJSON(message: MsgMoveTokenToSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined &&
    (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetSegmentId !== undefined &&
    (obj.targetSegmentId = message.targetSegmentId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgMoveTokenToSegment>
  ): MsgMoveTokenToSegment {
    const message = { ...baseMsgMoveTokenToSegment } as MsgMoveTokenToSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = object.tokenRefId;
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = object.sourceSegmentId;
    } else {
      message.sourceSegmentId = "";
    }
    if (
      object.targetSegmentId !== undefined &&
      object.targetSegmentId !== null
    ) {
      message.targetSegmentId = object.targetSegmentId;
    } else {
      message.targetSegmentId = "";
    }
    return message;
  }
};

const baseMsgMoveTokenToWallet: object = {
  creator: "",
  tokenRefId: "",
  sourceSegmentId: "",
  targetWalletId: ""
};

export const MsgMoveTokenToWallet = {
  encode(
    message: MsgMoveTokenToWallet,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetWalletId !== "") {
      writer.uint32(34).string(message.targetWalletId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgMoveTokenToWallet } as MsgMoveTokenToWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToWallet {
    const message = { ...baseMsgMoveTokenToWallet } as MsgMoveTokenToWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = String(object.tokenRefId);
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = String(object.sourceSegmentId);
    } else {
      message.sourceSegmentId = "";
    }
    if (object.targetWalletId !== undefined && object.targetWalletId !== null) {
      message.targetWalletId = String(object.targetWalletId);
    } else {
      message.targetWalletId = "";
    }
    return message;
  },

  toJSON(message: MsgMoveTokenToWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined &&
    (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetWalletId !== undefined &&
    (obj.targetWalletId = message.targetWalletId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgMoveTokenToWallet>): MsgMoveTokenToWallet {
    const message = { ...baseMsgMoveTokenToWallet } as MsgMoveTokenToWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = object.tokenRefId;
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = object.sourceSegmentId;
    } else {
      message.sourceSegmentId = "";
    }
    if (object.targetWalletId !== undefined && object.targetWalletId !== null) {
      message.targetWalletId = object.targetWalletId;
    } else {
      message.targetWalletId = "";
    }
    return message;
  }
};

const baseMsgCreateTokenHistoryGlobal: object = {
  creator: "",
  tokenId: "",
  walletIds: ""
};

export const MsgCreateTokenHistoryGlobal = {
  encode(
    message: MsgCreateTokenHistoryGlobal,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenId !== "") {
      writer.uint32(18).string(message.tokenId);
    }
    for (const v of message.walletIds) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateTokenHistoryGlobal {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateTokenHistoryGlobal
    } as MsgCreateTokenHistoryGlobal;
    message.walletIds = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenId = reader.string();
          break;
        case 3:
          message.walletIds.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenHistoryGlobal {
    const message = {
      ...baseMsgCreateTokenHistoryGlobal
    } as MsgCreateTokenHistoryGlobal;
    message.walletIds = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = String(object.tokenId);
    } else {
      message.tokenId = "";
    }
    if (object.walletIds !== undefined && object.walletIds !== null) {
      for (const e of object.walletIds) {
        message.walletIds.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: MsgCreateTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    if (message.walletIds) {
      obj.walletIds = message.walletIds.map((e) => e);
    } else {
      obj.walletIds = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateTokenHistoryGlobal>
  ): MsgCreateTokenHistoryGlobal {
    const message = {
      ...baseMsgCreateTokenHistoryGlobal
    } as MsgCreateTokenHistoryGlobal;
    message.walletIds = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = object.tokenId;
    } else {
      message.tokenId = "";
    }
    if (object.walletIds !== undefined && object.walletIds !== null) {
      for (const e of object.walletIds) {
        message.walletIds.push(e);
      }
    }
    return message;
  }
};

const baseMsgUpdateTokenHistoryGlobal: object = { creator: "", id: "" };

export const MsgUpdateTokenHistoryGlobal = {
  encode(
    message: MsgUpdateTokenHistoryGlobal,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateTokenHistoryGlobal {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateTokenHistoryGlobal
    } as MsgUpdateTokenHistoryGlobal;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenHistoryGlobal {
    const message = {
      ...baseMsgUpdateTokenHistoryGlobal
    } as MsgUpdateTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateTokenHistoryGlobal>
  ): MsgUpdateTokenHistoryGlobal {
    const message = {
      ...baseMsgUpdateTokenHistoryGlobal
    } as MsgUpdateTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateSegmentHistory: object = { creator: "", id: "" };

export const MsgCreateSegmentHistory = {
  encode(
    message: MsgCreateSegmentHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Segment.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegmentHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateSegmentHistory
    } as MsgCreateSegmentHistory;
    message.history = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Segment.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegmentHistory {
    const message = {
      ...baseMsgCreateSegmentHistory
    } as MsgCreateSegmentHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Segment.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgCreateSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) =>
        e ? Segment.toJSON(e) : undefined
      );
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateSegmentHistory>
  ): MsgCreateSegmentHistory {
    const message = {
      ...baseMsgCreateSegmentHistory
    } as MsgCreateSegmentHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Segment.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgUpdateSegmentHistory: object = { creator: "", id: "" };

export const MsgUpdateSegmentHistory = {
  encode(
    message: MsgUpdateSegmentHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateSegmentHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateSegmentHistory
    } as MsgUpdateSegmentHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateSegmentHistory {
    const message = {
      ...baseMsgUpdateSegmentHistory
    } as MsgUpdateSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateSegmentHistory>
  ): MsgUpdateSegmentHistory {
    const message = {
      ...baseMsgUpdateSegmentHistory
    } as MsgUpdateSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateWalletHistory: object = { creator: "", id: "" };

export const MsgCreateWalletHistory = {
  encode(
    message: MsgCreateWalletHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Wallet.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateWalletHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateWalletHistory } as MsgCreateWalletHistory;
    message.history = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Wallet.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWalletHistory {
    const message = { ...baseMsgCreateWalletHistory } as MsgCreateWalletHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Wallet.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgCreateWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) =>
        e ? Wallet.toJSON(e) : undefined
      );
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateWalletHistory>
  ): MsgCreateWalletHistory {
    const message = { ...baseMsgCreateWalletHistory } as MsgCreateWalletHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Wallet.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgUpdateWalletHistory: object = { creator: "", id: "" };

export const MsgUpdateWalletHistory = {
  encode(
    message: MsgUpdateWalletHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateWalletHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateWalletHistory } as MsgUpdateWalletHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateWalletHistory {
    const message = { ...baseMsgUpdateWalletHistory } as MsgUpdateWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateWalletHistory>
  ): MsgUpdateWalletHistory {
    const message = { ...baseMsgUpdateWalletHistory } as MsgUpdateWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateSegment: object = {
  creator: "",
  name: "",
  info: "",
  walletId: ""
};

export const MsgCreateSegment = {
  encode(message: MsgCreateSegment, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(26).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(34).string(message.walletId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateSegment } as MsgCreateSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.info = reader.string();
          break;
        case 4:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegment {
    const message = { ...baseMsgCreateSegment } as MsgCreateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = String(object.info);
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = String(object.walletId);
    } else {
      message.walletId = "";
    }
    return message;
  },

  toJSON(message: MsgCreateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateSegment>): MsgCreateSegment {
    const message = { ...baseMsgCreateSegment } as MsgCreateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = object.info;
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = object.walletId;
    } else {
      message.walletId = "";
    }
    return message;
  }
};

const baseMsgCreateSegmentWithId: object = {
  creator: "",
  Id: "",
  name: "",
  info: "",
  walletId: ""
};

export const MsgCreateSegmentWithId = {
  encode(
    message: MsgCreateSegmentWithId,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.Id !== "") {
      writer.uint32(18).string(message.Id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(42).string(message.walletId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegmentWithId {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateSegmentWithId } as MsgCreateSegmentWithId;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.Id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        case 5:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegmentWithId {
    const message = { ...baseMsgCreateSegmentWithId } as MsgCreateSegmentWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.Id !== undefined && object.Id !== null) {
      message.Id = String(object.Id);
    } else {
      message.Id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = String(object.info);
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = String(object.walletId);
    } else {
      message.walletId = "";
    }
    return message;
  },

  toJSON(message: MsgCreateSegmentWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.Id !== undefined && (obj.Id = message.Id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateSegmentWithId>
  ): MsgCreateSegmentWithId {
    const message = { ...baseMsgCreateSegmentWithId } as MsgCreateSegmentWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.Id !== undefined && object.Id !== null) {
      message.Id = object.Id;
    } else {
      message.Id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = object.info;
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = object.walletId;
    } else {
      message.walletId = "";
    }
    return message;
  }
};

const baseMsgUpdateSegment: object = {
  creator: "",
  id: "",
  name: "",
  info: ""
};

export const MsgUpdateSegment = {
  encode(message: MsgUpdateSegment, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateSegment } as MsgUpdateSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateSegment {
    const message = { ...baseMsgUpdateSegment } as MsgUpdateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = String(object.info);
    } else {
      message.info = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateSegment>): MsgUpdateSegment {
    const message = { ...baseMsgUpdateSegment } as MsgUpdateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = object.info;
    } else {
      message.info = "";
    }
    return message;
  }
};

const baseMsgCreateWallet: object = { creator: "", name: "" };

export const MsgCreateWallet = {
  encode(message: MsgCreateWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateWallet } as MsgCreateWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWallet {
    const message = { ...baseMsgCreateWallet } as MsgCreateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    return message;
  },

  toJSON(message: MsgCreateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateWallet>): MsgCreateWallet {
    const message = { ...baseMsgCreateWallet } as MsgCreateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    return message;
  }
};

const baseMsgCreateWalletWithId: object = { creator: "", Id: "", name: "" };

export const MsgCreateWalletWithId = {
  encode(
    message: MsgCreateWalletWithId,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.Id !== "") {
      writer.uint32(18).string(message.Id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateWalletWithId {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateWalletWithId } as MsgCreateWalletWithId;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.Id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWalletWithId {
    const message = { ...baseMsgCreateWalletWithId } as MsgCreateWalletWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.Id !== undefined && object.Id !== null) {
      message.Id = String(object.Id);
    } else {
      message.Id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    return message;
  },

  toJSON(message: MsgCreateWalletWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.Id !== undefined && (obj.Id = message.Id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateWalletWithId>
  ): MsgCreateWalletWithId {
    const message = { ...baseMsgCreateWalletWithId } as MsgCreateWalletWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.Id !== undefined && object.Id !== null) {
      message.Id = object.Id;
    } else {
      message.Id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    return message;
  }
};

const baseMsgAssignCosmosAddressToWallet: object = {
  creator: "",
  cosmosAddress: "",
  walletId: ""
};

export const MsgAssignCosmosAddressToWallet = {
  encode(
    message: MsgAssignCosmosAddressToWallet,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.cosmosAddress !== "") {
      writer.uint32(18).string(message.cosmosAddress);
    }
    if (message.walletId !== "") {
      writer.uint32(26).string(message.walletId);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgAssignCosmosAddressToWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAssignCosmosAddressToWallet
    } as MsgAssignCosmosAddressToWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.cosmosAddress = reader.string();
          break;
        case 3:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAssignCosmosAddressToWallet {
    const message = {
      ...baseMsgAssignCosmosAddressToWallet
    } as MsgAssignCosmosAddressToWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.cosmosAddress !== undefined && object.cosmosAddress !== null) {
      message.cosmosAddress = String(object.cosmosAddress);
    } else {
      message.cosmosAddress = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = String(object.walletId);
    } else {
      message.walletId = "";
    }
    return message;
  },

  toJSON(message: MsgAssignCosmosAddressToWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.cosmosAddress !== undefined &&
    (obj.cosmosAddress = message.cosmosAddress);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgAssignCosmosAddressToWallet>
  ): MsgAssignCosmosAddressToWallet {
    const message = {
      ...baseMsgAssignCosmosAddressToWallet
    } as MsgAssignCosmosAddressToWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.cosmosAddress !== undefined && object.cosmosAddress !== null) {
      message.cosmosAddress = object.cosmosAddress;
    } else {
      message.cosmosAddress = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = object.walletId;
    } else {
      message.walletId = "";
    }
    return message;
  }
};

const baseMsgUpdateWallet: object = { creator: "", id: "", name: "" };

export const MsgUpdateWallet = {
  encode(message: MsgUpdateWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateWallet } as MsgUpdateWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateWallet {
    const message = { ...baseMsgUpdateWallet } as MsgUpdateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateWallet>): MsgUpdateWallet {
    const message = { ...baseMsgUpdateWallet } as MsgUpdateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    return message;
  }
};

const baseMsgCreateTokenRef: object = {
  creator: "",
  id: "",
  moduleRef: "",
  segmentId: ""
};

export const MsgCreateTokenRef = {
  encode(message: MsgCreateTokenRef, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.moduleRef !== "") {
      writer.uint32(26).string(message.moduleRef);
    }
    if (message.segmentId !== "") {
      writer.uint32(34).string(message.segmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateTokenRef {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateTokenRef } as MsgCreateTokenRef;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.moduleRef = reader.string();
          break;
        case 4:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenRef {
    const message = { ...baseMsgCreateTokenRef } as MsgCreateTokenRef;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = String(object.moduleRef);
    } else {
      message.moduleRef = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    return message;
  },

  toJSON(message: MsgCreateTokenRef): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateTokenRef>): MsgCreateTokenRef {
    const message = { ...baseMsgCreateTokenRef } as MsgCreateTokenRef;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = object.moduleRef;
    } else {
      message.moduleRef = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    return message;
  }
};

const baseMsgEmptyResponse: object = {};

export const MsgEmptyResponse = {
  encode(_: MsgEmptyResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgEmptyResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgEmptyResponse {
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    return message;
  },

  toJSON(_: MsgEmptyResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgEmptyResponse>): MsgEmptyResponse {
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    return message;
  }
};

const baseMsgIdResponse: object = { id: "" };

export const MsgIdResponse = {
  encode(message: MsgIdResponse, writer: Writer = Writer.create()): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgIdResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgIdResponse {
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgIdResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgIdResponse>): MsgIdResponse {
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchGetTokenHistoryGlobal: object = { creator: "", id: "" };

export const MsgFetchGetTokenHistoryGlobal = {
  encode(
    message: MsgFetchGetTokenHistoryGlobal,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetTokenHistoryGlobal {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobal
    } as MsgFetchGetTokenHistoryGlobal;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobal
    } as MsgFetchGetTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetTokenHistoryGlobal>
  ): MsgFetchGetTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobal
    } as MsgFetchGetTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchGetTokenHistoryGlobalResponse: object = {};

export const MsgFetchGetTokenHistoryGlobalResponse = {
  encode(
    message: MsgFetchGetTokenHistoryGlobalResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.TokenHistoryGlobal !== undefined) {
      TokenHistoryGlobal.encode(
        message.TokenHistoryGlobal,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetTokenHistoryGlobalResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobalResponse
    } as MsgFetchGetTokenHistoryGlobalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistoryGlobal = TokenHistoryGlobal.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetTokenHistoryGlobalResponse {
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobalResponse
    } as MsgFetchGetTokenHistoryGlobalResponse;
    if (
      object.TokenHistoryGlobal !== undefined &&
      object.TokenHistoryGlobal !== null
    ) {
      message.TokenHistoryGlobal = TokenHistoryGlobal.fromJSON(
        object.TokenHistoryGlobal
      );
    } else {
      message.TokenHistoryGlobal = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchGetTokenHistoryGlobalResponse): unknown {
    const obj: any = {};
    message.TokenHistoryGlobal !== undefined &&
    (obj.TokenHistoryGlobal = message.TokenHistoryGlobal
      ? TokenHistoryGlobal.toJSON(message.TokenHistoryGlobal)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetTokenHistoryGlobalResponse>
  ): MsgFetchGetTokenHistoryGlobalResponse {
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobalResponse
    } as MsgFetchGetTokenHistoryGlobalResponse;
    if (
      object.TokenHistoryGlobal !== undefined &&
      object.TokenHistoryGlobal !== null
    ) {
      message.TokenHistoryGlobal = TokenHistoryGlobal.fromPartial(
        object.TokenHistoryGlobal
      );
    } else {
      message.TokenHistoryGlobal = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllTokenHistoryGlobal: object = { creator: "" };

export const MsgFetchAllTokenHistoryGlobal = {
  encode(
    message: MsgFetchAllTokenHistoryGlobal,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenHistoryGlobal {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobal
    } as MsgFetchAllTokenHistoryGlobal;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobal
    } as MsgFetchAllTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistoryGlobal>
  ): MsgFetchAllTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobal
    } as MsgFetchAllTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllTokenHistoryGlobalResponse: object = {};

export const MsgFetchAllTokenHistoryGlobalResponse = {
  encode(
    message: MsgFetchAllTokenHistoryGlobalResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.TokenHistoryGlobal) {
      TokenHistoryGlobal.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenHistoryGlobalResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobalResponse
    } as MsgFetchAllTokenHistoryGlobalResponse;
    message.TokenHistoryGlobal = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistoryGlobal.push(
            TokenHistoryGlobal.decode(reader, reader.uint32())
          );
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryGlobalResponse {
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobalResponse
    } as MsgFetchAllTokenHistoryGlobalResponse;
    message.TokenHistoryGlobal = [];
    if (
      object.TokenHistoryGlobal !== undefined &&
      object.TokenHistoryGlobal !== null
    ) {
      for (const e of object.TokenHistoryGlobal) {
        message.TokenHistoryGlobal.push(TokenHistoryGlobal.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllTokenHistoryGlobalResponse): unknown {
    const obj: any = {};
    if (message.TokenHistoryGlobal) {
      obj.TokenHistoryGlobal = message.TokenHistoryGlobal.map((e) =>
        e ? TokenHistoryGlobal.toJSON(e) : undefined
      );
    } else {
      obj.TokenHistoryGlobal = [];
    }
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageResponse.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistoryGlobalResponse>
  ): MsgFetchAllTokenHistoryGlobalResponse {
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobalResponse
    } as MsgFetchAllTokenHistoryGlobalResponse;
    message.TokenHistoryGlobal = [];
    if (
      object.TokenHistoryGlobal !== undefined &&
      object.TokenHistoryGlobal !== null
    ) {
      for (const e of object.TokenHistoryGlobal) {
        message.TokenHistoryGlobal.push(TokenHistoryGlobal.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchGetSegmentHistory: object = { creator: "", id: "" };

export const MsgFetchGetSegmentHistory = {
  encode(
    message: MsgFetchGetSegmentHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetSegmentHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetSegmentHistory
    } as MsgFetchGetSegmentHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegmentHistory {
    const message = {
      ...baseMsgFetchGetSegmentHistory
    } as MsgFetchGetSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetSegmentHistory>
  ): MsgFetchGetSegmentHistory {
    const message = {
      ...baseMsgFetchGetSegmentHistory
    } as MsgFetchGetSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchGetSegmentHistoryResponse: object = {};

export const MsgFetchGetSegmentHistoryResponse = {
  encode(
    message: MsgFetchGetSegmentHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.SegmentHistory !== undefined) {
      SegmentHistory.encode(
        message.SegmentHistory,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetSegmentHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetSegmentHistoryResponse
    } as MsgFetchGetSegmentHistoryResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.SegmentHistory = SegmentHistory.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegmentHistoryResponse {
    const message = {
      ...baseMsgFetchGetSegmentHistoryResponse
    } as MsgFetchGetSegmentHistoryResponse;
    if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
      message.SegmentHistory = SegmentHistory.fromJSON(object.SegmentHistory);
    } else {
      message.SegmentHistory = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchGetSegmentHistoryResponse): unknown {
    const obj: any = {};
    message.SegmentHistory !== undefined &&
    (obj.SegmentHistory = message.SegmentHistory
      ? SegmentHistory.toJSON(message.SegmentHistory)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetSegmentHistoryResponse>
  ): MsgFetchGetSegmentHistoryResponse {
    const message = {
      ...baseMsgFetchGetSegmentHistoryResponse
    } as MsgFetchGetSegmentHistoryResponse;
    if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
      message.SegmentHistory = SegmentHistory.fromPartial(
        object.SegmentHistory
      );
    } else {
      message.SegmentHistory = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllSegmentHistory: object = { creator: "" };

export const MsgFetchAllSegmentHistory = {
  encode(
    message: MsgFetchAllSegmentHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllSegmentHistory
    } as MsgFetchAllSegmentHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentHistory {
    const message = {
      ...baseMsgFetchAllSegmentHistory
    } as MsgFetchAllSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentHistory>
  ): MsgFetchAllSegmentHistory {
    const message = {
      ...baseMsgFetchAllSegmentHistory
    } as MsgFetchAllSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllSegmentHistoryResponse: object = {};

export const MsgFetchAllSegmentHistoryResponse = {
  encode(
    message: MsgFetchAllSegmentHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.SegmentHistory) {
      SegmentHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllSegmentHistoryResponse
    } as MsgFetchAllSegmentHistoryResponse;
    message.SegmentHistory = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.SegmentHistory.push(
            SegmentHistory.decode(reader, reader.uint32())
          );
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentHistoryResponse {
    const message = {
      ...baseMsgFetchAllSegmentHistoryResponse
    } as MsgFetchAllSegmentHistoryResponse;
    message.SegmentHistory = [];
    if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
      for (const e of object.SegmentHistory) {
        message.SegmentHistory.push(SegmentHistory.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllSegmentHistoryResponse): unknown {
    const obj: any = {};
    if (message.SegmentHistory) {
      obj.SegmentHistory = message.SegmentHistory.map((e) =>
        e ? SegmentHistory.toJSON(e) : undefined
      );
    } else {
      obj.SegmentHistory = [];
    }
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageResponse.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentHistoryResponse>
  ): MsgFetchAllSegmentHistoryResponse {
    const message = {
      ...baseMsgFetchAllSegmentHistoryResponse
    } as MsgFetchAllSegmentHistoryResponse;
    message.SegmentHistory = [];
    if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
      for (const e of object.SegmentHistory) {
        message.SegmentHistory.push(SegmentHistory.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchGetSegment: object = { creator: "", id: "" };

export const MsgFetchGetSegment = {
  encode(
    message: MsgFetchGetSegment,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchGetSegment } as MsgFetchGetSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegment {
    const message = { ...baseMsgFetchGetSegment } as MsgFetchGetSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchGetSegment>): MsgFetchGetSegment {
    const message = { ...baseMsgFetchGetSegment } as MsgFetchGetSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchGetSegmentResponse: object = {};

export const MsgFetchGetSegmentResponse = {
  encode(
    message: MsgFetchGetSegmentResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.Segment !== undefined) {
      Segment.encode(message.Segment, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetSegmentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetSegmentResponse
    } as MsgFetchGetSegmentResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Segment = Segment.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegmentResponse {
    const message = {
      ...baseMsgFetchGetSegmentResponse
    } as MsgFetchGetSegmentResponse;
    if (object.Segment !== undefined && object.Segment !== null) {
      message.Segment = Segment.fromJSON(object.Segment);
    } else {
      message.Segment = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchGetSegmentResponse): unknown {
    const obj: any = {};
    message.Segment !== undefined &&
    (obj.Segment = message.Segment
      ? Segment.toJSON(message.Segment)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetSegmentResponse>
  ): MsgFetchGetSegmentResponse {
    const message = {
      ...baseMsgFetchGetSegmentResponse
    } as MsgFetchGetSegmentResponse;
    if (object.Segment !== undefined && object.Segment !== null) {
      message.Segment = Segment.fromPartial(object.Segment);
    } else {
      message.Segment = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllSegment: object = { creator: "" };

export const MsgFetchAllSegment = {
  encode(
    message: MsgFetchAllSegment,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchAllSegment } as MsgFetchAllSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegment {
    const message = { ...baseMsgFetchAllSegment } as MsgFetchAllSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchAllSegment>): MsgFetchAllSegment {
    const message = { ...baseMsgFetchAllSegment } as MsgFetchAllSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllSegmentResponse: object = {};

export const MsgFetchAllSegmentResponse = {
  encode(
    message: MsgFetchAllSegmentResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.Segment) {
      Segment.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllSegmentResponse
    } as MsgFetchAllSegmentResponse;
    message.Segment = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Segment.push(Segment.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentResponse {
    const message = {
      ...baseMsgFetchAllSegmentResponse
    } as MsgFetchAllSegmentResponse;
    message.Segment = [];
    if (object.Segment !== undefined && object.Segment !== null) {
      for (const e of object.Segment) {
        message.Segment.push(Segment.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllSegmentResponse): unknown {
    const obj: any = {};
    if (message.Segment) {
      obj.Segment = message.Segment.map((e) =>
        e ? Segment.toJSON(e) : undefined
      );
    } else {
      obj.Segment = [];
    }
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageResponse.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentResponse>
  ): MsgFetchAllSegmentResponse {
    const message = {
      ...baseMsgFetchAllSegmentResponse
    } as MsgFetchAllSegmentResponse;
    message.Segment = [];
    if (object.Segment !== undefined && object.Segment !== null) {
      for (const e of object.Segment) {
        message.Segment.push(Segment.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchGetWalletHistory: object = { creator: "", id: "" };

export const MsgFetchGetWalletHistory = {
  encode(
    message: MsgFetchGetWalletHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetWalletHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetWalletHistory
    } as MsgFetchGetWalletHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletHistory {
    const message = {
      ...baseMsgFetchGetWalletHistory
    } as MsgFetchGetWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetWalletHistory>
  ): MsgFetchGetWalletHistory {
    const message = {
      ...baseMsgFetchGetWalletHistory
    } as MsgFetchGetWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchGetWalletHistoryResponse: object = {};

export const MsgFetchGetWalletHistoryResponse = {
  encode(
    message: MsgFetchGetWalletHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.WalletHistory !== undefined) {
      WalletHistory.encode(
        message.WalletHistory,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetWalletHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetWalletHistoryResponse
    } as MsgFetchGetWalletHistoryResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.WalletHistory = WalletHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletHistoryResponse {
    const message = {
      ...baseMsgFetchGetWalletHistoryResponse
    } as MsgFetchGetWalletHistoryResponse;
    if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
      message.WalletHistory = WalletHistory.fromJSON(object.WalletHistory);
    } else {
      message.WalletHistory = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchGetWalletHistoryResponse): unknown {
    const obj: any = {};
    message.WalletHistory !== undefined &&
    (obj.WalletHistory = message.WalletHistory
      ? WalletHistory.toJSON(message.WalletHistory)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetWalletHistoryResponse>
  ): MsgFetchGetWalletHistoryResponse {
    const message = {
      ...baseMsgFetchGetWalletHistoryResponse
    } as MsgFetchGetWalletHistoryResponse;
    if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
      message.WalletHistory = WalletHistory.fromPartial(object.WalletHistory);
    } else {
      message.WalletHistory = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllWalletHistory: object = { creator: "" };

export const MsgFetchAllWalletHistory = {
  encode(
    message: MsgFetchAllWalletHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllWalletHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllWalletHistory
    } as MsgFetchAllWalletHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletHistory {
    const message = {
      ...baseMsgFetchAllWalletHistory
    } as MsgFetchAllWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllWalletHistory>
  ): MsgFetchAllWalletHistory {
    const message = {
      ...baseMsgFetchAllWalletHistory
    } as MsgFetchAllWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllWalletHistoryResponse: object = {};

export const MsgFetchAllWalletHistoryResponse = {
  encode(
    message: MsgFetchAllWalletHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.WalletHistory) {
      WalletHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllWalletHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllWalletHistoryResponse
    } as MsgFetchAllWalletHistoryResponse;
    message.WalletHistory = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.WalletHistory.push(
            WalletHistory.decode(reader, reader.uint32())
          );
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletHistoryResponse {
    const message = {
      ...baseMsgFetchAllWalletHistoryResponse
    } as MsgFetchAllWalletHistoryResponse;
    message.WalletHistory = [];
    if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
      for (const e of object.WalletHistory) {
        message.WalletHistory.push(WalletHistory.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllWalletHistoryResponse): unknown {
    const obj: any = {};
    if (message.WalletHistory) {
      obj.WalletHistory = message.WalletHistory.map((e) =>
        e ? WalletHistory.toJSON(e) : undefined
      );
    } else {
      obj.WalletHistory = [];
    }
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageResponse.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllWalletHistoryResponse>
  ): MsgFetchAllWalletHistoryResponse {
    const message = {
      ...baseMsgFetchAllWalletHistoryResponse
    } as MsgFetchAllWalletHistoryResponse;
    message.WalletHistory = [];
    if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
      for (const e of object.WalletHistory) {
        message.WalletHistory.push(WalletHistory.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchGetWallet: object = { creator: "", id: "" };

export const MsgFetchGetWallet = {
  encode(message: MsgFetchGetWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchGetWallet } as MsgFetchGetWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWallet {
    const message = { ...baseMsgFetchGetWallet } as MsgFetchGetWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchGetWallet>): MsgFetchGetWallet {
    const message = { ...baseMsgFetchGetWallet } as MsgFetchGetWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchGetWalletResponse: object = {};

export const MsgFetchGetWalletResponse = {
  encode(
    message: MsgFetchGetWalletResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.Wallet !== undefined) {
      Wallet.encode(message.Wallet, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetWalletResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetWalletResponse
    } as MsgFetchGetWalletResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Wallet = Wallet.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletResponse {
    const message = {
      ...baseMsgFetchGetWalletResponse
    } as MsgFetchGetWalletResponse;
    if (object.Wallet !== undefined && object.Wallet !== null) {
      message.Wallet = Wallet.fromJSON(object.Wallet);
    } else {
      message.Wallet = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchGetWalletResponse): unknown {
    const obj: any = {};
    message.Wallet !== undefined &&
    (obj.Wallet = message.Wallet ? Wallet.toJSON(message.Wallet) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetWalletResponse>
  ): MsgFetchGetWalletResponse {
    const message = {
      ...baseMsgFetchGetWalletResponse
    } as MsgFetchGetWalletResponse;
    if (object.Wallet !== undefined && object.Wallet !== null) {
      message.Wallet = Wallet.fromPartial(object.Wallet);
    } else {
      message.Wallet = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllWallet: object = { creator: "" };

export const MsgFetchAllWallet = {
  encode(message: MsgFetchAllWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchAllWallet } as MsgFetchAllWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWallet {
    const message = { ...baseMsgFetchAllWallet } as MsgFetchAllWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchAllWallet>): MsgFetchAllWallet {
    const message = { ...baseMsgFetchAllWallet } as MsgFetchAllWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllWalletResponse: object = {};

export const MsgFetchAllWalletResponse = {
  encode(
    message: MsgFetchAllWalletResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.Wallet) {
      Wallet.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllWalletResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllWalletResponse
    } as MsgFetchAllWalletResponse;
    message.Wallet = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Wallet.push(Wallet.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletResponse {
    const message = {
      ...baseMsgFetchAllWalletResponse
    } as MsgFetchAllWalletResponse;
    message.Wallet = [];
    if (object.Wallet !== undefined && object.Wallet !== null) {
      for (const e of object.Wallet) {
        message.Wallet.push(Wallet.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchAllWalletResponse): unknown {
    const obj: any = {};
    if (message.Wallet) {
      obj.Wallet = message.Wallet.map((e) =>
        e ? Wallet.toJSON(e) : undefined
      );
    } else {
      obj.Wallet = [];
    }
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageResponse.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllWalletResponse>
  ): MsgFetchAllWalletResponse {
    const message = {
      ...baseMsgFetchAllWalletResponse
    } as MsgFetchAllWalletResponse;
    message.Wallet = [];
    if (object.Wallet !== undefined && object.Wallet !== null) {
      for (const e of object.Wallet) {
        message.Wallet.push(Wallet.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse>;

  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse>;

  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse>;

  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse>;

  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse>;

  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse>;

  AssignCosmosAddressToWallet(
    request: MsgAssignCosmosAddressToWallet
  ): Promise<MsgEmptyResponse>;

  CreateTokenRef(request: MsgCreateTokenRef): Promise<MsgIdResponse>;

  MoveTokenToSegment(request: MsgMoveTokenToSegment): Promise<MsgEmptyResponse>;

  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse>;

  RemoveTokenRefFromSegment(
    request: MsgRemoveTokenRefFromSegment
  ): Promise<MsgEmptyResponse>;

  /** Query */
  FetchTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse>;

  FetchTokenHistoryGlobalAll(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse>;

  FetchSegmentHistory(
    request: MsgFetchGetSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse>;

  FetchSegmentHistoryAll(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse>;

  FetchSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse>;

  FetchSegmentAll(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse>;

  FetchWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse>;

  FetchWalletHistoryAll(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse>;

  FetchWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse>;

  FetchWalletAll(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }

  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse> {
    const data = MsgCreateSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateSegment",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse> {
    const data = MsgCreateSegmentWithId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateSegmentWithId",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse> {
    const data = MsgUpdateSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "UpdateSegment",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse> {
    const data = MsgCreateWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateWallet",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse> {
    const data = MsgCreateWalletWithId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateWalletWithId",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse> {
    const data = MsgUpdateWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "UpdateWallet",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  AssignCosmosAddressToWallet(
    request: MsgAssignCosmosAddressToWallet
  ): Promise<MsgEmptyResponse> {
    const data = MsgAssignCosmosAddressToWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "AssignCosmosAddressToWallet",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  CreateTokenRef(request: MsgCreateTokenRef): Promise<MsgIdResponse> {
    const data = MsgCreateTokenRef.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateTokenRef",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  MoveTokenToSegment(
    request: MsgMoveTokenToSegment
  ): Promise<MsgEmptyResponse> {
    const data = MsgMoveTokenToSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "MoveTokenToSegment",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse> {
    const data = MsgMoveTokenToWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "MoveTokenToWallet",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  RemoveTokenRefFromSegment(
    request: MsgRemoveTokenRefFromSegment
  ): Promise<MsgEmptyResponse> {
    const data = MsgRemoveTokenRefFromSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "RemoveTokenRefFromSegment",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  FetchTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse> {
    const data = MsgFetchGetTokenHistoryGlobal.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchTokenHistoryGlobal",
      data
    );
    return promise.then((data) =>
      MsgFetchGetTokenHistoryGlobalResponse.decode(new Reader(data))
    );
  }

  FetchTokenHistoryGlobalAll(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse> {
    const data = MsgFetchAllTokenHistoryGlobal.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchTokenHistoryGlobalAll",
      data
    );
    return promise.then((data) =>
      MsgFetchAllTokenHistoryGlobalResponse.decode(new Reader(data))
    );
  }

  FetchSegmentHistory(
    request: MsgFetchGetSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse> {
    const data = MsgFetchGetSegmentHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegmentHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchGetSegmentHistoryResponse.decode(new Reader(data))
    );
  }

  FetchSegmentHistoryAll(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse> {
    const data = MsgFetchAllSegmentHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegmentHistoryAll",
      data
    );
    return promise.then((data) =>
      MsgFetchAllSegmentHistoryResponse.decode(new Reader(data))
    );
  }

  FetchSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse> {
    const data = MsgFetchGetSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegment",
      data
    );
    return promise.then((data) =>
      MsgFetchGetSegmentResponse.decode(new Reader(data))
    );
  }

  FetchSegmentAll(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse> {
    const data = MsgFetchAllSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegmentAll",
      data
    );
    return promise.then((data) =>
      MsgFetchAllSegmentResponse.decode(new Reader(data))
    );
  }

  FetchWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse> {
    const data = MsgFetchGetWalletHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWalletHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchGetWalletHistoryResponse.decode(new Reader(data))
    );
  }

  FetchWalletHistoryAll(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse> {
    const data = MsgFetchAllWalletHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWalletHistoryAll",
      data
    );
    return promise.then((data) =>
      MsgFetchAllWalletHistoryResponse.decode(new Reader(data))
    );
  }

  FetchWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse> {
    const data = MsgFetchGetWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWallet",
      data
    );
    return promise.then((data) =>
      MsgFetchGetWalletResponse.decode(new Reader(data))
    );
  }

  FetchWalletAll(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse> {
    const data = MsgFetchAllWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWalletAll",
      data
    );
    return promise.then((data) =>
      MsgFetchAllWalletResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
