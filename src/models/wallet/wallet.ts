// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

export interface Wallet {
  creator: string;
  id: string;
  name: string;
  timestamp: string;
  segmentIds: string[];
  walletAccounts: WalletAccount[];
}

export interface WalletAccount {
  address: string;
  active: boolean;
}

const baseWallet: object = {
  creator: "",
  id: "",
  name: "",
  timestamp: "",
  segmentIds: ""
};

export const Wallet = {
  encode(message: Wallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.timestamp !== "") {
      writer.uint32(34).string(message.timestamp);
    }
    for (const v of message.segmentIds) {
      writer.uint32(42).string(v!);
    }
    for (const v of message.walletAccounts) {
      WalletAccount.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Wallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseWallet } as Wallet;
    message.segmentIds = [];
    message.walletAccounts = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.timestamp = reader.string();
          break;
        case 5:
          message.segmentIds.push(reader.string());
          break;
        case 6:
          message.walletAccounts.push(
            WalletAccount.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Wallet {
    const message = { ...baseWallet } as Wallet;
    message.segmentIds = [];
    message.walletAccounts = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = "";
    }
    if (object.segmentIds !== undefined && object.segmentIds !== null) {
      for (const e of object.segmentIds) {
        message.segmentIds.push(String(e));
      }
    }
    if (object.walletAccounts !== undefined && object.walletAccounts !== null) {
      for (const e of object.walletAccounts) {
        message.walletAccounts.push(WalletAccount.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Wallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    if (message.segmentIds) {
      obj.segmentIds = message.segmentIds.map((e) => e);
    } else {
      obj.segmentIds = [];
    }
    if (message.walletAccounts) {
      obj.walletAccounts = message.walletAccounts.map((e) =>
        e ? WalletAccount.toJSON(e) : undefined
      );
    } else {
      obj.walletAccounts = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Wallet>): Wallet {
    const message = { ...baseWallet } as Wallet;
    message.segmentIds = [];
    message.walletAccounts = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = "";
    }
    if (object.segmentIds !== undefined && object.segmentIds !== null) {
      for (const e of object.segmentIds) {
        message.segmentIds.push(e);
      }
    }
    if (object.walletAccounts !== undefined && object.walletAccounts !== null) {
      for (const e of object.walletAccounts) {
        message.walletAccounts.push(WalletAccount.fromPartial(e));
      }
    }
    return message;
  }
};

const baseWalletAccount: object = { address: "", active: false };

export const WalletAccount = {
  encode(message: WalletAccount, writer: Writer = Writer.create()): Writer {
    if (message.address !== "") {
      writer.uint32(10).string(message.address);
    }
    if (message.active === true) {
      writer.uint32(16).bool(message.active);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): WalletAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseWalletAccount } as WalletAccount;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = reader.string();
          break;
        case 2:
          message.active = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): WalletAccount {
    const message = { ...baseWalletAccount } as WalletAccount;
    if (object.address !== undefined && object.address !== null) {
      message.address = String(object.address);
    } else {
      message.address = "";
    }
    if (object.active !== undefined && object.active !== null) {
      message.active = Boolean(object.active);
    } else {
      message.active = false;
    }
    return message;
  },

  toJSON(message: WalletAccount): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address);
    message.active !== undefined && (obj.active = message.active);
    return obj;
  },

  fromPartial(object: DeepPartial<WalletAccount>): WalletAccount {
    const message = { ...baseWalletAccount } as WalletAccount;
    if (object.address !== undefined && object.address !== null) {
      message.address = object.address;
    } else {
      message.address = "";
    }
    if (object.active !== undefined && object.active !== null) {
      message.active = object.active;
    } else {
      message.active = false;
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
