// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

export interface Segment {
  creator: string;
  id: string;
  name: string;
  timestamp: string;
  info: string;
  walletId: string;
  tokenRefs: TokenRef[];
}

export interface TokenRef {
  id: string;
  moduleRef: string;
  valid: boolean;
}

const baseSegment: object = {
  creator: "",
  id: "",
  name: "",
  timestamp: "",
  info: "",
  walletId: ""
};

export const Segment = {
  encode(message: Segment, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.timestamp !== "") {
      writer.uint32(34).string(message.timestamp);
    }
    if (message.info !== "") {
      writer.uint32(42).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(50).string(message.walletId);
    }
    for (const v of message.tokenRefs) {
      TokenRef.encode(v!, writer.uint32(58).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Segment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSegment } as Segment;
    message.tokenRefs = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.timestamp = reader.string();
          break;
        case 5:
          message.info = reader.string();
          break;
        case 6:
          message.walletId = reader.string();
          break;
        case 7:
          message.tokenRefs.push(TokenRef.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Segment {
    const message = { ...baseSegment } as Segment;
    message.tokenRefs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = String(object.info);
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = String(object.walletId);
    } else {
      message.walletId = "";
    }
    if (object.tokenRefs !== undefined && object.tokenRefs !== null) {
      for (const e of object.tokenRefs) {
        message.tokenRefs.push(TokenRef.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Segment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    if (message.tokenRefs) {
      obj.tokenRefs = message.tokenRefs.map((e) =>
        e ? TokenRef.toJSON(e) : undefined
      );
    } else {
      obj.tokenRefs = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Segment>): Segment {
    const message = { ...baseSegment } as Segment;
    message.tokenRefs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = object.info;
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = object.walletId;
    } else {
      message.walletId = "";
    }
    if (object.tokenRefs !== undefined && object.tokenRefs !== null) {
      for (const e of object.tokenRefs) {
        message.tokenRefs.push(TokenRef.fromPartial(e));
      }
    }
    return message;
  }
};

const baseTokenRef: object = { id: "", moduleRef: "", valid: false };

export const TokenRef = {
  encode(message: TokenRef, writer: Writer = Writer.create()): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.moduleRef !== "") {
      writer.uint32(18).string(message.moduleRef);
    }
    if (message.valid === true) {
      writer.uint32(24).bool(message.valid);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): TokenRef {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseTokenRef } as TokenRef;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.moduleRef = reader.string();
          break;
        case 3:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TokenRef {
    const message = { ...baseTokenRef } as TokenRef;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = String(object.moduleRef);
    } else {
      message.moduleRef = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    return message;
  },

  toJSON(message: TokenRef): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial(object: DeepPartial<TokenRef>): TokenRef {
    const message = { ...baseTokenRef } as TokenRef;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = object.moduleRef;
    } else {
      message.moduleRef = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
