// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";
import { Token } from "../token/token";
import { TokenHistory } from "../token/tokenHistory";

export const protobufPackage =
  "silicon_economy.base.blockchainbroker.digital_folder.modules.token";

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgFetchAllToken {
  creator: string;
}

export interface MsgFetchAllTokenResponse {
  Token: Token[];
}

export interface MsgFetchAllTokenHistory {
  creator: string;
}

export interface MsgFetchAllTokenHistoryResponse {
  TokenHistory: TokenHistory[];
}

export interface MsgFetchTokenHistory {
  creator: string;
  id: string;
}

export interface MsgFetchTokenHistoryResponse {
  TokenHistory: TokenHistory | undefined;
}

export interface MsgFetchToken {
  creator: string;
  id: string;
}

export interface MsgFetchTokenResponse {
  Token: Token | undefined;
}

export interface MsgCreateTokenHistory {
  creator: string;
  id: string;
  history: Token[];
}

export interface MsgUpdateTokenHistory {
  creator: string;
  id: string;
}

export interface MsgCreateToken {
  creator: string;
  id: string;
  timestamp: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
}

export interface MsgUpdateToken {
  creator: string;
  id: string;
  timestamp: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
}

export interface MsgDeactivateToken {
  creator: string;
  id: string;
}

export interface MsgActivateToken {
  creator: string;
  id: string;
}

export interface MsgUpdateTokenInformation {
  creator: string;
  tokenId: string;
  data: string;
}

export interface MsgEmptyResponse {
}

export interface MsgIdResponse {
  id: string;
}

const baseMsgFetchAllToken: object = { creator: "" };

export const MsgFetchAllToken = {
  encode(message: MsgFetchAllToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchAllToken } as MsgFetchAllToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllToken {
    const message = { ...baseMsgFetchAllToken } as MsgFetchAllToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchAllToken>): MsgFetchAllToken {
    const message = { ...baseMsgFetchAllToken } as MsgFetchAllToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchAllTokenResponse: object = {};

export const MsgFetchAllTokenResponse = {
  encode(
    message: MsgFetchAllTokenResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.Token) {
      Token.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllTokenResponse
    } as MsgFetchAllTokenResponse;
    message.Token = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token.push(Token.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenResponse {
    const message = {
      ...baseMsgFetchAllTokenResponse
    } as MsgFetchAllTokenResponse;
    message.Token = [];
    if (object.Token !== undefined && object.Token !== null) {
      for (const e of object.Token) {
        message.Token.push(Token.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgFetchAllTokenResponse): unknown {
    const obj: any = {};
    if (message.Token) {
      obj.Token = message.Token.map((e) => (e ? Token.toJSON(e) : undefined));
    } else {
      obj.Token = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllTokenResponse>
  ): MsgFetchAllTokenResponse {
    const message = {
      ...baseMsgFetchAllTokenResponse
    } as MsgFetchAllTokenResponse;
    message.Token = [];
    if (object.Token !== undefined && object.Token !== null) {
      for (const e of object.Token) {
        message.Token.push(Token.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgFetchAllTokenHistory: object = { creator: "" };

export const MsgFetchAllTokenHistory = {
  encode(
    message: MsgFetchAllTokenHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllTokenHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllTokenHistory
    } as MsgFetchAllTokenHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistory {
    const message = {
      ...baseMsgFetchAllTokenHistory
    } as MsgFetchAllTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistory>
  ): MsgFetchAllTokenHistory {
    const message = {
      ...baseMsgFetchAllTokenHistory
    } as MsgFetchAllTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchAllTokenHistoryResponse: object = {};

export const MsgFetchAllTokenHistoryResponse = {
  encode(
    message: MsgFetchAllTokenHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.TokenHistory) {
      TokenHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllTokenHistoryResponse
    } as MsgFetchAllTokenHistoryResponse;
    message.TokenHistory = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory.push(
            TokenHistory.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryResponse {
    const message = {
      ...baseMsgFetchAllTokenHistoryResponse
    } as MsgFetchAllTokenHistoryResponse;
    message.TokenHistory = [];
    if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
      for (const e of object.TokenHistory) {
        message.TokenHistory.push(TokenHistory.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgFetchAllTokenHistoryResponse): unknown {
    const obj: any = {};
    if (message.TokenHistory) {
      obj.TokenHistory = message.TokenHistory.map((e) =>
        e ? TokenHistory.toJSON(e) : undefined
      );
    } else {
      obj.TokenHistory = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistoryResponse>
  ): MsgFetchAllTokenHistoryResponse {
    const message = {
      ...baseMsgFetchAllTokenHistoryResponse
    } as MsgFetchAllTokenHistoryResponse;
    message.TokenHistory = [];
    if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
      for (const e of object.TokenHistory) {
        message.TokenHistory.push(TokenHistory.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgFetchTokenHistory: object = { creator: "", id: "" };

export const MsgFetchTokenHistory = {
  encode(
    message: MsgFetchTokenHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchTokenHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchTokenHistory } as MsgFetchTokenHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistory {
    const message = { ...baseMsgFetchTokenHistory } as MsgFetchTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchTokenHistory>): MsgFetchTokenHistory {
    const message = { ...baseMsgFetchTokenHistory } as MsgFetchTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchTokenHistoryResponse: object = {};

export const MsgFetchTokenHistoryResponse = {
  encode(
    message: MsgFetchTokenHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.TokenHistory !== undefined) {
      TokenHistory.encode(
        message.TokenHistory,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchTokenHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchTokenHistoryResponse
    } as MsgFetchTokenHistoryResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory = TokenHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistoryResponse {
    const message = {
      ...baseMsgFetchTokenHistoryResponse
    } as MsgFetchTokenHistoryResponse;
    if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
      message.TokenHistory = TokenHistory.fromJSON(object.TokenHistory);
    } else {
      message.TokenHistory = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchTokenHistoryResponse): unknown {
    const obj: any = {};
    message.TokenHistory !== undefined &&
    (obj.TokenHistory = message.TokenHistory
      ? TokenHistory.toJSON(message.TokenHistory)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchTokenHistoryResponse>
  ): MsgFetchTokenHistoryResponse {
    const message = {
      ...baseMsgFetchTokenHistoryResponse
    } as MsgFetchTokenHistoryResponse;
    if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
      message.TokenHistory = TokenHistory.fromPartial(object.TokenHistory);
    } else {
      message.TokenHistory = undefined;
    }
    return message;
  }
};

const baseMsgFetchToken: object = { creator: "", id: "" };

export const MsgFetchToken = {
  encode(message: MsgFetchToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchToken } as MsgFetchToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchToken {
    const message = { ...baseMsgFetchToken } as MsgFetchToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchToken>): MsgFetchToken {
    const message = { ...baseMsgFetchToken } as MsgFetchToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchTokenResponse: object = {};

export const MsgFetchTokenResponse = {
  encode(
    message: MsgFetchTokenResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.Token !== undefined) {
      Token.encode(message.Token, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchTokenResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchTokenResponse } as MsgFetchTokenResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token = Token.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenResponse {
    const message = { ...baseMsgFetchTokenResponse } as MsgFetchTokenResponse;
    if (object.Token !== undefined && object.Token !== null) {
      message.Token = Token.fromJSON(object.Token);
    } else {
      message.Token = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchTokenResponse): unknown {
    const obj: any = {};
    message.Token !== undefined &&
    (obj.Token = message.Token ? Token.toJSON(message.Token) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchTokenResponse>
  ): MsgFetchTokenResponse {
    const message = { ...baseMsgFetchTokenResponse } as MsgFetchTokenResponse;
    if (object.Token !== undefined && object.Token !== null) {
      message.Token = Token.fromPartial(object.Token);
    } else {
      message.Token = undefined;
    }
    return message;
  }
};

const baseMsgCreateTokenHistory: object = { creator: "", id: "" };

export const MsgCreateTokenHistory = {
  encode(
    message: MsgCreateTokenHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Token.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateTokenHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateTokenHistory } as MsgCreateTokenHistory;
    message.history = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Token.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenHistory {
    const message = { ...baseMsgCreateTokenHistory } as MsgCreateTokenHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Token.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgCreateTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) =>
        e ? Token.toJSON(e) : undefined
      );
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateTokenHistory>
  ): MsgCreateTokenHistory {
    const message = { ...baseMsgCreateTokenHistory } as MsgCreateTokenHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Token.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgUpdateTokenHistory: object = { creator: "", id: "" };

export const MsgUpdateTokenHistory = {
  encode(
    message: MsgUpdateTokenHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateTokenHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateTokenHistory } as MsgUpdateTokenHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenHistory {
    const message = { ...baseMsgUpdateTokenHistory } as MsgUpdateTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateTokenHistory>
  ): MsgUpdateTokenHistory {
    const message = { ...baseMsgUpdateTokenHistory } as MsgUpdateTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateToken: object = {
  creator: "",
  id: "",
  timestamp: "",
  tokenType: "",
  changeMessage: "",
  segmentId: ""
};

export const MsgCreateToken = {
  encode(message: MsgCreateToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenType !== "") {
      writer.uint32(34).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(50).string(message.segmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateToken } as MsgCreateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 4:
          message.tokenType = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateToken {
    const message = { ...baseMsgCreateToken } as MsgCreateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = String(object.tokenType);
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = String(object.changeMessage);
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    return message;
  },

  toJSON(message: MsgCreateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined &&
    (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateToken>): MsgCreateToken {
    const message = { ...baseMsgCreateToken } as MsgCreateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = object.tokenType;
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = object.changeMessage;
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    return message;
  }
};

const baseMsgUpdateToken: object = {
  creator: "",
  id: "",
  timestamp: "",
  tokenType: "",
  changeMessage: "",
  segmentId: ""
};

export const MsgUpdateToken = {
  encode(message: MsgUpdateToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenType !== "") {
      writer.uint32(34).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(50).string(message.segmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateToken } as MsgUpdateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 4:
          message.tokenType = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateToken {
    const message = { ...baseMsgUpdateToken } as MsgUpdateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = String(object.tokenType);
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = String(object.changeMessage);
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined &&
    (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateToken>): MsgUpdateToken {
    const message = { ...baseMsgUpdateToken } as MsgUpdateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = object.tokenType;
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = object.changeMessage;
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    return message;
  }
};

const baseMsgDeactivateToken: object = { creator: "", id: "" };

export const MsgDeactivateToken = {
  encode(
    message: MsgDeactivateToken,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeactivateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDeactivateToken } as MsgDeactivateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateToken {
    const message = { ...baseMsgDeactivateToken } as MsgDeactivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgDeactivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgDeactivateToken>): MsgDeactivateToken {
    const message = { ...baseMsgDeactivateToken } as MsgDeactivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgActivateToken: object = { creator: "", id: "" };

export const MsgActivateToken = {
  encode(message: MsgActivateToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgActivateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgActivateToken } as MsgActivateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgActivateToken {
    const message = { ...baseMsgActivateToken } as MsgActivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgActivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgActivateToken>): MsgActivateToken {
    const message = { ...baseMsgActivateToken } as MsgActivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgUpdateTokenInformation: object = {
  creator: "",
  tokenId: "",
  data: ""
};

export const MsgUpdateTokenInformation = {
  encode(
    message: MsgUpdateTokenInformation,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenId !== "") {
      writer.uint32(18).string(message.tokenId);
    }
    if (message.data !== "") {
      writer.uint32(26).string(message.data);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateTokenInformation {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateTokenInformation
    } as MsgUpdateTokenInformation;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenId = reader.string();
          break;
        case 3:
          message.data = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenInformation {
    const message = {
      ...baseMsgUpdateTokenInformation
    } as MsgUpdateTokenInformation;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = String(object.tokenId);
    } else {
      message.tokenId = "";
    }
    if (object.data !== undefined && object.data !== null) {
      message.data = String(object.data);
    } else {
      message.data = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateTokenInformation): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    message.data !== undefined && (obj.data = message.data);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateTokenInformation>
  ): MsgUpdateTokenInformation {
    const message = {
      ...baseMsgUpdateTokenInformation
    } as MsgUpdateTokenInformation;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = object.tokenId;
    } else {
      message.tokenId = "";
    }
    if (object.data !== undefined && object.data !== null) {
      message.data = object.data;
    } else {
      message.data = "";
    }
    return message;
  }
};

const baseMsgEmptyResponse: object = {};

export const MsgEmptyResponse = {
  encode(_: MsgEmptyResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgEmptyResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgEmptyResponse {
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    return message;
  },

  toJSON(_: MsgEmptyResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgEmptyResponse>): MsgEmptyResponse {
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    return message;
  }
};

const baseMsgIdResponse: object = { id: "" };

export const MsgIdResponse = {
  encode(message: MsgIdResponse, writer: Writer = Writer.create()): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgIdResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgIdResponse {
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgIdResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgIdResponse>): MsgIdResponse {
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  FetchAllToken(request: MsgFetchAllToken): Promise<MsgFetchAllTokenResponse>;

  FetchAllTokenHistory(
    request: MsgFetchAllTokenHistory
  ): Promise<MsgFetchAllTokenHistoryResponse>;

  FetchTokenHistory(
    request: MsgFetchTokenHistory
  ): Promise<MsgFetchTokenHistoryResponse>;

  FetchToken(request: MsgFetchToken): Promise<MsgFetchTokenResponse>;

  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse>;

  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse>;

  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse>;

  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse>;

  UpdateTokenInformation(
    request: MsgUpdateTokenInformation
  ): Promise<MsgEmptyResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }

  FetchAllToken(request: MsgFetchAllToken): Promise<MsgFetchAllTokenResponse> {
    const data = MsgFetchAllToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchAllToken",
      data
    );
    return promise.then((data) =>
      MsgFetchAllTokenResponse.decode(new Reader(data))
    );
  }

  FetchAllTokenHistory(
    request: MsgFetchAllTokenHistory
  ): Promise<MsgFetchAllTokenHistoryResponse> {
    const data = MsgFetchAllTokenHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchAllTokenHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchAllTokenHistoryResponse.decode(new Reader(data))
    );
  }

  FetchTokenHistory(
    request: MsgFetchTokenHistory
  ): Promise<MsgFetchTokenHistoryResponse> {
    const data = MsgFetchTokenHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchTokenHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchTokenHistoryResponse.decode(new Reader(data))
    );
  }

  FetchToken(request: MsgFetchToken): Promise<MsgFetchTokenResponse> {
    const data = MsgFetchToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchToken",
      data
    );
    return promise.then((data) =>
      MsgFetchTokenResponse.decode(new Reader(data))
    );
  }

  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse> {
    const data = MsgCreateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "CreateToken",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse> {
    const data = MsgUpdateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "UpdateToken",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse> {
    const data = MsgDeactivateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "DeactivateToken",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse> {
    const data = MsgActivateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "ActivateToken",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  UpdateTokenInformation(
    request: MsgUpdateTokenInformation
  ): Promise<MsgEmptyResponse> {
    const data = MsgUpdateTokenInformation.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "UpdateTokenInformation",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
