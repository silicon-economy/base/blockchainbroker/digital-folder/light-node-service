// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Token } from "../token/token";
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.token";

export interface TokenHistory {
  creator: string;
  id: string;
  history: Token[];
}

const baseTokenHistory: object = { creator: "", id: "" };

export const TokenHistory = {
  encode(message: TokenHistory, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Token.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): TokenHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseTokenHistory } as TokenHistory;
    message.history = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Token.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TokenHistory {
    const message = { ...baseTokenHistory } as TokenHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Token.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: TokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) =>
        e ? Token.toJSON(e) : undefined
      );
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<TokenHistory>): TokenHistory {
    const message = { ...baseTokenHistory } as TokenHistory;
    message.history = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(Token.fromPartial(e));
      }
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
