// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.hashtoken";

export interface Token {
  creator: string;
  id: string;
  tokenType: string;
  timestamp: string;
  changeMessage: string;
  valid: boolean;
  info: Info | undefined;
  segmentId: string;
}

export interface Info {
  document: string;
  hash: string;
  hashFunction: string;
  metadata: string;
}

const baseToken: object = {
  creator: "",
  id: "",
  tokenType: "",
  timestamp: "",
  changeMessage: "",
  valid: false,
  segmentId: ""
};

export const Token = {
  encode(message: Token, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.tokenType !== "") {
      writer.uint32(26).string(message.tokenType);
    }
    if (message.timestamp !== "") {
      writer.uint32(34).string(message.timestamp);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    if (message.info !== undefined) {
      Info.encode(message.info, writer.uint32(58).fork()).ldelim();
    }
    if (message.segmentId !== "") {
      writer.uint32(66).string(message.segmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Token {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseToken } as Token;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.tokenType = reader.string();
          break;
        case 4:
          message.timestamp = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.valid = reader.bool();
          break;
        case 7:
          message.info = Info.decode(reader, reader.uint32());
          break;
        case 8:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Token {
    const message = { ...baseToken } as Token;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = String(object.tokenType);
    } else {
      message.tokenType = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = String(object.changeMessage);
    } else {
      message.changeMessage = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = Info.fromJSON(object.info);
    } else {
      message.info = undefined;
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    return message;
  },

  toJSON(message: Token): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.changeMessage !== undefined &&
    (obj.changeMessage = message.changeMessage);
    message.valid !== undefined && (obj.valid = message.valid);
    message.info !== undefined &&
    (obj.info = message.info ? Info.toJSON(message.info) : undefined);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial(object: DeepPartial<Token>): Token {
    const message = { ...baseToken } as Token;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = object.tokenType;
    } else {
      message.tokenType = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = object.changeMessage;
    } else {
      message.changeMessage = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = Info.fromPartial(object.info);
    } else {
      message.info = undefined;
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    return message;
  }
};

const baseInfo: object = {
  document: "",
  hash: "",
  hashFunction: "",
  metadata: ""
};

export const Info = {
  encode(message: Info, writer: Writer = Writer.create()): Writer {
    if (message.document !== "") {
      writer.uint32(10).string(message.document);
    }
    if (message.hash !== "") {
      writer.uint32(18).string(message.hash);
    }
    if (message.hashFunction !== "") {
      writer.uint32(26).string(message.hashFunction);
    }
    if (message.metadata !== "") {
      writer.uint32(34).string(message.metadata);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Info {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseInfo } as Info;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.document = reader.string();
          break;
        case 2:
          message.hash = reader.string();
          break;
        case 3:
          message.hashFunction = reader.string();
          break;
        case 4:
          message.metadata = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Info {
    const message = { ...baseInfo } as Info;
    if (object.document !== undefined && object.document !== null) {
      message.document = String(object.document);
    } else {
      message.document = "";
    }
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = String(object.hash);
    } else {
      message.hash = "";
    }
    if (object.hashFunction !== undefined && object.hashFunction !== null) {
      message.hashFunction = String(object.hashFunction);
    } else {
      message.hashFunction = "";
    }
    if (object.metadata !== undefined && object.metadata !== null) {
      message.metadata = String(object.metadata);
    } else {
      message.metadata = "";
    }
    return message;
  },

  toJSON(message: Info): unknown {
    const obj: any = {};
    message.document !== undefined && (obj.document = message.document);
    message.hash !== undefined && (obj.hash = message.hash);
    message.hashFunction !== undefined &&
    (obj.hashFunction = message.hashFunction);
    message.metadata !== undefined && (obj.metadata = message.metadata);
    return obj;
  },

  fromPartial(object: DeepPartial<Info>): Info {
    const message = { ...baseInfo } as Info;
    if (object.document !== undefined && object.document !== null) {
      message.document = object.document;
    } else {
      message.document = "";
    }
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = object.hash;
    } else {
      message.hash = "";
    }
    if (object.hashFunction !== undefined && object.hashFunction !== null) {
      message.hashFunction = object.hashFunction;
    } else {
      message.hashFunction = "";
    }
    if (object.metadata !== undefined && object.metadata !== null) {
      message.metadata = object.metadata;
    } else {
      message.metadata = "";
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
