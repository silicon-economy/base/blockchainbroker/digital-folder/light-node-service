// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { ApplicationRoleState } from "../authorization/applicationRoleState";
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

export interface ApplicationRole {
  creator: string;
  id: string;
  applicationRoleStates: ApplicationRoleState[];
}

const baseApplicationRole: object = { creator: "", id: "" };

export const ApplicationRole = {
  encode(message: ApplicationRole, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.applicationRoleStates) {
      ApplicationRoleState.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): ApplicationRole {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseApplicationRole } as ApplicationRole;
    message.applicationRoleStates = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleStates.push(
            ApplicationRoleState.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ApplicationRole {
    const message = { ...baseApplicationRole } as ApplicationRole;
    message.applicationRoleStates = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleStates !== undefined &&
      object.applicationRoleStates !== null
    ) {
      for (const e of object.applicationRoleStates) {
        message.applicationRoleStates.push(ApplicationRoleState.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: ApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.applicationRoleStates) {
      obj.applicationRoleStates = message.applicationRoleStates.map((e) =>
        e ? ApplicationRoleState.toJSON(e) : undefined
      );
    } else {
      obj.applicationRoleStates = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<ApplicationRole>): ApplicationRole {
    const message = { ...baseApplicationRole } as ApplicationRole;
    message.applicationRoleStates = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleStates !== undefined &&
      object.applicationRoleStates !== null
    ) {
      for (const e of object.applicationRoleStates) {
        message.applicationRoleStates.push(ApplicationRoleState.fromPartial(e));
      }
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
