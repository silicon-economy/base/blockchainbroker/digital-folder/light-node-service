// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { configure, Reader, util, Writer } from "protobufjs/minimal";
import * as Long from "long";
import { BlockchainAccountState } from "../authorization/blockchainAccountState";
import { ApplicationRoleState } from "../authorization/applicationRoleState";
import { ApplicationRole } from "../authorization/applicationRole";
import { BlockchainAccount } from "../authorization/blockchainAccount";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgCreateConfiguration {
  creator: string;
  PermissionCheck: boolean;
}

export interface MsgCreateConfigurationResponse {
  id: number;
}

export interface MsgUpdateConfiguration {
  creator: string;
  id: number;
  PermissionCheck: boolean;
}

export interface MsgUpdateConfigurationResponse {
}

export interface MsgDeleteConfiguration {
  creator: string;
  id: number;
}

export interface MsgDeleteConfigurationResponse {
}

export interface MsgCreateBlockchainAccountState {
  creator: string;
  id: string;
  accountID: string;
  timeStamp: string;
  applicationRoleIDs: string[];
  valid: boolean;
}

export interface MsgCreateBlockchainAccountStateResponse {
  id: string;
}

export interface MsgUpdateBlockchainAccountState {
  creator: string;
  id: string;
  accountID: string;
  timeStamp: string;
  applicationRoleIDs: string[];
  valid: boolean;
}

export interface MsgUpdateBlockchainAccountStateResponse {
}

export interface MsgDeleteBlockchainAccountState {
  creator: string;
  id: string;
}

export interface MsgDeleteBlockchainAccountStateResponse {
}

export interface MsgCreateApplicationRoleState {
  creator: string;
  id: string;
  applicationRoleID: string;
  description: string;
  valid: boolean;
  timeStamp: string;
}

export interface MsgCreateApplicationRoleStateResponse {
  id: string;
}

export interface MsgUpdateApplicationRoleState {
  creator: string;
  id: string;
  applicationRoleID: string;
  description: string;
  valid: boolean;
  timeStamp: string;
}

export interface MsgUpdateApplicationRoleStateResponse {
}

export interface MsgDeleteApplicationRoleState {
  creator: string;
  id: string;
}

export interface MsgDeleteApplicationRoleStateResponse {
}

export interface MsgCreateBlockchainAccount {
  creator: string;
  /** repeated BlockchainAccountState blockchainAccountStates = 2; */
  id: string;
}

export interface MsgCreateBlockchainAccountResponse {
  id: string;
}

export interface MsgUpdateBlockchainAccount {
  creator: string;
  id: string;
  blockchainAccountStates: BlockchainAccountState[];
}

export interface MsgUpdateBlockchainAccountResponse {
}

export interface MsgDeactivateBlockchainAccount {
  creator: string;
  id: string;
}

export interface MsgDeleteBlockchainAccountResponse {
}

export interface MsgCreateApplicationRole {
  creator: string;
  id: string;
  /** repeated ApplicationRoleState applicationRoleStates = 2; */
  description: string;
}

export interface MsgCreateApplicationRoleResponse {
  id: string;
}

export interface MsgUpdateApplicationRole {
  creator: string;
  id: string;
  applicationRoleStates: ApplicationRoleState[];
}

export interface MsgUpdateApplicationRoleResponse {
}

export interface MsgDeleteApplicationRole {
  creator: string;
  id: string;
}

export interface MsgDeleteApplicationRoleResponse {
}

export interface MsgDeactivateApplicationRole {
  creator: string;
  id: string;
}

export interface MsgDeactivateApplicationRoleResponse {
}

export interface MsgDeactivateBlockchainAccountResponse {
}

export interface MsgGrantAppRoleToBlockchainAccount {
  creator: string;
  user: string;
  roleID: string;
}

export interface MsgGrantAppRoleToBlockchainAccountResponse {
}

export interface MsgRevokeAppRoleFromBlockchainAccount {
  creator: string;
  account: string;
  roleID: string;
}

export interface MsgRevokeAppRoleFromBlockchainAccountResponse {
}

export interface MsgFetchAllApplicationRole {
  creator: string;
}

export interface MsgFetchAllApplicationRoleResponse {
  role: ApplicationRole[];
}

export interface MsgFetchApplicationRole {
  creator: string;
  id: string;
}

export interface MsgFetchApplicationRoleResponse {
  role: ApplicationRole | undefined;
}

export interface MsgFetchAllBlockchainAccount {
  creator: string;
}

export interface MsgFetchAllBlockchainAccountResponse {
  account: BlockchainAccount[];
}

export interface MsgFetchBlockchainAccount {
  creator: string;
  id: string;
}

export interface MsgFetchBlockchainAccountResponse {
  account: BlockchainAccount | undefined;
}

const baseMsgCreateConfiguration: object = {
  creator: "",
  PermissionCheck: false
};

export const MsgCreateConfiguration = {
  encode(
    message: MsgCreateConfiguration,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.PermissionCheck === true) {
      writer.uint32(16).bool(message.PermissionCheck);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateConfiguration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateConfiguration } as MsgCreateConfiguration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.PermissionCheck = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateConfiguration {
    const message = { ...baseMsgCreateConfiguration } as MsgCreateConfiguration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (
      object.PermissionCheck !== undefined &&
      object.PermissionCheck !== null
    ) {
      message.PermissionCheck = Boolean(object.PermissionCheck);
    } else {
      message.PermissionCheck = false;
    }
    return message;
  },

  toJSON(message: MsgCreateConfiguration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.PermissionCheck !== undefined &&
    (obj.PermissionCheck = message.PermissionCheck);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateConfiguration>
  ): MsgCreateConfiguration {
    const message = { ...baseMsgCreateConfiguration } as MsgCreateConfiguration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (
      object.PermissionCheck !== undefined &&
      object.PermissionCheck !== null
    ) {
      message.PermissionCheck = object.PermissionCheck;
    } else {
      message.PermissionCheck = false;
    }
    return message;
  }
};

const baseMsgCreateConfigurationResponse: object = { id: 0 };

export const MsgCreateConfigurationResponse = {
  encode(
    message: MsgCreateConfigurationResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateConfigurationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateConfigurationResponse
    } as MsgCreateConfigurationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateConfigurationResponse {
    const message = {
      ...baseMsgCreateConfigurationResponse
    } as MsgCreateConfigurationResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: MsgCreateConfigurationResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateConfigurationResponse>
  ): MsgCreateConfigurationResponse {
    const message = {
      ...baseMsgCreateConfigurationResponse
    } as MsgCreateConfigurationResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  }
};

const baseMsgUpdateConfiguration: object = {
  creator: "",
  id: 0,
  PermissionCheck: false
};

export const MsgUpdateConfiguration = {
  encode(
    message: MsgUpdateConfiguration,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== 0) {
      writer.uint32(16).uint64(message.id);
    }
    if (message.PermissionCheck === true) {
      writer.uint32(24).bool(message.PermissionCheck);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateConfiguration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateConfiguration } as MsgUpdateConfiguration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.PermissionCheck = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateConfiguration {
    const message = { ...baseMsgUpdateConfiguration } as MsgUpdateConfiguration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (
      object.PermissionCheck !== undefined &&
      object.PermissionCheck !== null
    ) {
      message.PermissionCheck = Boolean(object.PermissionCheck);
    } else {
      message.PermissionCheck = false;
    }
    return message;
  },

  toJSON(message: MsgUpdateConfiguration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.PermissionCheck !== undefined &&
    (obj.PermissionCheck = message.PermissionCheck);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateConfiguration>
  ): MsgUpdateConfiguration {
    const message = { ...baseMsgUpdateConfiguration } as MsgUpdateConfiguration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (
      object.PermissionCheck !== undefined &&
      object.PermissionCheck !== null
    ) {
      message.PermissionCheck = object.PermissionCheck;
    } else {
      message.PermissionCheck = false;
    }
    return message;
  }
};

const baseMsgUpdateConfigurationResponse: object = {};

export const MsgUpdateConfigurationResponse = {
  encode(
    _: MsgUpdateConfigurationResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateConfigurationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateConfigurationResponse
    } as MsgUpdateConfigurationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateConfigurationResponse {
    const message = {
      ...baseMsgUpdateConfigurationResponse
    } as MsgUpdateConfigurationResponse;
    return message;
  },

  toJSON(_: MsgUpdateConfigurationResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgUpdateConfigurationResponse>
  ): MsgUpdateConfigurationResponse {
    const message = {
      ...baseMsgUpdateConfigurationResponse
    } as MsgUpdateConfigurationResponse;
    return message;
  }
};

const baseMsgDeleteConfiguration: object = { creator: "", id: 0 };

export const MsgDeleteConfiguration = {
  encode(
    message: MsgDeleteConfiguration,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== 0) {
      writer.uint32(16).uint64(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteConfiguration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDeleteConfiguration } as MsgDeleteConfiguration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteConfiguration {
    const message = { ...baseMsgDeleteConfiguration } as MsgDeleteConfiguration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: MsgDeleteConfiguration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDeleteConfiguration>
  ): MsgDeleteConfiguration {
    const message = { ...baseMsgDeleteConfiguration } as MsgDeleteConfiguration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  }
};

const baseMsgDeleteConfigurationResponse: object = {};

export const MsgDeleteConfigurationResponse = {
  encode(
    _: MsgDeleteConfigurationResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteConfigurationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteConfigurationResponse
    } as MsgDeleteConfigurationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteConfigurationResponse {
    const message = {
      ...baseMsgDeleteConfigurationResponse
    } as MsgDeleteConfigurationResponse;
    return message;
  },

  toJSON(_: MsgDeleteConfigurationResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeleteConfigurationResponse>
  ): MsgDeleteConfigurationResponse {
    const message = {
      ...baseMsgDeleteConfigurationResponse
    } as MsgDeleteConfigurationResponse;
    return message;
  }
};

const baseMsgCreateBlockchainAccountState: object = {
  creator: "",
  id: "",
  accountID: "",
  timeStamp: "",
  applicationRoleIDs: "",
  valid: false
};

export const MsgCreateBlockchainAccountState = {
  encode(
    message: MsgCreateBlockchainAccountState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.accountID !== "") {
      writer.uint32(26).string(message.accountID);
    }
    if (message.timeStamp !== "") {
      writer.uint32(34).string(message.timeStamp);
    }
    for (const v of message.applicationRoleIDs) {
      writer.uint32(42).string(v!);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateBlockchainAccountState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateBlockchainAccountState
    } as MsgCreateBlockchainAccountState;
    message.applicationRoleIDs = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.accountID = reader.string();
          break;
        case 4:
          message.timeStamp = reader.string();
          break;
        case 5:
          message.applicationRoleIDs.push(reader.string());
          break;
        case 6:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccountState {
    const message = {
      ...baseMsgCreateBlockchainAccountState
    } as MsgCreateBlockchainAccountState;
    message.applicationRoleIDs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.accountID !== undefined && object.accountID !== null) {
      message.accountID = String(object.accountID);
    } else {
      message.accountID = "";
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = String(object.timeStamp);
    } else {
      message.timeStamp = "";
    }
    if (
      object.applicationRoleIDs !== undefined &&
      object.applicationRoleIDs !== null
    ) {
      for (const e of object.applicationRoleIDs) {
        message.applicationRoleIDs.push(String(e));
      }
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    return message;
  },

  toJSON(message: MsgCreateBlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.accountID !== undefined && (obj.accountID = message.accountID);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    if (message.applicationRoleIDs) {
      obj.applicationRoleIDs = message.applicationRoleIDs.map((e) => e);
    } else {
      obj.applicationRoleIDs = [];
    }
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateBlockchainAccountState>
  ): MsgCreateBlockchainAccountState {
    const message = {
      ...baseMsgCreateBlockchainAccountState
    } as MsgCreateBlockchainAccountState;
    message.applicationRoleIDs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.accountID !== undefined && object.accountID !== null) {
      message.accountID = object.accountID;
    } else {
      message.accountID = "";
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = object.timeStamp;
    } else {
      message.timeStamp = "";
    }
    if (
      object.applicationRoleIDs !== undefined &&
      object.applicationRoleIDs !== null
    ) {
      for (const e of object.applicationRoleIDs) {
        message.applicationRoleIDs.push(e);
      }
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    return message;
  }
};

const baseMsgCreateBlockchainAccountStateResponse: object = { id: "" };

export const MsgCreateBlockchainAccountStateResponse = {
  encode(
    message: MsgCreateBlockchainAccountStateResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateBlockchainAccountStateResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateBlockchainAccountStateResponse
    } as MsgCreateBlockchainAccountStateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccountStateResponse {
    const message = {
      ...baseMsgCreateBlockchainAccountStateResponse
    } as MsgCreateBlockchainAccountStateResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgCreateBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateBlockchainAccountStateResponse>
  ): MsgCreateBlockchainAccountStateResponse {
    const message = {
      ...baseMsgCreateBlockchainAccountStateResponse
    } as MsgCreateBlockchainAccountStateResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgUpdateBlockchainAccountState: object = {
  creator: "",
  id: "",
  accountID: "",
  timeStamp: "",
  applicationRoleIDs: "",
  valid: false
};

export const MsgUpdateBlockchainAccountState = {
  encode(
    message: MsgUpdateBlockchainAccountState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.accountID !== "") {
      writer.uint32(26).string(message.accountID);
    }
    if (message.timeStamp !== "") {
      writer.uint32(34).string(message.timeStamp);
    }
    for (const v of message.applicationRoleIDs) {
      writer.uint32(42).string(v!);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateBlockchainAccountState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateBlockchainAccountState
    } as MsgUpdateBlockchainAccountState;
    message.applicationRoleIDs = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.accountID = reader.string();
          break;
        case 4:
          message.timeStamp = reader.string();
          break;
        case 5:
          message.applicationRoleIDs.push(reader.string());
          break;
        case 6:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateBlockchainAccountState {
    const message = {
      ...baseMsgUpdateBlockchainAccountState
    } as MsgUpdateBlockchainAccountState;
    message.applicationRoleIDs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.accountID !== undefined && object.accountID !== null) {
      message.accountID = String(object.accountID);
    } else {
      message.accountID = "";
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = String(object.timeStamp);
    } else {
      message.timeStamp = "";
    }
    if (
      object.applicationRoleIDs !== undefined &&
      object.applicationRoleIDs !== null
    ) {
      for (const e of object.applicationRoleIDs) {
        message.applicationRoleIDs.push(String(e));
      }
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    return message;
  },

  toJSON(message: MsgUpdateBlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.accountID !== undefined && (obj.accountID = message.accountID);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    if (message.applicationRoleIDs) {
      obj.applicationRoleIDs = message.applicationRoleIDs.map((e) => e);
    } else {
      obj.applicationRoleIDs = [];
    }
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateBlockchainAccountState>
  ): MsgUpdateBlockchainAccountState {
    const message = {
      ...baseMsgUpdateBlockchainAccountState
    } as MsgUpdateBlockchainAccountState;
    message.applicationRoleIDs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.accountID !== undefined && object.accountID !== null) {
      message.accountID = object.accountID;
    } else {
      message.accountID = "";
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = object.timeStamp;
    } else {
      message.timeStamp = "";
    }
    if (
      object.applicationRoleIDs !== undefined &&
      object.applicationRoleIDs !== null
    ) {
      for (const e of object.applicationRoleIDs) {
        message.applicationRoleIDs.push(e);
      }
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    return message;
  }
};

const baseMsgUpdateBlockchainAccountStateResponse: object = {};

export const MsgUpdateBlockchainAccountStateResponse = {
  encode(
    _: MsgUpdateBlockchainAccountStateResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateBlockchainAccountStateResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateBlockchainAccountStateResponse
    } as MsgUpdateBlockchainAccountStateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateBlockchainAccountStateResponse {
    const message = {
      ...baseMsgUpdateBlockchainAccountStateResponse
    } as MsgUpdateBlockchainAccountStateResponse;
    return message;
  },

  toJSON(_: MsgUpdateBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgUpdateBlockchainAccountStateResponse>
  ): MsgUpdateBlockchainAccountStateResponse {
    const message = {
      ...baseMsgUpdateBlockchainAccountStateResponse
    } as MsgUpdateBlockchainAccountStateResponse;
    return message;
  }
};

const baseMsgDeleteBlockchainAccountState: object = { creator: "", id: "" };

export const MsgDeleteBlockchainAccountState = {
  encode(
    message: MsgDeleteBlockchainAccountState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteBlockchainAccountState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteBlockchainAccountState
    } as MsgDeleteBlockchainAccountState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteBlockchainAccountState {
    const message = {
      ...baseMsgDeleteBlockchainAccountState
    } as MsgDeleteBlockchainAccountState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgDeleteBlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDeleteBlockchainAccountState>
  ): MsgDeleteBlockchainAccountState {
    const message = {
      ...baseMsgDeleteBlockchainAccountState
    } as MsgDeleteBlockchainAccountState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgDeleteBlockchainAccountStateResponse: object = {};

export const MsgDeleteBlockchainAccountStateResponse = {
  encode(
    _: MsgDeleteBlockchainAccountStateResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteBlockchainAccountStateResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteBlockchainAccountStateResponse
    } as MsgDeleteBlockchainAccountStateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteBlockchainAccountStateResponse {
    const message = {
      ...baseMsgDeleteBlockchainAccountStateResponse
    } as MsgDeleteBlockchainAccountStateResponse;
    return message;
  },

  toJSON(_: MsgDeleteBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeleteBlockchainAccountStateResponse>
  ): MsgDeleteBlockchainAccountStateResponse {
    const message = {
      ...baseMsgDeleteBlockchainAccountStateResponse
    } as MsgDeleteBlockchainAccountStateResponse;
    return message;
  }
};

const baseMsgCreateApplicationRoleState: object = {
  creator: "",
  id: "",
  applicationRoleID: "",
  description: "",
  valid: false,
  timeStamp: ""
};

export const MsgCreateApplicationRoleState = {
  encode(
    message: MsgCreateApplicationRoleState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.applicationRoleID !== "") {
      writer.uint32(26).string(message.applicationRoleID);
    }
    if (message.description !== "") {
      writer.uint32(34).string(message.description);
    }
    if (message.valid === true) {
      writer.uint32(40).bool(message.valid);
    }
    if (message.timeStamp !== "") {
      writer.uint32(50).string(message.timeStamp);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateApplicationRoleState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateApplicationRoleState
    } as MsgCreateApplicationRoleState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleID = reader.string();
          break;
        case 4:
          message.description = reader.string();
          break;
        case 5:
          message.valid = reader.bool();
          break;
        case 6:
          message.timeStamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRoleState {
    const message = {
      ...baseMsgCreateApplicationRoleState
    } as MsgCreateApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleID !== undefined &&
      object.applicationRoleID !== null
    ) {
      message.applicationRoleID = String(object.applicationRoleID);
    } else {
      message.applicationRoleID = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    } else {
      message.description = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = String(object.timeStamp);
    } else {
      message.timeStamp = "";
    }
    return message;
  },

  toJSON(message: MsgCreateApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.applicationRoleID !== undefined &&
    (obj.applicationRoleID = message.applicationRoleID);
    message.description !== undefined &&
    (obj.description = message.description);
    message.valid !== undefined && (obj.valid = message.valid);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateApplicationRoleState>
  ): MsgCreateApplicationRoleState {
    const message = {
      ...baseMsgCreateApplicationRoleState
    } as MsgCreateApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleID !== undefined &&
      object.applicationRoleID !== null
    ) {
      message.applicationRoleID = object.applicationRoleID;
    } else {
      message.applicationRoleID = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    } else {
      message.description = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = object.timeStamp;
    } else {
      message.timeStamp = "";
    }
    return message;
  }
};

const baseMsgCreateApplicationRoleStateResponse: object = { id: "" };

export const MsgCreateApplicationRoleStateResponse = {
  encode(
    message: MsgCreateApplicationRoleStateResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateApplicationRoleStateResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateApplicationRoleStateResponse
    } as MsgCreateApplicationRoleStateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRoleStateResponse {
    const message = {
      ...baseMsgCreateApplicationRoleStateResponse
    } as MsgCreateApplicationRoleStateResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgCreateApplicationRoleStateResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateApplicationRoleStateResponse>
  ): MsgCreateApplicationRoleStateResponse {
    const message = {
      ...baseMsgCreateApplicationRoleStateResponse
    } as MsgCreateApplicationRoleStateResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgUpdateApplicationRoleState: object = {
  creator: "",
  id: "",
  applicationRoleID: "",
  description: "",
  valid: false,
  timeStamp: ""
};

export const MsgUpdateApplicationRoleState = {
  encode(
    message: MsgUpdateApplicationRoleState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.applicationRoleID !== "") {
      writer.uint32(26).string(message.applicationRoleID);
    }
    if (message.description !== "") {
      writer.uint32(34).string(message.description);
    }
    if (message.valid === true) {
      writer.uint32(40).bool(message.valid);
    }
    if (message.timeStamp !== "") {
      writer.uint32(50).string(message.timeStamp);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateApplicationRoleState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateApplicationRoleState
    } as MsgUpdateApplicationRoleState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleID = reader.string();
          break;
        case 4:
          message.description = reader.string();
          break;
        case 5:
          message.valid = reader.bool();
          break;
        case 6:
          message.timeStamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateApplicationRoleState {
    const message = {
      ...baseMsgUpdateApplicationRoleState
    } as MsgUpdateApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleID !== undefined &&
      object.applicationRoleID !== null
    ) {
      message.applicationRoleID = String(object.applicationRoleID);
    } else {
      message.applicationRoleID = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    } else {
      message.description = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = String(object.timeStamp);
    } else {
      message.timeStamp = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.applicationRoleID !== undefined &&
    (obj.applicationRoleID = message.applicationRoleID);
    message.description !== undefined &&
    (obj.description = message.description);
    message.valid !== undefined && (obj.valid = message.valid);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateApplicationRoleState>
  ): MsgUpdateApplicationRoleState {
    const message = {
      ...baseMsgUpdateApplicationRoleState
    } as MsgUpdateApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleID !== undefined &&
      object.applicationRoleID !== null
    ) {
      message.applicationRoleID = object.applicationRoleID;
    } else {
      message.applicationRoleID = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    } else {
      message.description = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = object.timeStamp;
    } else {
      message.timeStamp = "";
    }
    return message;
  }
};

const baseMsgUpdateApplicationRoleStateResponse: object = {};

export const MsgUpdateApplicationRoleStateResponse = {
  encode(
    _: MsgUpdateApplicationRoleStateResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateApplicationRoleStateResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateApplicationRoleStateResponse
    } as MsgUpdateApplicationRoleStateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateApplicationRoleStateResponse {
    const message = {
      ...baseMsgUpdateApplicationRoleStateResponse
    } as MsgUpdateApplicationRoleStateResponse;
    return message;
  },

  toJSON(_: MsgUpdateApplicationRoleStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgUpdateApplicationRoleStateResponse>
  ): MsgUpdateApplicationRoleStateResponse {
    const message = {
      ...baseMsgUpdateApplicationRoleStateResponse
    } as MsgUpdateApplicationRoleStateResponse;
    return message;
  }
};

const baseMsgDeleteApplicationRoleState: object = { creator: "", id: "" };

export const MsgDeleteApplicationRoleState = {
  encode(
    message: MsgDeleteApplicationRoleState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteApplicationRoleState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteApplicationRoleState
    } as MsgDeleteApplicationRoleState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteApplicationRoleState {
    const message = {
      ...baseMsgDeleteApplicationRoleState
    } as MsgDeleteApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgDeleteApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDeleteApplicationRoleState>
  ): MsgDeleteApplicationRoleState {
    const message = {
      ...baseMsgDeleteApplicationRoleState
    } as MsgDeleteApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgDeleteApplicationRoleStateResponse: object = {};

export const MsgDeleteApplicationRoleStateResponse = {
  encode(
    _: MsgDeleteApplicationRoleStateResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteApplicationRoleStateResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteApplicationRoleStateResponse
    } as MsgDeleteApplicationRoleStateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteApplicationRoleStateResponse {
    const message = {
      ...baseMsgDeleteApplicationRoleStateResponse
    } as MsgDeleteApplicationRoleStateResponse;
    return message;
  },

  toJSON(_: MsgDeleteApplicationRoleStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeleteApplicationRoleStateResponse>
  ): MsgDeleteApplicationRoleStateResponse {
    const message = {
      ...baseMsgDeleteApplicationRoleStateResponse
    } as MsgDeleteApplicationRoleStateResponse;
    return message;
  }
};

const baseMsgCreateBlockchainAccount: object = { creator: "", id: "" };

export const MsgCreateBlockchainAccount = {
  encode(
    message: MsgCreateBlockchainAccount,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateBlockchainAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateBlockchainAccount
    } as MsgCreateBlockchainAccount;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccount {
    const message = {
      ...baseMsgCreateBlockchainAccount
    } as MsgCreateBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgCreateBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateBlockchainAccount>
  ): MsgCreateBlockchainAccount {
    const message = {
      ...baseMsgCreateBlockchainAccount
    } as MsgCreateBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateBlockchainAccountResponse: object = { id: "" };

export const MsgCreateBlockchainAccountResponse = {
  encode(
    message: MsgCreateBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateBlockchainAccountResponse
    } as MsgCreateBlockchainAccountResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccountResponse {
    const message = {
      ...baseMsgCreateBlockchainAccountResponse
    } as MsgCreateBlockchainAccountResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgCreateBlockchainAccountResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateBlockchainAccountResponse>
  ): MsgCreateBlockchainAccountResponse {
    const message = {
      ...baseMsgCreateBlockchainAccountResponse
    } as MsgCreateBlockchainAccountResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgUpdateBlockchainAccount: object = { creator: "", id: "" };

export const MsgUpdateBlockchainAccount = {
  encode(
    message: MsgUpdateBlockchainAccount,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.blockchainAccountStates) {
      BlockchainAccountState.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateBlockchainAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateBlockchainAccount
    } as MsgUpdateBlockchainAccount;
    message.blockchainAccountStates = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.blockchainAccountStates.push(
            BlockchainAccountState.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateBlockchainAccount {
    const message = {
      ...baseMsgUpdateBlockchainAccount
    } as MsgUpdateBlockchainAccount;
    message.blockchainAccountStates = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (
      object.blockchainAccountStates !== undefined &&
      object.blockchainAccountStates !== null
    ) {
      for (const e of object.blockchainAccountStates) {
        message.blockchainAccountStates.push(
          BlockchainAccountState.fromJSON(e)
        );
      }
    }
    return message;
  },

  toJSON(message: MsgUpdateBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.blockchainAccountStates) {
      obj.blockchainAccountStates = message.blockchainAccountStates.map((e) =>
        e ? BlockchainAccountState.toJSON(e) : undefined
      );
    } else {
      obj.blockchainAccountStates = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateBlockchainAccount>
  ): MsgUpdateBlockchainAccount {
    const message = {
      ...baseMsgUpdateBlockchainAccount
    } as MsgUpdateBlockchainAccount;
    message.blockchainAccountStates = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (
      object.blockchainAccountStates !== undefined &&
      object.blockchainAccountStates !== null
    ) {
      for (const e of object.blockchainAccountStates) {
        message.blockchainAccountStates.push(
          BlockchainAccountState.fromPartial(e)
        );
      }
    }
    return message;
  }
};

const baseMsgUpdateBlockchainAccountResponse: object = {};

export const MsgUpdateBlockchainAccountResponse = {
  encode(
    _: MsgUpdateBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateBlockchainAccountResponse
    } as MsgUpdateBlockchainAccountResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateBlockchainAccountResponse {
    const message = {
      ...baseMsgUpdateBlockchainAccountResponse
    } as MsgUpdateBlockchainAccountResponse;
    return message;
  },

  toJSON(_: MsgUpdateBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgUpdateBlockchainAccountResponse>
  ): MsgUpdateBlockchainAccountResponse {
    const message = {
      ...baseMsgUpdateBlockchainAccountResponse
    } as MsgUpdateBlockchainAccountResponse;
    return message;
  }
};

const baseMsgDeactivateBlockchainAccount: object = { creator: "", id: "" };

export const MsgDeactivateBlockchainAccount = {
  encode(
    message: MsgDeactivateBlockchainAccount,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeactivateBlockchainAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeactivateBlockchainAccount
    } as MsgDeactivateBlockchainAccount;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateBlockchainAccount {
    const message = {
      ...baseMsgDeactivateBlockchainAccount
    } as MsgDeactivateBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgDeactivateBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDeactivateBlockchainAccount>
  ): MsgDeactivateBlockchainAccount {
    const message = {
      ...baseMsgDeactivateBlockchainAccount
    } as MsgDeactivateBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgDeleteBlockchainAccountResponse: object = {};

export const MsgDeleteBlockchainAccountResponse = {
  encode(
    _: MsgDeleteBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteBlockchainAccountResponse
    } as MsgDeleteBlockchainAccountResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteBlockchainAccountResponse {
    const message = {
      ...baseMsgDeleteBlockchainAccountResponse
    } as MsgDeleteBlockchainAccountResponse;
    return message;
  },

  toJSON(_: MsgDeleteBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeleteBlockchainAccountResponse>
  ): MsgDeleteBlockchainAccountResponse {
    const message = {
      ...baseMsgDeleteBlockchainAccountResponse
    } as MsgDeleteBlockchainAccountResponse;
    return message;
  }
};

const baseMsgCreateApplicationRole: object = {
  creator: "",
  id: "",
  description: ""
};

export const MsgCreateApplicationRole = {
  encode(
    message: MsgCreateApplicationRole,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.description !== "") {
      writer.uint32(26).string(message.description);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateApplicationRole {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateApplicationRole
    } as MsgCreateApplicationRole;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRole {
    const message = {
      ...baseMsgCreateApplicationRole
    } as MsgCreateApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    } else {
      message.description = "";
    }
    return message;
  },

  toJSON(message: MsgCreateApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.description !== undefined &&
    (obj.description = message.description);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateApplicationRole>
  ): MsgCreateApplicationRole {
    const message = {
      ...baseMsgCreateApplicationRole
    } as MsgCreateApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    } else {
      message.description = "";
    }
    return message;
  }
};

const baseMsgCreateApplicationRoleResponse: object = { id: "" };

export const MsgCreateApplicationRoleResponse = {
  encode(
    message: MsgCreateApplicationRoleResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateApplicationRoleResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateApplicationRoleResponse
    } as MsgCreateApplicationRoleResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRoleResponse {
    const message = {
      ...baseMsgCreateApplicationRoleResponse
    } as MsgCreateApplicationRoleResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgCreateApplicationRoleResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateApplicationRoleResponse>
  ): MsgCreateApplicationRoleResponse {
    const message = {
      ...baseMsgCreateApplicationRoleResponse
    } as MsgCreateApplicationRoleResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgUpdateApplicationRole: object = { creator: "", id: "" };

export const MsgUpdateApplicationRole = {
  encode(
    message: MsgUpdateApplicationRole,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.applicationRoleStates) {
      ApplicationRoleState.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateApplicationRole {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateApplicationRole
    } as MsgUpdateApplicationRole;
    message.applicationRoleStates = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleStates.push(
            ApplicationRoleState.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateApplicationRole {
    const message = {
      ...baseMsgUpdateApplicationRole
    } as MsgUpdateApplicationRole;
    message.applicationRoleStates = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleStates !== undefined &&
      object.applicationRoleStates !== null
    ) {
      for (const e of object.applicationRoleStates) {
        message.applicationRoleStates.push(ApplicationRoleState.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgUpdateApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.applicationRoleStates) {
      obj.applicationRoleStates = message.applicationRoleStates.map((e) =>
        e ? ApplicationRoleState.toJSON(e) : undefined
      );
    } else {
      obj.applicationRoleStates = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateApplicationRole>
  ): MsgUpdateApplicationRole {
    const message = {
      ...baseMsgUpdateApplicationRole
    } as MsgUpdateApplicationRole;
    message.applicationRoleStates = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleStates !== undefined &&
      object.applicationRoleStates !== null
    ) {
      for (const e of object.applicationRoleStates) {
        message.applicationRoleStates.push(ApplicationRoleState.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgUpdateApplicationRoleResponse: object = {};

export const MsgUpdateApplicationRoleResponse = {
  encode(
    _: MsgUpdateApplicationRoleResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateApplicationRoleResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateApplicationRoleResponse
    } as MsgUpdateApplicationRoleResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateApplicationRoleResponse {
    const message = {
      ...baseMsgUpdateApplicationRoleResponse
    } as MsgUpdateApplicationRoleResponse;
    return message;
  },

  toJSON(_: MsgUpdateApplicationRoleResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgUpdateApplicationRoleResponse>
  ): MsgUpdateApplicationRoleResponse {
    const message = {
      ...baseMsgUpdateApplicationRoleResponse
    } as MsgUpdateApplicationRoleResponse;
    return message;
  }
};

const baseMsgDeleteApplicationRole: object = { creator: "", id: "" };

export const MsgDeleteApplicationRole = {
  encode(
    message: MsgDeleteApplicationRole,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteApplicationRole {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteApplicationRole
    } as MsgDeleteApplicationRole;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteApplicationRole {
    const message = {
      ...baseMsgDeleteApplicationRole
    } as MsgDeleteApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgDeleteApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDeleteApplicationRole>
  ): MsgDeleteApplicationRole {
    const message = {
      ...baseMsgDeleteApplicationRole
    } as MsgDeleteApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgDeleteApplicationRoleResponse: object = {};

export const MsgDeleteApplicationRoleResponse = {
  encode(
    _: MsgDeleteApplicationRoleResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteApplicationRoleResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteApplicationRoleResponse
    } as MsgDeleteApplicationRoleResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteApplicationRoleResponse {
    const message = {
      ...baseMsgDeleteApplicationRoleResponse
    } as MsgDeleteApplicationRoleResponse;
    return message;
  },

  toJSON(_: MsgDeleteApplicationRoleResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeleteApplicationRoleResponse>
  ): MsgDeleteApplicationRoleResponse {
    const message = {
      ...baseMsgDeleteApplicationRoleResponse
    } as MsgDeleteApplicationRoleResponse;
    return message;
  }
};

const baseMsgDeactivateApplicationRole: object = { creator: "", id: "" };

export const MsgDeactivateApplicationRole = {
  encode(
    message: MsgDeactivateApplicationRole,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeactivateApplicationRole {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeactivateApplicationRole
    } as MsgDeactivateApplicationRole;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateApplicationRole {
    const message = {
      ...baseMsgDeactivateApplicationRole
    } as MsgDeactivateApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgDeactivateApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDeactivateApplicationRole>
  ): MsgDeactivateApplicationRole {
    const message = {
      ...baseMsgDeactivateApplicationRole
    } as MsgDeactivateApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgDeactivateApplicationRoleResponse: object = {};

export const MsgDeactivateApplicationRoleResponse = {
  encode(
    _: MsgDeactivateApplicationRoleResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeactivateApplicationRoleResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeactivateApplicationRoleResponse
    } as MsgDeactivateApplicationRoleResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeactivateApplicationRoleResponse {
    const message = {
      ...baseMsgDeactivateApplicationRoleResponse
    } as MsgDeactivateApplicationRoleResponse;
    return message;
  },

  toJSON(_: MsgDeactivateApplicationRoleResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeactivateApplicationRoleResponse>
  ): MsgDeactivateApplicationRoleResponse {
    const message = {
      ...baseMsgDeactivateApplicationRoleResponse
    } as MsgDeactivateApplicationRoleResponse;
    return message;
  }
};

const baseMsgDeactivateBlockchainAccountResponse: object = {};

export const MsgDeactivateBlockchainAccountResponse = {
  encode(
    _: MsgDeactivateBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeactivateBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeactivateBlockchainAccountResponse
    } as MsgDeactivateBlockchainAccountResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeactivateBlockchainAccountResponse {
    const message = {
      ...baseMsgDeactivateBlockchainAccountResponse
    } as MsgDeactivateBlockchainAccountResponse;
    return message;
  },

  toJSON(_: MsgDeactivateBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeactivateBlockchainAccountResponse>
  ): MsgDeactivateBlockchainAccountResponse {
    const message = {
      ...baseMsgDeactivateBlockchainAccountResponse
    } as MsgDeactivateBlockchainAccountResponse;
    return message;
  }
};

const baseMsgGrantAppRoleToBlockchainAccount: object = {
  creator: "",
  user: "",
  roleID: ""
};

export const MsgGrantAppRoleToBlockchainAccount = {
  encode(
    message: MsgGrantAppRoleToBlockchainAccount,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.user !== "") {
      writer.uint32(18).string(message.user);
    }
    if (message.roleID !== "") {
      writer.uint32(26).string(message.roleID);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgGrantAppRoleToBlockchainAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgGrantAppRoleToBlockchainAccount
    } as MsgGrantAppRoleToBlockchainAccount;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.user = reader.string();
          break;
        case 3:
          message.roleID = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgGrantAppRoleToBlockchainAccount {
    const message = {
      ...baseMsgGrantAppRoleToBlockchainAccount
    } as MsgGrantAppRoleToBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.user !== undefined && object.user !== null) {
      message.user = String(object.user);
    } else {
      message.user = "";
    }
    if (object.roleID !== undefined && object.roleID !== null) {
      message.roleID = String(object.roleID);
    } else {
      message.roleID = "";
    }
    return message;
  },

  toJSON(message: MsgGrantAppRoleToBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.user !== undefined && (obj.user = message.user);
    message.roleID !== undefined && (obj.roleID = message.roleID);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgGrantAppRoleToBlockchainAccount>
  ): MsgGrantAppRoleToBlockchainAccount {
    const message = {
      ...baseMsgGrantAppRoleToBlockchainAccount
    } as MsgGrantAppRoleToBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.user !== undefined && object.user !== null) {
      message.user = object.user;
    } else {
      message.user = "";
    }
    if (object.roleID !== undefined && object.roleID !== null) {
      message.roleID = object.roleID;
    } else {
      message.roleID = "";
    }
    return message;
  }
};

const baseMsgGrantAppRoleToBlockchainAccountResponse: object = {};

export const MsgGrantAppRoleToBlockchainAccountResponse = {
  encode(
    _: MsgGrantAppRoleToBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgGrantAppRoleToBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgGrantAppRoleToBlockchainAccountResponse
    } as MsgGrantAppRoleToBlockchainAccountResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgGrantAppRoleToBlockchainAccountResponse {
    const message = {
      ...baseMsgGrantAppRoleToBlockchainAccountResponse
    } as MsgGrantAppRoleToBlockchainAccountResponse;
    return message;
  },

  toJSON(_: MsgGrantAppRoleToBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgGrantAppRoleToBlockchainAccountResponse>
  ): MsgGrantAppRoleToBlockchainAccountResponse {
    const message = {
      ...baseMsgGrantAppRoleToBlockchainAccountResponse
    } as MsgGrantAppRoleToBlockchainAccountResponse;
    return message;
  }
};

const baseMsgRevokeAppRoleFromBlockchainAccount: object = {
  creator: "",
  account: "",
  roleID: ""
};

export const MsgRevokeAppRoleFromBlockchainAccount = {
  encode(
    message: MsgRevokeAppRoleFromBlockchainAccount,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.account !== "") {
      writer.uint32(18).string(message.account);
    }
    if (message.roleID !== "") {
      writer.uint32(26).string(message.roleID);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgRevokeAppRoleFromBlockchainAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgRevokeAppRoleFromBlockchainAccount
    } as MsgRevokeAppRoleFromBlockchainAccount;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.account = reader.string();
          break;
        case 3:
          message.roleID = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRevokeAppRoleFromBlockchainAccount {
    const message = {
      ...baseMsgRevokeAppRoleFromBlockchainAccount
    } as MsgRevokeAppRoleFromBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.account !== undefined && object.account !== null) {
      message.account = String(object.account);
    } else {
      message.account = "";
    }
    if (object.roleID !== undefined && object.roleID !== null) {
      message.roleID = String(object.roleID);
    } else {
      message.roleID = "";
    }
    return message;
  },

  toJSON(message: MsgRevokeAppRoleFromBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.account !== undefined && (obj.account = message.account);
    message.roleID !== undefined && (obj.roleID = message.roleID);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgRevokeAppRoleFromBlockchainAccount>
  ): MsgRevokeAppRoleFromBlockchainAccount {
    const message = {
      ...baseMsgRevokeAppRoleFromBlockchainAccount
    } as MsgRevokeAppRoleFromBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.account !== undefined && object.account !== null) {
      message.account = object.account;
    } else {
      message.account = "";
    }
    if (object.roleID !== undefined && object.roleID !== null) {
      message.roleID = object.roleID;
    } else {
      message.roleID = "";
    }
    return message;
  }
};

const baseMsgRevokeAppRoleFromBlockchainAccountResponse: object = {};

export const MsgRevokeAppRoleFromBlockchainAccountResponse = {
  encode(
    _: MsgRevokeAppRoleFromBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgRevokeAppRoleFromBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgRevokeAppRoleFromBlockchainAccountResponse
    } as MsgRevokeAppRoleFromBlockchainAccountResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgRevokeAppRoleFromBlockchainAccountResponse {
    const message = {
      ...baseMsgRevokeAppRoleFromBlockchainAccountResponse
    } as MsgRevokeAppRoleFromBlockchainAccountResponse;
    return message;
  },

  toJSON(_: MsgRevokeAppRoleFromBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgRevokeAppRoleFromBlockchainAccountResponse>
  ): MsgRevokeAppRoleFromBlockchainAccountResponse {
    const message = {
      ...baseMsgRevokeAppRoleFromBlockchainAccountResponse
    } as MsgRevokeAppRoleFromBlockchainAccountResponse;
    return message;
  }
};

const baseMsgFetchAllApplicationRole: object = { creator: "" };

export const MsgFetchAllApplicationRole = {
  encode(
    message: MsgFetchAllApplicationRole,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllApplicationRole {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllApplicationRole
    } as MsgFetchAllApplicationRole;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllApplicationRole {
    const message = {
      ...baseMsgFetchAllApplicationRole
    } as MsgFetchAllApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllApplicationRole>
  ): MsgFetchAllApplicationRole {
    const message = {
      ...baseMsgFetchAllApplicationRole
    } as MsgFetchAllApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchAllApplicationRoleResponse: object = {};

export const MsgFetchAllApplicationRoleResponse = {
  encode(
    message: MsgFetchAllApplicationRoleResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.role) {
      ApplicationRole.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllApplicationRoleResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllApplicationRoleResponse
    } as MsgFetchAllApplicationRoleResponse;
    message.role = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.role.push(ApplicationRole.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllApplicationRoleResponse {
    const message = {
      ...baseMsgFetchAllApplicationRoleResponse
    } as MsgFetchAllApplicationRoleResponse;
    message.role = [];
    if (object.role !== undefined && object.role !== null) {
      for (const e of object.role) {
        message.role.push(ApplicationRole.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgFetchAllApplicationRoleResponse): unknown {
    const obj: any = {};
    if (message.role) {
      obj.role = message.role.map((e) =>
        e ? ApplicationRole.toJSON(e) : undefined
      );
    } else {
      obj.role = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllApplicationRoleResponse>
  ): MsgFetchAllApplicationRoleResponse {
    const message = {
      ...baseMsgFetchAllApplicationRoleResponse
    } as MsgFetchAllApplicationRoleResponse;
    message.role = [];
    if (object.role !== undefined && object.role !== null) {
      for (const e of object.role) {
        message.role.push(ApplicationRole.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgFetchApplicationRole: object = { creator: "", id: "" };

export const MsgFetchApplicationRole = {
  encode(
    message: MsgFetchApplicationRole,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchApplicationRole {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchApplicationRole
    } as MsgFetchApplicationRole;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchApplicationRole {
    const message = {
      ...baseMsgFetchApplicationRole
    } as MsgFetchApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchApplicationRole>
  ): MsgFetchApplicationRole {
    const message = {
      ...baseMsgFetchApplicationRole
    } as MsgFetchApplicationRole;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchApplicationRoleResponse: object = {};

export const MsgFetchApplicationRoleResponse = {
  encode(
    message: MsgFetchApplicationRoleResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.role !== undefined) {
      ApplicationRole.encode(message.role, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchApplicationRoleResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchApplicationRoleResponse
    } as MsgFetchApplicationRoleResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.role = ApplicationRole.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchApplicationRoleResponse {
    const message = {
      ...baseMsgFetchApplicationRoleResponse
    } as MsgFetchApplicationRoleResponse;
    if (object.role !== undefined && object.role !== null) {
      message.role = ApplicationRole.fromJSON(object.role);
    } else {
      message.role = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchApplicationRoleResponse): unknown {
    const obj: any = {};
    message.role !== undefined &&
    (obj.role = message.role
      ? ApplicationRole.toJSON(message.role)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchApplicationRoleResponse>
  ): MsgFetchApplicationRoleResponse {
    const message = {
      ...baseMsgFetchApplicationRoleResponse
    } as MsgFetchApplicationRoleResponse;
    if (object.role !== undefined && object.role !== null) {
      message.role = ApplicationRole.fromPartial(object.role);
    } else {
      message.role = undefined;
    }
    return message;
  }
};

const baseMsgFetchAllBlockchainAccount: object = { creator: "" };

export const MsgFetchAllBlockchainAccount = {
  encode(
    message: MsgFetchAllBlockchainAccount,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllBlockchainAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllBlockchainAccount
    } as MsgFetchAllBlockchainAccount;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllBlockchainAccount {
    const message = {
      ...baseMsgFetchAllBlockchainAccount
    } as MsgFetchAllBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllBlockchainAccount>
  ): MsgFetchAllBlockchainAccount {
    const message = {
      ...baseMsgFetchAllBlockchainAccount
    } as MsgFetchAllBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchAllBlockchainAccountResponse: object = {};

export const MsgFetchAllBlockchainAccountResponse = {
  encode(
    message: MsgFetchAllBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.account) {
      BlockchainAccount.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllBlockchainAccountResponse
    } as MsgFetchAllBlockchainAccountResponse;
    message.account = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.account.push(
            BlockchainAccount.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllBlockchainAccountResponse {
    const message = {
      ...baseMsgFetchAllBlockchainAccountResponse
    } as MsgFetchAllBlockchainAccountResponse;
    message.account = [];
    if (object.account !== undefined && object.account !== null) {
      for (const e of object.account) {
        message.account.push(BlockchainAccount.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgFetchAllBlockchainAccountResponse): unknown {
    const obj: any = {};
    if (message.account) {
      obj.account = message.account.map((e) =>
        e ? BlockchainAccount.toJSON(e) : undefined
      );
    } else {
      obj.account = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllBlockchainAccountResponse>
  ): MsgFetchAllBlockchainAccountResponse {
    const message = {
      ...baseMsgFetchAllBlockchainAccountResponse
    } as MsgFetchAllBlockchainAccountResponse;
    message.account = [];
    if (object.account !== undefined && object.account !== null) {
      for (const e of object.account) {
        message.account.push(BlockchainAccount.fromPartial(e));
      }
    }
    return message;
  }
};

const baseMsgFetchBlockchainAccount: object = { creator: "", id: "" };

export const MsgFetchBlockchainAccount = {
  encode(
    message: MsgFetchBlockchainAccount,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchBlockchainAccount {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchBlockchainAccount
    } as MsgFetchBlockchainAccount;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchBlockchainAccount {
    const message = {
      ...baseMsgFetchBlockchainAccount
    } as MsgFetchBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchBlockchainAccount>
  ): MsgFetchBlockchainAccount {
    const message = {
      ...baseMsgFetchBlockchainAccount
    } as MsgFetchBlockchainAccount;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchBlockchainAccountResponse: object = {};

export const MsgFetchBlockchainAccountResponse = {
  encode(
    message: MsgFetchBlockchainAccountResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.account !== undefined) {
      BlockchainAccount.encode(
        message.account,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchBlockchainAccountResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchBlockchainAccountResponse
    } as MsgFetchBlockchainAccountResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.account = BlockchainAccount.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchBlockchainAccountResponse {
    const message = {
      ...baseMsgFetchBlockchainAccountResponse
    } as MsgFetchBlockchainAccountResponse;
    if (object.account !== undefined && object.account !== null) {
      message.account = BlockchainAccount.fromJSON(object.account);
    } else {
      message.account = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchBlockchainAccountResponse): unknown {
    const obj: any = {};
    message.account !== undefined &&
    (obj.account = message.account
      ? BlockchainAccount.toJSON(message.account)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchBlockchainAccountResponse>
  ): MsgFetchBlockchainAccountResponse {
    const message = {
      ...baseMsgFetchBlockchainAccountResponse
    } as MsgFetchBlockchainAccountResponse;
    if (object.account !== undefined && object.account !== null) {
      message.account = BlockchainAccount.fromPartial(object.account);
    } else {
      message.account = undefined;
    }
    return message;
  }
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  CreateBlockchainAccount(
    request: MsgCreateBlockchainAccount
  ): Promise<MsgCreateBlockchainAccountResponse>;

  UpdateBlockchainAccount(
    request: MsgUpdateBlockchainAccount
  ): Promise<MsgUpdateBlockchainAccountResponse>;

  CreateApplicationRole(
    request: MsgCreateApplicationRole
  ): Promise<MsgCreateApplicationRoleResponse>;

  UpdateApplicationRole(
    request: MsgUpdateApplicationRole
  ): Promise<MsgUpdateApplicationRoleResponse>;

  UpdateConfiguration(
    request: MsgUpdateConfiguration
  ): Promise<MsgUpdateConfigurationResponse>;

  DeactivateApplicationRole(
    request: MsgDeactivateApplicationRole
  ): Promise<MsgDeactivateApplicationRoleResponse>;

  DeactivateBlockchainAccount(
    request: MsgDeactivateBlockchainAccount
  ): Promise<MsgDeactivateBlockchainAccountResponse>;

  GrantAppRoleToBlockchainAccount(
    request: MsgGrantAppRoleToBlockchainAccount
  ): Promise<MsgGrantAppRoleToBlockchainAccountResponse>;

  RevokeAppRoleFromBlockchainAccount(
    request: MsgRevokeAppRoleFromBlockchainAccount
  ): Promise<MsgRevokeAppRoleFromBlockchainAccountResponse>;

  FetchAllApplicationRole(
    request: MsgFetchAllApplicationRole
  ): Promise<MsgFetchAllApplicationRoleResponse>;

  FetchApplicationRole(
    request: MsgFetchApplicationRole
  ): Promise<MsgFetchApplicationRoleResponse>;

  FetchAllBlockchainAccount(
    request: MsgFetchAllBlockchainAccount
  ): Promise<MsgFetchAllBlockchainAccountResponse>;

  FetchBlockchainAccount(
    request: MsgFetchBlockchainAccount
  ): Promise<MsgFetchBlockchainAccountResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }

  CreateBlockchainAccount(
    request: MsgCreateBlockchainAccount
  ): Promise<MsgCreateBlockchainAccountResponse> {
    const data = MsgCreateBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "CreateBlockchainAccount",
      data
    );
    return promise.then((data) =>
      MsgCreateBlockchainAccountResponse.decode(new Reader(data))
    );
  }

  UpdateBlockchainAccount(
    request: MsgUpdateBlockchainAccount
  ): Promise<MsgUpdateBlockchainAccountResponse> {
    const data = MsgUpdateBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "UpdateBlockchainAccount",
      data
    );
    return promise.then((data) =>
      MsgUpdateBlockchainAccountResponse.decode(new Reader(data))
    );
  }

  CreateApplicationRole(
    request: MsgCreateApplicationRole
  ): Promise<MsgCreateApplicationRoleResponse> {
    const data = MsgCreateApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "CreateApplicationRole",
      data
    );
    return promise.then((data) =>
      MsgCreateApplicationRoleResponse.decode(new Reader(data))
    );
  }

  UpdateApplicationRole(
    request: MsgUpdateApplicationRole
  ): Promise<MsgUpdateApplicationRoleResponse> {
    const data = MsgUpdateApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "UpdateApplicationRole",
      data
    );
    return promise.then((data) =>
      MsgUpdateApplicationRoleResponse.decode(new Reader(data))
    );
  }

  UpdateConfiguration(
    request: MsgUpdateConfiguration
  ): Promise<MsgUpdateConfigurationResponse> {
    const data = MsgUpdateConfiguration.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "UpdateConfiguration",
      data
    );
    return promise.then((data) =>
      MsgUpdateConfigurationResponse.decode(new Reader(data))
    );
  }

  DeactivateApplicationRole(
    request: MsgDeactivateApplicationRole
  ): Promise<MsgDeactivateApplicationRoleResponse> {
    const data = MsgDeactivateApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "DeactivateApplicationRole",
      data
    );
    return promise.then((data) =>
      MsgDeactivateApplicationRoleResponse.decode(new Reader(data))
    );
  }

  DeactivateBlockchainAccount(
    request: MsgDeactivateBlockchainAccount
  ): Promise<MsgDeactivateBlockchainAccountResponse> {
    const data = MsgDeactivateBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "DeactivateBlockchainAccount",
      data
    );
    return promise.then((data) =>
      MsgDeactivateBlockchainAccountResponse.decode(new Reader(data))
    );
  }

  GrantAppRoleToBlockchainAccount(
    request: MsgGrantAppRoleToBlockchainAccount
  ): Promise<MsgGrantAppRoleToBlockchainAccountResponse> {
    const data = MsgGrantAppRoleToBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "GrantAppRoleToBlockchainAccount",
      data
    );
    return promise.then((data) =>
      MsgGrantAppRoleToBlockchainAccountResponse.decode(new Reader(data))
    );
  }

  RevokeAppRoleFromBlockchainAccount(
    request: MsgRevokeAppRoleFromBlockchainAccount
  ): Promise<MsgRevokeAppRoleFromBlockchainAccountResponse> {
    const data = MsgRevokeAppRoleFromBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "RevokeAppRoleFromBlockchainAccount",
      data
    );
    return promise.then((data) =>
      MsgRevokeAppRoleFromBlockchainAccountResponse.decode(new Reader(data))
    );
  }

  FetchAllApplicationRole(
    request: MsgFetchAllApplicationRole
  ): Promise<MsgFetchAllApplicationRoleResponse> {
    const data = MsgFetchAllApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchAllApplicationRole",
      data
    );
    return promise.then((data) =>
      MsgFetchAllApplicationRoleResponse.decode(new Reader(data))
    );
  }

  FetchApplicationRole(
    request: MsgFetchApplicationRole
  ): Promise<MsgFetchApplicationRoleResponse> {
    const data = MsgFetchApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchApplicationRole",
      data
    );
    return promise.then((data) =>
      MsgFetchApplicationRoleResponse.decode(new Reader(data))
    );
  }

  FetchAllBlockchainAccount(
    request: MsgFetchAllBlockchainAccount
  ): Promise<MsgFetchAllBlockchainAccountResponse> {
    const data = MsgFetchAllBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchAllBlockchainAccount",
      data
    );
    return promise.then((data) =>
      MsgFetchAllBlockchainAccountResponse.decode(new Reader(data))
    );
  }

  FetchBlockchainAccount(
    request: MsgFetchBlockchainAccount
  ): Promise<MsgFetchBlockchainAccountResponse> {
    const data = MsgFetchBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchBlockchainAccount",
      data
    );
    return promise.then((data) =>
      MsgFetchBlockchainAccountResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
