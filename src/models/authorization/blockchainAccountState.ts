// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

export interface BlockchainAccountState {
  creator: string;
  id: string;
  accountID: string;
  timeStamp: string;
  applicationRoleIDs: string[];
  valid: boolean;
}

const baseBlockchainAccountState: object = {
  creator: "",
  id: "",
  accountID: "",
  timeStamp: "",
  applicationRoleIDs: "",
  valid: false
};

export const BlockchainAccountState = {
  encode(
    message: BlockchainAccountState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.accountID !== "") {
      writer.uint32(26).string(message.accountID);
    }
    if (message.timeStamp !== "") {
      writer.uint32(34).string(message.timeStamp);
    }
    for (const v of message.applicationRoleIDs) {
      writer.uint32(42).string(v!);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): BlockchainAccountState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseBlockchainAccountState } as BlockchainAccountState;
    message.applicationRoleIDs = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.accountID = reader.string();
          break;
        case 4:
          message.timeStamp = reader.string();
          break;
        case 5:
          message.applicationRoleIDs.push(reader.string());
          break;
        case 6:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BlockchainAccountState {
    const message = { ...baseBlockchainAccountState } as BlockchainAccountState;
    message.applicationRoleIDs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.accountID !== undefined && object.accountID !== null) {
      message.accountID = String(object.accountID);
    } else {
      message.accountID = "";
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = String(object.timeStamp);
    } else {
      message.timeStamp = "";
    }
    if (
      object.applicationRoleIDs !== undefined &&
      object.applicationRoleIDs !== null
    ) {
      for (const e of object.applicationRoleIDs) {
        message.applicationRoleIDs.push(String(e));
      }
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    return message;
  },

  toJSON(message: BlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.accountID !== undefined && (obj.accountID = message.accountID);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    if (message.applicationRoleIDs) {
      obj.applicationRoleIDs = message.applicationRoleIDs.map((e) => e);
    } else {
      obj.applicationRoleIDs = [];
    }
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial(
    object: DeepPartial<BlockchainAccountState>
  ): BlockchainAccountState {
    const message = { ...baseBlockchainAccountState } as BlockchainAccountState;
    message.applicationRoleIDs = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.accountID !== undefined && object.accountID !== null) {
      message.accountID = object.accountID;
    } else {
      message.accountID = "";
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = object.timeStamp;
    } else {
      message.timeStamp = "";
    }
    if (
      object.applicationRoleIDs !== undefined &&
      object.applicationRoleIDs !== null
    ) {
      for (const e of object.applicationRoleIDs) {
        message.applicationRoleIDs.push(e);
      }
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
