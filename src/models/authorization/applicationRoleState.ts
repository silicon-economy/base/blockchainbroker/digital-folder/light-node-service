// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

export interface ApplicationRoleState {
  creator: string;
  id: string;
  applicationRoleID: string;
  description: string;
  valid: boolean;
  timeStamp: string;
}

const baseApplicationRoleState: object = {
  creator: "",
  id: "",
  applicationRoleID: "",
  description: "",
  valid: false,
  timeStamp: ""
};

export const ApplicationRoleState = {
  encode(
    message: ApplicationRoleState,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.applicationRoleID !== "") {
      writer.uint32(26).string(message.applicationRoleID);
    }
    if (message.description !== "") {
      writer.uint32(34).string(message.description);
    }
    if (message.valid === true) {
      writer.uint32(40).bool(message.valid);
    }
    if (message.timeStamp !== "") {
      writer.uint32(50).string(message.timeStamp);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): ApplicationRoleState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseApplicationRoleState } as ApplicationRoleState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleID = reader.string();
          break;
        case 4:
          message.description = reader.string();
          break;
        case 5:
          message.valid = reader.bool();
          break;
        case 6:
          message.timeStamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ApplicationRoleState {
    const message = { ...baseApplicationRoleState } as ApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleID !== undefined &&
      object.applicationRoleID !== null
    ) {
      message.applicationRoleID = String(object.applicationRoleID);
    } else {
      message.applicationRoleID = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    } else {
      message.description = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = Boolean(object.valid);
    } else {
      message.valid = false;
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = String(object.timeStamp);
    } else {
      message.timeStamp = "";
    }
    return message;
  },

  toJSON(message: ApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.applicationRoleID !== undefined &&
    (obj.applicationRoleID = message.applicationRoleID);
    message.description !== undefined &&
    (obj.description = message.description);
    message.valid !== undefined && (obj.valid = message.valid);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    return obj;
  },

  fromPartial(object: DeepPartial<ApplicationRoleState>): ApplicationRoleState {
    const message = { ...baseApplicationRoleState } as ApplicationRoleState;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (
      object.applicationRoleID !== undefined &&
      object.applicationRoleID !== null
    ) {
      message.applicationRoleID = object.applicationRoleID;
    } else {
      message.applicationRoleID = "";
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    } else {
      message.description = "";
    }
    if (object.valid !== undefined && object.valid !== null) {
      message.valid = object.valid;
    } else {
      message.valid = false;
    }
    if (object.timeStamp !== undefined && object.timeStamp !== null) {
      message.timeStamp = object.timeStamp;
    } else {
      message.timeStamp = "";
    }
    return message;
  }
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
