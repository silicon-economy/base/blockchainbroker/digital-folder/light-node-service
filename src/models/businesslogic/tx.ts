// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";
import { TokenHistory } from "../token/tokenHistory";
import { Token } from "../token/token";
import {
  MsgEmptyResponse as MsgEmptyResponse1,
  MsgFetchAllSegmentHistoryResponse,
  MsgFetchAllSegmentResponse,
  MsgFetchAllTokenHistoryGlobalResponse,
  MsgFetchAllWalletHistoryResponse,
  MsgFetchAllWalletResponse,
  MsgFetchGetSegmentHistoryResponse,
  MsgFetchGetSegmentResponse,
  MsgFetchGetTokenHistoryGlobalResponse,
  MsgFetchGetWalletHistoryResponse,
  MsgFetchGetWalletResponse,
  MsgIdResponse as MsgIdResponse2
} from "../wallet/tx";
import { QueryAllTokenResponse, QueryGetDocumentHashResponse } from "../businesslogic/query";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic";

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgFetchAllSegmentHistory {
  creator: string;
}

export interface MsgFetchGetWallet {
  creator: string;
  id: string;
}

export interface MsgFetchGetSegment {
  creator: string;
  id: string;
}

export interface MsgFetchAllSegment {
  creator: string;
}

export interface MsgUpdateWallet {
  creator: string;
  id: string;
  name: string;
}

export interface MsgCreateWallet {
  creator: string;
  name: string;
}

export interface MsgUpdateSegment {
  creator: string;
  id: string;
  name: string;
  info: string;
}

export interface MsgFetchGetWalletHistory {
  creator: string;
  id: string;
}

export interface MsgCreateSegment {
  creator: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgFetchAllTokenHistoryGlobal {
  creator: string;
}

export interface MsgFetchGetTokenHistoryGlobal {
  creator: string;
  id: string;
}

export interface MsgCreateWalletWithId {
  creator: string;
  id: string;
  name: string;
}

export interface MsgMoveTokenToSegment {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}

export interface MsgFetchAllWalletHistory {
  creator: string;
}

export interface MsgFetchAllWallet {
  creator: string;
}

export interface MsgFetchSegmentHistory {
  creator: string;
  id: string;
}

export interface MsgCreateSegmentWithId {
  creator: string;
  id: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgFetchTokensByWalletId {
  creator: string;
  id: string;
}

export interface MsgFetchTokensBySegmentId {
  creator: string;
  id: string;
}

export interface MsgFetchDocumentHash {
  creator: string;
  id: string;
}

export interface MsgCreateTokenCopies {
  creator: string;
  index: string;
  tokens: string[];
}

export interface MsgCreateTokenCopiesResponse {
}

export interface MsgUpdateTokenCopies {
  creator: string;
  index: string;
  tokens: string[];
}

export interface MsgUpdateTokenCopiesResponse {
}

export interface MsgDeleteTokenCopies {
  creator: string;
  index: string;
}

export interface MsgDeleteTokenCopiesResponse {
}

export interface MsgCreateDocumentTokenMapper {
  creator: string;
  documentId: string;
  tokenId: string;
}

export interface MsgCreateDocumentTokenMapperResponse {
}

export interface MsgUpdateDocumentTokenMapper {
  creator: string;
  documentId: string;
  tokenId: string;
}

export interface MsgUpdateDocumentTokenMapperResponse {
}

export interface MsgDeleteDocumentTokenMapper {
  creator: string;
  index: string;
}

export interface MsgDeleteDocumentTokenMapperResponse {
}

export interface MsgMoveTokenToWallet {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}

export interface MsgCreateToken {
  creator: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
  moduleRef: string;
}

export interface MsgUpdateToken {
  creator: string;
  tokenRefId: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
  moduleRef: string;
}

export interface MsgIdResponse {
  id: string;
}

export interface MsgActivateToken {
  creator: string;
  id: string;
  segmentId: string;
  moduleRef: string;
}

export interface MsgDeactivateToken {
  creator: string;
  id: string;
}

export interface MsgCreateHashToken {
  creator: string;
  changeMessage: string;
  segmentId: string;
  document: string;
  hash: string;
  hashFunction: string;
  metadata: string;
}

export interface MsgCloneToken {
  creator: string;
  tokenId: string;
  walletId: string;
  moduleRef: string;
}

export interface MsgRevertToGenesis {
  creator: string;
}

export interface MsgUpdateTokenInformation {
  creator: string;
  tokenId: string;
  data: string;
}

export interface MsgFetchTokenHistory {
  creator: string;
  id: string;
}

export interface MsgFetchTokenHistoryResponse {
  TokenHistory: TokenHistory | undefined;
}

export interface MsgFetchToken {
  creator: string;
  id: string;
}

export interface MsgFetchTokenResponse {
  Token: Token | undefined;
}

export interface MsgEmptyResponse {
}

const baseMsgFetchAllSegmentHistory: object = { creator: "" };

export const MsgFetchAllSegmentHistory = {
  encode(
    message: MsgFetchAllSegmentHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllSegmentHistory
    } as MsgFetchAllSegmentHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentHistory {
    const message = {
      ...baseMsgFetchAllSegmentHistory
    } as MsgFetchAllSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentHistory>
  ): MsgFetchAllSegmentHistory {
    const message = {
      ...baseMsgFetchAllSegmentHistory
    } as MsgFetchAllSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchGetWallet: object = { creator: "", id: "" };

export const MsgFetchGetWallet = {
  encode(message: MsgFetchGetWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchGetWallet } as MsgFetchGetWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWallet {
    const message = { ...baseMsgFetchGetWallet } as MsgFetchGetWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchGetWallet>): MsgFetchGetWallet {
    const message = { ...baseMsgFetchGetWallet } as MsgFetchGetWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchGetSegment: object = { creator: "", id: "" };

export const MsgFetchGetSegment = {
  encode(
    message: MsgFetchGetSegment,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchGetSegment } as MsgFetchGetSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegment {
    const message = { ...baseMsgFetchGetSegment } as MsgFetchGetSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchGetSegment>): MsgFetchGetSegment {
    const message = { ...baseMsgFetchGetSegment } as MsgFetchGetSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchAllSegment: object = { creator: "" };

export const MsgFetchAllSegment = {
  encode(
    message: MsgFetchAllSegment,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchAllSegment } as MsgFetchAllSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegment {
    const message = { ...baseMsgFetchAllSegment } as MsgFetchAllSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchAllSegment>): MsgFetchAllSegment {
    const message = { ...baseMsgFetchAllSegment } as MsgFetchAllSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgUpdateWallet: object = { creator: "", id: "", name: "" };

export const MsgUpdateWallet = {
  encode(message: MsgUpdateWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateWallet } as MsgUpdateWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateWallet {
    const message = { ...baseMsgUpdateWallet } as MsgUpdateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateWallet>): MsgUpdateWallet {
    const message = { ...baseMsgUpdateWallet } as MsgUpdateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    return message;
  }
};

const baseMsgCreateWallet: object = { creator: "", name: "" };

export const MsgCreateWallet = {
  encode(message: MsgCreateWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateWallet } as MsgCreateWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWallet {
    const message = { ...baseMsgCreateWallet } as MsgCreateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    return message;
  },

  toJSON(message: MsgCreateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateWallet>): MsgCreateWallet {
    const message = { ...baseMsgCreateWallet } as MsgCreateWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    return message;
  }
};

const baseMsgUpdateSegment: object = {
  creator: "",
  id: "",
  name: "",
  info: ""
};

export const MsgUpdateSegment = {
  encode(message: MsgUpdateSegment, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateSegment } as MsgUpdateSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateSegment {
    const message = { ...baseMsgUpdateSegment } as MsgUpdateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = String(object.info);
    } else {
      message.info = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateSegment>): MsgUpdateSegment {
    const message = { ...baseMsgUpdateSegment } as MsgUpdateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = object.info;
    } else {
      message.info = "";
    }
    return message;
  }
};

const baseMsgFetchGetWalletHistory: object = { creator: "", id: "" };

export const MsgFetchGetWalletHistory = {
  encode(
    message: MsgFetchGetWalletHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetWalletHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetWalletHistory
    } as MsgFetchGetWalletHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletHistory {
    const message = {
      ...baseMsgFetchGetWalletHistory
    } as MsgFetchGetWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetWalletHistory>
  ): MsgFetchGetWalletHistory {
    const message = {
      ...baseMsgFetchGetWalletHistory
    } as MsgFetchGetWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateSegment: object = {
  creator: "",
  name: "",
  info: "",
  walletId: ""
};

export const MsgCreateSegment = {
  encode(message: MsgCreateSegment, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(26).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(34).string(message.walletId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateSegment } as MsgCreateSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.info = reader.string();
          break;
        case 4:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegment {
    const message = { ...baseMsgCreateSegment } as MsgCreateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = String(object.info);
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = String(object.walletId);
    } else {
      message.walletId = "";
    }
    return message;
  },

  toJSON(message: MsgCreateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateSegment>): MsgCreateSegment {
    const message = { ...baseMsgCreateSegment } as MsgCreateSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = object.info;
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = object.walletId;
    } else {
      message.walletId = "";
    }
    return message;
  }
};

const baseMsgFetchAllTokenHistoryGlobal: object = { creator: "" };

export const MsgFetchAllTokenHistoryGlobal = {
  encode(
    message: MsgFetchAllTokenHistoryGlobal,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenHistoryGlobal {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobal
    } as MsgFetchAllTokenHistoryGlobal;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobal
    } as MsgFetchAllTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistoryGlobal>
  ): MsgFetchAllTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchAllTokenHistoryGlobal
    } as MsgFetchAllTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchGetTokenHistoryGlobal: object = { creator: "", id: "" };

export const MsgFetchGetTokenHistoryGlobal = {
  encode(
    message: MsgFetchGetTokenHistoryGlobal,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetTokenHistoryGlobal {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobal
    } as MsgFetchGetTokenHistoryGlobal;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobal
    } as MsgFetchGetTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchGetTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchGetTokenHistoryGlobal>
  ): MsgFetchGetTokenHistoryGlobal {
    const message = {
      ...baseMsgFetchGetTokenHistoryGlobal
    } as MsgFetchGetTokenHistoryGlobal;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateWalletWithId: object = { creator: "", id: "", name: "" };

export const MsgCreateWalletWithId = {
  encode(
    message: MsgCreateWalletWithId,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateWalletWithId {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateWalletWithId } as MsgCreateWalletWithId;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWalletWithId {
    const message = { ...baseMsgCreateWalletWithId } as MsgCreateWalletWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    return message;
  },

  toJSON(message: MsgCreateWalletWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateWalletWithId>
  ): MsgCreateWalletWithId {
    const message = { ...baseMsgCreateWalletWithId } as MsgCreateWalletWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    return message;
  }
};

const baseMsgMoveTokenToSegment: object = {
  creator: "",
  tokenRefId: "",
  sourceSegmentId: "",
  targetSegmentId: ""
};

export const MsgMoveTokenToSegment = {
  encode(
    message: MsgMoveTokenToSegment,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetSegmentId !== "") {
      writer.uint32(34).string(message.targetSegmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToSegment {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgMoveTokenToSegment } as MsgMoveTokenToSegment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetSegmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToSegment {
    const message = { ...baseMsgMoveTokenToSegment } as MsgMoveTokenToSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = String(object.tokenRefId);
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = String(object.sourceSegmentId);
    } else {
      message.sourceSegmentId = "";
    }
    if (
      object.targetSegmentId !== undefined &&
      object.targetSegmentId !== null
    ) {
      message.targetSegmentId = String(object.targetSegmentId);
    } else {
      message.targetSegmentId = "";
    }
    return message;
  },

  toJSON(message: MsgMoveTokenToSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined &&
    (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetSegmentId !== undefined &&
    (obj.targetSegmentId = message.targetSegmentId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgMoveTokenToSegment>
  ): MsgMoveTokenToSegment {
    const message = { ...baseMsgMoveTokenToSegment } as MsgMoveTokenToSegment;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = object.tokenRefId;
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = object.sourceSegmentId;
    } else {
      message.sourceSegmentId = "";
    }
    if (
      object.targetSegmentId !== undefined &&
      object.targetSegmentId !== null
    ) {
      message.targetSegmentId = object.targetSegmentId;
    } else {
      message.targetSegmentId = "";
    }
    return message;
  }
};

const baseMsgFetchAllWalletHistory: object = { creator: "" };

export const MsgFetchAllWalletHistory = {
  encode(
    message: MsgFetchAllWalletHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllWalletHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchAllWalletHistory
    } as MsgFetchAllWalletHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletHistory {
    const message = {
      ...baseMsgFetchAllWalletHistory
    } as MsgFetchAllWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchAllWalletHistory>
  ): MsgFetchAllWalletHistory {
    const message = {
      ...baseMsgFetchAllWalletHistory
    } as MsgFetchAllWalletHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchAllWallet: object = { creator: "" };

export const MsgFetchAllWallet = {
  encode(message: MsgFetchAllWallet, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchAllWallet } as MsgFetchAllWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWallet {
    const message = { ...baseMsgFetchAllWallet } as MsgFetchAllWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgFetchAllWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchAllWallet>): MsgFetchAllWallet {
    const message = { ...baseMsgFetchAllWallet } as MsgFetchAllWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgFetchSegmentHistory: object = { creator: "", id: "" };

export const MsgFetchSegmentHistory = {
  encode(
    message: MsgFetchSegmentHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchSegmentHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchSegmentHistory } as MsgFetchSegmentHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchSegmentHistory {
    const message = { ...baseMsgFetchSegmentHistory } as MsgFetchSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchSegmentHistory>
  ): MsgFetchSegmentHistory {
    const message = { ...baseMsgFetchSegmentHistory } as MsgFetchSegmentHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateSegmentWithId: object = {
  creator: "",
  id: "",
  name: "",
  info: "",
  walletId: ""
};

export const MsgCreateSegmentWithId = {
  encode(
    message: MsgCreateSegmentWithId,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(42).string(message.walletId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegmentWithId {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateSegmentWithId } as MsgCreateSegmentWithId;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        case 5:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegmentWithId {
    const message = { ...baseMsgCreateSegmentWithId } as MsgCreateSegmentWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = String(object.info);
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = String(object.walletId);
    } else {
      message.walletId = "";
    }
    return message;
  },

  toJSON(message: MsgCreateSegmentWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateSegmentWithId>
  ): MsgCreateSegmentWithId {
    const message = { ...baseMsgCreateSegmentWithId } as MsgCreateSegmentWithId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = "";
    }
    if (object.info !== undefined && object.info !== null) {
      message.info = object.info;
    } else {
      message.info = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = object.walletId;
    } else {
      message.walletId = "";
    }
    return message;
  }
};

const baseMsgFetchTokensByWalletId: object = { creator: "", id: "" };

export const MsgFetchTokensByWalletId = {
  encode(
    message: MsgFetchTokensByWalletId,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchTokensByWalletId {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchTokensByWalletId
    } as MsgFetchTokensByWalletId;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokensByWalletId {
    const message = {
      ...baseMsgFetchTokensByWalletId
    } as MsgFetchTokensByWalletId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchTokensByWalletId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchTokensByWalletId>
  ): MsgFetchTokensByWalletId {
    const message = {
      ...baseMsgFetchTokensByWalletId
    } as MsgFetchTokensByWalletId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchTokensBySegmentId: object = { creator: "", id: "" };

export const MsgFetchTokensBySegmentId = {
  encode(
    message: MsgFetchTokensBySegmentId,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchTokensBySegmentId {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchTokensBySegmentId
    } as MsgFetchTokensBySegmentId;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokensBySegmentId {
    const message = {
      ...baseMsgFetchTokensBySegmentId
    } as MsgFetchTokensBySegmentId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchTokensBySegmentId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchTokensBySegmentId>
  ): MsgFetchTokensBySegmentId {
    const message = {
      ...baseMsgFetchTokensBySegmentId
    } as MsgFetchTokensBySegmentId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchDocumentHash: object = { creator: "", id: "" };

export const MsgFetchDocumentHash = {
  encode(
    message: MsgFetchDocumentHash,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchDocumentHash {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchDocumentHash } as MsgFetchDocumentHash;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchDocumentHash {
    const message = { ...baseMsgFetchDocumentHash } as MsgFetchDocumentHash;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchDocumentHash): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchDocumentHash>): MsgFetchDocumentHash {
    const message = { ...baseMsgFetchDocumentHash } as MsgFetchDocumentHash;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateTokenCopies: object = { creator: "", index: "", tokens: "" };

export const MsgCreateTokenCopies = {
  encode(
    message: MsgCreateTokenCopies,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    for (const v of message.tokens) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateTokenCopies {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateTokenCopies } as MsgCreateTokenCopies;
    message.tokens = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.tokens.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenCopies {
    const message = { ...baseMsgCreateTokenCopies } as MsgCreateTokenCopies;
    message.tokens = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = String(object.index);
    } else {
      message.index = "";
    }
    if (object.tokens !== undefined && object.tokens !== null) {
      for (const e of object.tokens) {
        message.tokens.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: MsgCreateTokenCopies): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateTokenCopies>): MsgCreateTokenCopies {
    const message = { ...baseMsgCreateTokenCopies } as MsgCreateTokenCopies;
    message.tokens = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = object.index;
    } else {
      message.index = "";
    }
    if (object.tokens !== undefined && object.tokens !== null) {
      for (const e of object.tokens) {
        message.tokens.push(e);
      }
    }
    return message;
  }
};

const baseMsgCreateTokenCopiesResponse: object = {};

export const MsgCreateTokenCopiesResponse = {
  encode(
    _: MsgCreateTokenCopiesResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateTokenCopiesResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateTokenCopiesResponse
    } as MsgCreateTokenCopiesResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateTokenCopiesResponse {
    const message = {
      ...baseMsgCreateTokenCopiesResponse
    } as MsgCreateTokenCopiesResponse;
    return message;
  },

  toJSON(_: MsgCreateTokenCopiesResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgCreateTokenCopiesResponse>
  ): MsgCreateTokenCopiesResponse {
    const message = {
      ...baseMsgCreateTokenCopiesResponse
    } as MsgCreateTokenCopiesResponse;
    return message;
  }
};

const baseMsgUpdateTokenCopies: object = { creator: "", index: "", tokens: "" };

export const MsgUpdateTokenCopies = {
  encode(
    message: MsgUpdateTokenCopies,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    for (const v of message.tokens) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateTokenCopies {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateTokenCopies } as MsgUpdateTokenCopies;
    message.tokens = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.tokens.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenCopies {
    const message = { ...baseMsgUpdateTokenCopies } as MsgUpdateTokenCopies;
    message.tokens = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = String(object.index);
    } else {
      message.index = "";
    }
    if (object.tokens !== undefined && object.tokens !== null) {
      for (const e of object.tokens) {
        message.tokens.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: MsgUpdateTokenCopies): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateTokenCopies>): MsgUpdateTokenCopies {
    const message = { ...baseMsgUpdateTokenCopies } as MsgUpdateTokenCopies;
    message.tokens = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = object.index;
    } else {
      message.index = "";
    }
    if (object.tokens !== undefined && object.tokens !== null) {
      for (const e of object.tokens) {
        message.tokens.push(e);
      }
    }
    return message;
  }
};

const baseMsgUpdateTokenCopiesResponse: object = {};

export const MsgUpdateTokenCopiesResponse = {
  encode(
    _: MsgUpdateTokenCopiesResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateTokenCopiesResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateTokenCopiesResponse
    } as MsgUpdateTokenCopiesResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateTokenCopiesResponse {
    const message = {
      ...baseMsgUpdateTokenCopiesResponse
    } as MsgUpdateTokenCopiesResponse;
    return message;
  },

  toJSON(_: MsgUpdateTokenCopiesResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgUpdateTokenCopiesResponse>
  ): MsgUpdateTokenCopiesResponse {
    const message = {
      ...baseMsgUpdateTokenCopiesResponse
    } as MsgUpdateTokenCopiesResponse;
    return message;
  }
};

const baseMsgDeleteTokenCopies: object = { creator: "", index: "" };

export const MsgDeleteTokenCopies = {
  encode(
    message: MsgDeleteTokenCopies,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteTokenCopies {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDeleteTokenCopies } as MsgDeleteTokenCopies;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteTokenCopies {
    const message = { ...baseMsgDeleteTokenCopies } as MsgDeleteTokenCopies;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = String(object.index);
    } else {
      message.index = "";
    }
    return message;
  },

  toJSON(message: MsgDeleteTokenCopies): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgDeleteTokenCopies>): MsgDeleteTokenCopies {
    const message = { ...baseMsgDeleteTokenCopies } as MsgDeleteTokenCopies;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = object.index;
    } else {
      message.index = "";
    }
    return message;
  }
};

const baseMsgDeleteTokenCopiesResponse: object = {};

export const MsgDeleteTokenCopiesResponse = {
  encode(
    _: MsgDeleteTokenCopiesResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteTokenCopiesResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteTokenCopiesResponse
    } as MsgDeleteTokenCopiesResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteTokenCopiesResponse {
    const message = {
      ...baseMsgDeleteTokenCopiesResponse
    } as MsgDeleteTokenCopiesResponse;
    return message;
  },

  toJSON(_: MsgDeleteTokenCopiesResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeleteTokenCopiesResponse>
  ): MsgDeleteTokenCopiesResponse {
    const message = {
      ...baseMsgDeleteTokenCopiesResponse
    } as MsgDeleteTokenCopiesResponse;
    return message;
  }
};

const baseMsgCreateDocumentTokenMapper: object = {
  creator: "",
  documentId: "",
  tokenId: ""
};

export const MsgCreateDocumentTokenMapper = {
  encode(
    message: MsgCreateDocumentTokenMapper,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.documentId !== "") {
      writer.uint32(18).string(message.documentId);
    }
    if (message.tokenId !== "") {
      writer.uint32(26).string(message.tokenId);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateDocumentTokenMapper {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateDocumentTokenMapper
    } as MsgCreateDocumentTokenMapper;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.documentId = reader.string();
          break;
        case 3:
          message.tokenId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateDocumentTokenMapper {
    const message = {
      ...baseMsgCreateDocumentTokenMapper
    } as MsgCreateDocumentTokenMapper;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.documentId !== undefined && object.documentId !== null) {
      message.documentId = String(object.documentId);
    } else {
      message.documentId = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = String(object.tokenId);
    } else {
      message.tokenId = "";
    }
    return message;
  },

  toJSON(message: MsgCreateDocumentTokenMapper): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.documentId !== undefined && (obj.documentId = message.documentId);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateDocumentTokenMapper>
  ): MsgCreateDocumentTokenMapper {
    const message = {
      ...baseMsgCreateDocumentTokenMapper
    } as MsgCreateDocumentTokenMapper;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.documentId !== undefined && object.documentId !== null) {
      message.documentId = object.documentId;
    } else {
      message.documentId = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = object.tokenId;
    } else {
      message.tokenId = "";
    }
    return message;
  }
};

const baseMsgCreateDocumentTokenMapperResponse: object = {};

export const MsgCreateDocumentTokenMapperResponse = {
  encode(
    _: MsgCreateDocumentTokenMapperResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateDocumentTokenMapperResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateDocumentTokenMapperResponse
    } as MsgCreateDocumentTokenMapperResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateDocumentTokenMapperResponse {
    const message = {
      ...baseMsgCreateDocumentTokenMapperResponse
    } as MsgCreateDocumentTokenMapperResponse;
    return message;
  },

  toJSON(_: MsgCreateDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgCreateDocumentTokenMapperResponse>
  ): MsgCreateDocumentTokenMapperResponse {
    const message = {
      ...baseMsgCreateDocumentTokenMapperResponse
    } as MsgCreateDocumentTokenMapperResponse;
    return message;
  }
};

const baseMsgUpdateDocumentTokenMapper: object = {
  creator: "",
  documentId: "",
  tokenId: ""
};

export const MsgUpdateDocumentTokenMapper = {
  encode(
    message: MsgUpdateDocumentTokenMapper,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.documentId !== "") {
      writer.uint32(18).string(message.documentId);
    }
    if (message.tokenId !== "") {
      writer.uint32(26).string(message.tokenId);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateDocumentTokenMapper {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateDocumentTokenMapper
    } as MsgUpdateDocumentTokenMapper;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.documentId = reader.string();
          break;
        case 3:
          message.tokenId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateDocumentTokenMapper {
    const message = {
      ...baseMsgUpdateDocumentTokenMapper
    } as MsgUpdateDocumentTokenMapper;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.documentId !== undefined && object.documentId !== null) {
      message.documentId = String(object.documentId);
    } else {
      message.documentId = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = String(object.tokenId);
    } else {
      message.tokenId = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateDocumentTokenMapper): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.documentId !== undefined && (obj.documentId = message.documentId);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateDocumentTokenMapper>
  ): MsgUpdateDocumentTokenMapper {
    const message = {
      ...baseMsgUpdateDocumentTokenMapper
    } as MsgUpdateDocumentTokenMapper;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.documentId !== undefined && object.documentId !== null) {
      message.documentId = object.documentId;
    } else {
      message.documentId = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = object.tokenId;
    } else {
      message.tokenId = "";
    }
    return message;
  }
};

const baseMsgUpdateDocumentTokenMapperResponse: object = {};

export const MsgUpdateDocumentTokenMapperResponse = {
  encode(
    _: MsgUpdateDocumentTokenMapperResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateDocumentTokenMapperResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateDocumentTokenMapperResponse
    } as MsgUpdateDocumentTokenMapperResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateDocumentTokenMapperResponse {
    const message = {
      ...baseMsgUpdateDocumentTokenMapperResponse
    } as MsgUpdateDocumentTokenMapperResponse;
    return message;
  },

  toJSON(_: MsgUpdateDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgUpdateDocumentTokenMapperResponse>
  ): MsgUpdateDocumentTokenMapperResponse {
    const message = {
      ...baseMsgUpdateDocumentTokenMapperResponse
    } as MsgUpdateDocumentTokenMapperResponse;
    return message;
  }
};

const baseMsgDeleteDocumentTokenMapper: object = { creator: "", index: "" };

export const MsgDeleteDocumentTokenMapper = {
  encode(
    message: MsgDeleteDocumentTokenMapper,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteDocumentTokenMapper {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteDocumentTokenMapper
    } as MsgDeleteDocumentTokenMapper;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteDocumentTokenMapper {
    const message = {
      ...baseMsgDeleteDocumentTokenMapper
    } as MsgDeleteDocumentTokenMapper;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = String(object.index);
    } else {
      message.index = "";
    }
    return message;
  },

  toJSON(message: MsgDeleteDocumentTokenMapper): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDeleteDocumentTokenMapper>
  ): MsgDeleteDocumentTokenMapper {
    const message = {
      ...baseMsgDeleteDocumentTokenMapper
    } as MsgDeleteDocumentTokenMapper;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.index !== undefined && object.index !== null) {
      message.index = object.index;
    } else {
      message.index = "";
    }
    return message;
  }
};

const baseMsgDeleteDocumentTokenMapperResponse: object = {};

export const MsgDeleteDocumentTokenMapperResponse = {
  encode(
    _: MsgDeleteDocumentTokenMapperResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteDocumentTokenMapperResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteDocumentTokenMapperResponse
    } as MsgDeleteDocumentTokenMapperResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteDocumentTokenMapperResponse {
    const message = {
      ...baseMsgDeleteDocumentTokenMapperResponse
    } as MsgDeleteDocumentTokenMapperResponse;
    return message;
  },

  toJSON(_: MsgDeleteDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDeleteDocumentTokenMapperResponse>
  ): MsgDeleteDocumentTokenMapperResponse {
    const message = {
      ...baseMsgDeleteDocumentTokenMapperResponse
    } as MsgDeleteDocumentTokenMapperResponse;
    return message;
  }
};

const baseMsgMoveTokenToWallet: object = {
  creator: "",
  tokenRefId: "",
  sourceSegmentId: "",
  targetSegmentId: ""
};

export const MsgMoveTokenToWallet = {
  encode(
    message: MsgMoveTokenToWallet,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetSegmentId !== "") {
      writer.uint32(34).string(message.targetSegmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToWallet {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgMoveTokenToWallet } as MsgMoveTokenToWallet;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetSegmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToWallet {
    const message = { ...baseMsgMoveTokenToWallet } as MsgMoveTokenToWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = String(object.tokenRefId);
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = String(object.sourceSegmentId);
    } else {
      message.sourceSegmentId = "";
    }
    if (
      object.targetSegmentId !== undefined &&
      object.targetSegmentId !== null
    ) {
      message.targetSegmentId = String(object.targetSegmentId);
    } else {
      message.targetSegmentId = "";
    }
    return message;
  },

  toJSON(message: MsgMoveTokenToWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined &&
    (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetSegmentId !== undefined &&
    (obj.targetSegmentId = message.targetSegmentId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgMoveTokenToWallet>): MsgMoveTokenToWallet {
    const message = { ...baseMsgMoveTokenToWallet } as MsgMoveTokenToWallet;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = object.tokenRefId;
    } else {
      message.tokenRefId = "";
    }
    if (
      object.sourceSegmentId !== undefined &&
      object.sourceSegmentId !== null
    ) {
      message.sourceSegmentId = object.sourceSegmentId;
    } else {
      message.sourceSegmentId = "";
    }
    if (
      object.targetSegmentId !== undefined &&
      object.targetSegmentId !== null
    ) {
      message.targetSegmentId = object.targetSegmentId;
    } else {
      message.targetSegmentId = "";
    }
    return message;
  }
};

const baseMsgCreateToken: object = {
  creator: "",
  tokenType: "",
  changeMessage: "",
  segmentId: "",
  moduleRef: ""
};

export const MsgCreateToken = {
  encode(message: MsgCreateToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenType !== "") {
      writer.uint32(18).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(26).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(34).string(message.segmentId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(42).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateToken } as MsgCreateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenType = reader.string();
          break;
        case 3:
          message.changeMessage = reader.string();
          break;
        case 4:
          message.segmentId = reader.string();
          break;
        case 5:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateToken {
    const message = { ...baseMsgCreateToken } as MsgCreateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = String(object.tokenType);
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = String(object.changeMessage);
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = String(object.moduleRef);
    } else {
      message.moduleRef = "";
    }
    return message;
  },

  toJSON(message: MsgCreateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined &&
    (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateToken>): MsgCreateToken {
    const message = { ...baseMsgCreateToken } as MsgCreateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = object.tokenType;
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = object.changeMessage;
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = object.moduleRef;
    } else {
      message.moduleRef = "";
    }
    return message;
  }
};

const baseMsgUpdateToken: object = {
  creator: "",
  tokenRefId: "",
  tokenType: "",
  changeMessage: "",
  segmentId: "",
  moduleRef: ""
};

export const MsgUpdateToken = {
  encode(message: MsgUpdateToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.tokenType !== "") {
      writer.uint32(34).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(50).string(message.segmentId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(58).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateToken } as MsgUpdateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 4:
          message.tokenType = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.segmentId = reader.string();
          break;
        case 7:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateToken {
    const message = { ...baseMsgUpdateToken } as MsgUpdateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = String(object.tokenRefId);
    } else {
      message.tokenRefId = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = String(object.tokenType);
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = String(object.changeMessage);
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = String(object.moduleRef);
    } else {
      message.moduleRef = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined &&
    (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateToken>): MsgUpdateToken {
    const message = { ...baseMsgUpdateToken } as MsgUpdateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
      message.tokenRefId = object.tokenRefId;
    } else {
      message.tokenRefId = "";
    }
    if (object.tokenType !== undefined && object.tokenType !== null) {
      message.tokenType = object.tokenType;
    } else {
      message.tokenType = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = object.changeMessage;
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = object.moduleRef;
    } else {
      message.moduleRef = "";
    }
    return message;
  }
};

const baseMsgIdResponse: object = { id: "" };

export const MsgIdResponse = {
  encode(message: MsgIdResponse, writer: Writer = Writer.create()): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgIdResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgIdResponse {
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgIdResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgIdResponse>): MsgIdResponse {
    const message = { ...baseMsgIdResponse } as MsgIdResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgActivateToken: object = {
  creator: "",
  id: "",
  segmentId: "",
  moduleRef: ""
};

export const MsgActivateToken = {
  encode(message: MsgActivateToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.segmentId !== "") {
      writer.uint32(26).string(message.segmentId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(34).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgActivateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgActivateToken } as MsgActivateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.segmentId = reader.string();
          break;
        case 4:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgActivateToken {
    const message = { ...baseMsgActivateToken } as MsgActivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = String(object.moduleRef);
    } else {
      message.moduleRef = "";
    }
    return message;
  },

  toJSON(message: MsgActivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgActivateToken>): MsgActivateToken {
    const message = { ...baseMsgActivateToken } as MsgActivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = object.moduleRef;
    } else {
      message.moduleRef = "";
    }
    return message;
  }
};

const baseMsgDeactivateToken: object = { creator: "", id: "" };

export const MsgDeactivateToken = {
  encode(
    message: MsgDeactivateToken,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeactivateToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDeactivateToken } as MsgDeactivateToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateToken {
    const message = { ...baseMsgDeactivateToken } as MsgDeactivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgDeactivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgDeactivateToken>): MsgDeactivateToken {
    const message = { ...baseMsgDeactivateToken } as MsgDeactivateToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgCreateHashToken: object = {
  creator: "",
  changeMessage: "",
  segmentId: "",
  document: "",
  hash: "",
  hashFunction: "",
  metadata: ""
};

export const MsgCreateHashToken = {
  encode(
    message: MsgCreateHashToken,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.changeMessage !== "") {
      writer.uint32(18).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(26).string(message.segmentId);
    }
    if (message.document !== "") {
      writer.uint32(34).string(message.document);
    }
    if (message.hash !== "") {
      writer.uint32(42).string(message.hash);
    }
    if (message.hashFunction !== "") {
      writer.uint32(50).string(message.hashFunction);
    }
    if (message.metadata !== "") {
      writer.uint32(58).string(message.metadata);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateHashToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateHashToken } as MsgCreateHashToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.changeMessage = reader.string();
          break;
        case 3:
          message.segmentId = reader.string();
          break;
        case 4:
          message.document = reader.string();
          break;
        case 5:
          message.hash = reader.string();
          break;
        case 6:
          message.hashFunction = reader.string();
          break;
        case 7:
          message.metadata = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateHashToken {
    const message = { ...baseMsgCreateHashToken } as MsgCreateHashToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = String(object.changeMessage);
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = "";
    }
    if (object.document !== undefined && object.document !== null) {
      message.document = String(object.document);
    } else {
      message.document = "";
    }
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = String(object.hash);
    } else {
      message.hash = "";
    }
    if (object.hashFunction !== undefined && object.hashFunction !== null) {
      message.hashFunction = String(object.hashFunction);
    } else {
      message.hashFunction = "";
    }
    if (object.metadata !== undefined && object.metadata !== null) {
      message.metadata = String(object.metadata);
    } else {
      message.metadata = "";
    }
    return message;
  },

  toJSON(message: MsgCreateHashToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.changeMessage !== undefined &&
    (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.document !== undefined && (obj.document = message.document);
    message.hash !== undefined && (obj.hash = message.hash);
    message.hashFunction !== undefined &&
    (obj.hashFunction = message.hashFunction);
    message.metadata !== undefined && (obj.metadata = message.metadata);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateHashToken>): MsgCreateHashToken {
    const message = { ...baseMsgCreateHashToken } as MsgCreateHashToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.changeMessage !== undefined && object.changeMessage !== null) {
      message.changeMessage = object.changeMessage;
    } else {
      message.changeMessage = "";
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = "";
    }
    if (object.document !== undefined && object.document !== null) {
      message.document = object.document;
    } else {
      message.document = "";
    }
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = object.hash;
    } else {
      message.hash = "";
    }
    if (object.hashFunction !== undefined && object.hashFunction !== null) {
      message.hashFunction = object.hashFunction;
    } else {
      message.hashFunction = "";
    }
    if (object.metadata !== undefined && object.metadata !== null) {
      message.metadata = object.metadata;
    } else {
      message.metadata = "";
    }
    return message;
  }
};

const baseMsgCloneToken: object = {
  creator: "",
  tokenId: "",
  walletId: "",
  moduleRef: ""
};

export const MsgCloneToken = {
  encode(message: MsgCloneToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenId !== "") {
      writer.uint32(18).string(message.tokenId);
    }
    if (message.walletId !== "") {
      writer.uint32(26).string(message.walletId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(34).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCloneToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCloneToken } as MsgCloneToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenId = reader.string();
          break;
        case 3:
          message.walletId = reader.string();
          break;
        case 4:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCloneToken {
    const message = { ...baseMsgCloneToken } as MsgCloneToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = String(object.tokenId);
    } else {
      message.tokenId = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = String(object.walletId);
    } else {
      message.walletId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = String(object.moduleRef);
    } else {
      message.moduleRef = "";
    }
    return message;
  },

  toJSON(message: MsgCloneToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCloneToken>): MsgCloneToken {
    const message = { ...baseMsgCloneToken } as MsgCloneToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = object.tokenId;
    } else {
      message.tokenId = "";
    }
    if (object.walletId !== undefined && object.walletId !== null) {
      message.walletId = object.walletId;
    } else {
      message.walletId = "";
    }
    if (object.moduleRef !== undefined && object.moduleRef !== null) {
      message.moduleRef = object.moduleRef;
    } else {
      message.moduleRef = "";
    }
    return message;
  }
};

const baseMsgRevertToGenesis: object = { creator: "" };

export const MsgRevertToGenesis = {
  encode(
    message: MsgRevertToGenesis,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgRevertToGenesis {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgRevertToGenesis } as MsgRevertToGenesis;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRevertToGenesis {
    const message = { ...baseMsgRevertToGenesis } as MsgRevertToGenesis;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgRevertToGenesis): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgRevertToGenesis>): MsgRevertToGenesis {
    const message = { ...baseMsgRevertToGenesis } as MsgRevertToGenesis;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  }
};

const baseMsgUpdateTokenInformation: object = {
  creator: "",
  tokenId: "",
  data: ""
};

export const MsgUpdateTokenInformation = {
  encode(
    message: MsgUpdateTokenInformation,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenId !== "") {
      writer.uint32(18).string(message.tokenId);
    }
    if (message.data !== "") {
      writer.uint32(26).string(message.data);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateTokenInformation {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateTokenInformation
    } as MsgUpdateTokenInformation;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenId = reader.string();
          break;
        case 3:
          message.data = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenInformation {
    const message = {
      ...baseMsgUpdateTokenInformation
    } as MsgUpdateTokenInformation;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = String(object.tokenId);
    } else {
      message.tokenId = "";
    }
    if (object.data !== undefined && object.data !== null) {
      message.data = String(object.data);
    } else {
      message.data = "";
    }
    return message;
  },

  toJSON(message: MsgUpdateTokenInformation): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    message.data !== undefined && (obj.data = message.data);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateTokenInformation>
  ): MsgUpdateTokenInformation {
    const message = {
      ...baseMsgUpdateTokenInformation
    } as MsgUpdateTokenInformation;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tokenId !== undefined && object.tokenId !== null) {
      message.tokenId = object.tokenId;
    } else {
      message.tokenId = "";
    }
    if (object.data !== undefined && object.data !== null) {
      message.data = object.data;
    } else {
      message.data = "";
    }
    return message;
  }
};

const baseMsgFetchTokenHistory: object = { creator: "", id: "" };

export const MsgFetchTokenHistory = {
  encode(
    message: MsgFetchTokenHistory,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchTokenHistory {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchTokenHistory } as MsgFetchTokenHistory;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistory {
    const message = { ...baseMsgFetchTokenHistory } as MsgFetchTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchTokenHistory>): MsgFetchTokenHistory {
    const message = { ...baseMsgFetchTokenHistory } as MsgFetchTokenHistory;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchTokenHistoryResponse: object = {};

export const MsgFetchTokenHistoryResponse = {
  encode(
    message: MsgFetchTokenHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.TokenHistory !== undefined) {
      TokenHistory.encode(
        message.TokenHistory,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchTokenHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFetchTokenHistoryResponse
    } as MsgFetchTokenHistoryResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory = TokenHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistoryResponse {
    const message = {
      ...baseMsgFetchTokenHistoryResponse
    } as MsgFetchTokenHistoryResponse;
    if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
      message.TokenHistory = TokenHistory.fromJSON(object.TokenHistory);
    } else {
      message.TokenHistory = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchTokenHistoryResponse): unknown {
    const obj: any = {};
    message.TokenHistory !== undefined &&
    (obj.TokenHistory = message.TokenHistory
      ? TokenHistory.toJSON(message.TokenHistory)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchTokenHistoryResponse>
  ): MsgFetchTokenHistoryResponse {
    const message = {
      ...baseMsgFetchTokenHistoryResponse
    } as MsgFetchTokenHistoryResponse;
    if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
      message.TokenHistory = TokenHistory.fromPartial(object.TokenHistory);
    } else {
      message.TokenHistory = undefined;
    }
    return message;
  }
};

const baseMsgFetchToken: object = { creator: "", id: "" };

export const MsgFetchToken = {
  encode(message: MsgFetchToken, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchToken {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchToken } as MsgFetchToken;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchToken {
    const message = { ...baseMsgFetchToken } as MsgFetchToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: MsgFetchToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFetchToken>): MsgFetchToken {
    const message = { ...baseMsgFetchToken } as MsgFetchToken;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseMsgFetchTokenResponse: object = {};

export const MsgFetchTokenResponse = {
  encode(
    message: MsgFetchTokenResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.Token !== undefined) {
      Token.encode(message.Token, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFetchTokenResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFetchTokenResponse } as MsgFetchTokenResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token = Token.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenResponse {
    const message = { ...baseMsgFetchTokenResponse } as MsgFetchTokenResponse;
    if (object.Token !== undefined && object.Token !== null) {
      message.Token = Token.fromJSON(object.Token);
    } else {
      message.Token = undefined;
    }
    return message;
  },

  toJSON(message: MsgFetchTokenResponse): unknown {
    const obj: any = {};
    message.Token !== undefined &&
    (obj.Token = message.Token ? Token.toJSON(message.Token) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgFetchTokenResponse>
  ): MsgFetchTokenResponse {
    const message = { ...baseMsgFetchTokenResponse } as MsgFetchTokenResponse;
    if (object.Token !== undefined && object.Token !== null) {
      message.Token = Token.fromPartial(object.Token);
    } else {
      message.Token = undefined;
    }
    return message;
  }
};

const baseMsgEmptyResponse: object = {};

export const MsgEmptyResponse = {
  encode(_: MsgEmptyResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgEmptyResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgEmptyResponse {
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    return message;
  },

  toJSON(_: MsgEmptyResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgEmptyResponse>): MsgEmptyResponse {
    const message = { ...baseMsgEmptyResponse } as MsgEmptyResponse;
    return message;
  }
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  FetchAllSegmentHistory(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse>;

  FetchGetWallet(
    request: MsgFetchGetWallet
  ): Promise<MsgFetchGetWalletResponse>;

  FetchGetSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse>;

  FetchAllSegment(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse>;

  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse1>;

  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse2>;

  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse1>;

  FetchGetWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse>;

  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse2>;

  FetchAllTokenHistoryGlobal(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse>;

  FetchGetTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse>;

  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse2>;

  MoveTokenToSegment(
    request: MsgMoveTokenToSegment
  ): Promise<MsgEmptyResponse1>;

  FetchAllWalletHistory(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse>;

  FetchAllWallet(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse>;

  FetchSegmentHistory(
    request: MsgFetchSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse>;

  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse2>;

  FetchTokensByWalletId(
    request: MsgFetchTokensByWalletId
  ): Promise<QueryAllTokenResponse>;

  FetchTokensBySegmentId(
    request: MsgFetchTokensBySegmentId
  ): Promise<QueryAllTokenResponse>;

  FetchDocumentHash(
    request: MsgFetchDocumentHash
  ): Promise<QueryGetDocumentHashResponse>;

  CreateDocumentTokenMapper(
    request: MsgCreateDocumentTokenMapper
  ): Promise<MsgCreateDocumentTokenMapperResponse>;

  UpdateDocumentTokenMapper(
    request: MsgUpdateDocumentTokenMapper
  ): Promise<MsgUpdateDocumentTokenMapperResponse>;

  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse>;

  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse>;

  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse>;

  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse>;

  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse>;

  StoreDocumentHash(request: MsgCreateHashToken): Promise<MsgIdResponse>;

  CloneToken(request: MsgCloneToken): Promise<MsgEmptyResponse>;

  RevertModulesToGenesis(
    request: MsgRevertToGenesis
  ): Promise<MsgEmptyResponse>;

  UpdateTokenInformation(
    request: MsgUpdateTokenInformation
  ): Promise<MsgEmptyResponse>;

  FetchTokenHistory(
    request: MsgFetchTokenHistory
  ): Promise<MsgFetchTokenHistoryResponse>;

  FetchToken(request: MsgFetchToken): Promise<MsgFetchTokenResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }

  FetchAllSegmentHistory(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse> {
    const data = MsgFetchAllSegmentHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchAllSegmentHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchAllSegmentHistoryResponse.decode(new Reader(data))
    );
  }

  FetchGetWallet(
    request: MsgFetchGetWallet
  ): Promise<MsgFetchGetWalletResponse> {
    const data = MsgFetchGetWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchGetWallet",
      data
    );
    return promise.then((data) =>
      MsgFetchGetWalletResponse.decode(new Reader(data))
    );
  }

  FetchGetSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse> {
    const data = MsgFetchGetSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchGetSegment",
      data
    );
    return promise.then((data) =>
      MsgFetchGetSegmentResponse.decode(new Reader(data))
    );
  }

  FetchAllSegment(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse> {
    const data = MsgFetchAllSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchAllSegment",
      data
    );
    return promise.then((data) =>
      MsgFetchAllSegmentResponse.decode(new Reader(data))
    );
  }

  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse1> {
    const data = MsgUpdateWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "UpdateWallet",
      data
    );
    return promise.then((data) => MsgEmptyResponse1.decode(new Reader(data)));
  }

  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse2> {
    const data = MsgCreateWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "CreateWallet",
      data
    );
    return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
  }

  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse1> {
    const data = MsgUpdateSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "UpdateSegment",
      data
    );
    return promise.then((data) => MsgEmptyResponse1.decode(new Reader(data)));
  }

  FetchGetWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse> {
    const data = MsgFetchGetWalletHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchGetWalletHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchGetWalletHistoryResponse.decode(new Reader(data))
    );
  }

  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse2> {
    const data = MsgCreateSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "CreateSegment",
      data
    );
    return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
  }

  FetchAllTokenHistoryGlobal(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse> {
    const data = MsgFetchAllTokenHistoryGlobal.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchAllTokenHistoryGlobal",
      data
    );
    return promise.then((data) =>
      MsgFetchAllTokenHistoryGlobalResponse.decode(new Reader(data))
    );
  }

  FetchGetTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse> {
    const data = MsgFetchGetTokenHistoryGlobal.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchGetTokenHistoryGlobal",
      data
    );
    return promise.then((data) =>
      MsgFetchGetTokenHistoryGlobalResponse.decode(new Reader(data))
    );
  }

  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse2> {
    const data = MsgCreateWalletWithId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "CreateWalletWithId",
      data
    );
    return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
  }

  MoveTokenToSegment(
    request: MsgMoveTokenToSegment
  ): Promise<MsgEmptyResponse1> {
    const data = MsgMoveTokenToSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "MoveTokenToSegment",
      data
    );
    return promise.then((data) => MsgEmptyResponse1.decode(new Reader(data)));
  }

  FetchAllWalletHistory(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse> {
    const data = MsgFetchAllWalletHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchAllWalletHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchAllWalletHistoryResponse.decode(new Reader(data))
    );
  }

  FetchAllWallet(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse> {
    const data = MsgFetchAllWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchAllWallet",
      data
    );
    return promise.then((data) =>
      MsgFetchAllWalletResponse.decode(new Reader(data))
    );
  }

  FetchSegmentHistory(
    request: MsgFetchSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse> {
    const data = MsgFetchSegmentHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchSegmentHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchGetSegmentHistoryResponse.decode(new Reader(data))
    );
  }

  CreateSegmentWithId(
    request: MsgCreateSegmentWithId
  ): Promise<MsgIdResponse2> {
    const data = MsgCreateSegmentWithId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "CreateSegmentWithId",
      data
    );
    return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
  }

  FetchTokensByWalletId(
    request: MsgFetchTokensByWalletId
  ): Promise<QueryAllTokenResponse> {
    const data = MsgFetchTokensByWalletId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchTokensByWalletId",
      data
    );
    return promise.then((data) =>
      QueryAllTokenResponse.decode(new Reader(data))
    );
  }

  FetchTokensBySegmentId(
    request: MsgFetchTokensBySegmentId
  ): Promise<QueryAllTokenResponse> {
    const data = MsgFetchTokensBySegmentId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchTokensBySegmentId",
      data
    );
    return promise.then((data) =>
      QueryAllTokenResponse.decode(new Reader(data))
    );
  }

  FetchDocumentHash(
    request: MsgFetchDocumentHash
  ): Promise<QueryGetDocumentHashResponse> {
    const data = MsgFetchDocumentHash.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchDocumentHash",
      data
    );
    return promise.then((data) =>
      QueryGetDocumentHashResponse.decode(new Reader(data))
    );
  }

  CreateDocumentTokenMapper(
    request: MsgCreateDocumentTokenMapper
  ): Promise<MsgCreateDocumentTokenMapperResponse> {
    const data = MsgCreateDocumentTokenMapper.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "CreateDocumentTokenMapper",
      data
    );
    return promise.then((data) =>
      MsgCreateDocumentTokenMapperResponse.decode(new Reader(data))
    );
  }

  UpdateDocumentTokenMapper(
    request: MsgUpdateDocumentTokenMapper
  ): Promise<MsgUpdateDocumentTokenMapperResponse> {
    const data = MsgUpdateDocumentTokenMapper.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "UpdateDocumentTokenMapper",
      data
    );
    return promise.then((data) =>
      MsgUpdateDocumentTokenMapperResponse.decode(new Reader(data))
    );
  }

  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse> {
    const data = MsgCreateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "CreateToken",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse> {
    const data = MsgUpdateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "UpdateToken",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse> {
    const data = MsgMoveTokenToWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "MoveTokenToWallet",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse> {
    const data = MsgActivateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "ActivateToken",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse> {
    const data = MsgDeactivateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "DeactivateToken",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  StoreDocumentHash(request: MsgCreateHashToken): Promise<MsgIdResponse> {
    const data = MsgCreateHashToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "StoreDocumentHash",
      data
    );
    return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
  }

  CloneToken(request: MsgCloneToken): Promise<MsgEmptyResponse> {
    const data = MsgCloneToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "CloneToken",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  RevertModulesToGenesis(
    request: MsgRevertToGenesis
  ): Promise<MsgEmptyResponse> {
    const data = MsgRevertToGenesis.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "RevertModulesToGenesis",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  UpdateTokenInformation(
    request: MsgUpdateTokenInformation
  ): Promise<MsgEmptyResponse> {
    const data = MsgUpdateTokenInformation.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "UpdateTokenInformation",
      data
    );
    return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
  }

  FetchTokenHistory(
    request: MsgFetchTokenHistory
  ): Promise<MsgFetchTokenHistoryResponse> {
    const data = MsgFetchTokenHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchTokenHistory",
      data
    );
    return promise.then((data) =>
      MsgFetchTokenHistoryResponse.decode(new Reader(data))
    );
  }

  FetchToken(request: MsgFetchToken): Promise<MsgFetchTokenResponse> {
    const data = MsgFetchToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg",
      "FetchToken",
      data
    );
    return promise.then((data) =>
      MsgFetchTokenResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
