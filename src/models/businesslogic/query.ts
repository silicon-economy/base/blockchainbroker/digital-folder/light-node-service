// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";
import { TokenCopies } from "../businesslogic/token_copies";
import { DocumentTokenMapper } from "../businesslogic/document_token_mapper";
import { Token as Token1 } from "../hashtoken/token";
import { PageRequest, PageResponse } from "../cosmos/pagination";
import { Token } from "../token/token";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic";

/** this line is used by starport scaffolding # 3 */
export interface QueryGetTokenCopiesRequest {
  index: string;
}

export interface QueryGetTokenCopiesResponse {
  TokenCopies: TokenCopies | undefined;
}

export interface QueryAllTokenCopiesRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllTokenCopiesResponse {
  TokenCopies: TokenCopies[];
  pagination: PageResponse | undefined;
}

export interface QueryGetDocumentTokenMapperRequest {
  index: string;
}

export interface QueryGetDocumentTokenMapperResponse {
  DocumentTokenMapper: DocumentTokenMapper | undefined;
}

export interface QueryAllDocumentTokenMapperRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllDocumentTokenMapperResponse {
  DocumentTokenMapper: DocumentTokenMapper[];
  pagination: PageResponse | undefined;
}

export interface QueryGetDocumentHashRequest {
  id: string;
}

export interface QueryGetDocumentHashResponse {
  hash: string;
  hashFunction: string;
  metadata: string;
  creator: string;
  timestamp: string;
}

export interface QueryAllTokenByIdRequest {
  id: string;
  pagination: PageRequest | undefined;
}

export interface QueryAllTokenResponse {
  Token: Token[];
  HashTokens: Token1[];
}

export interface QueryGetDocumentHashHistoryRequest {
  id: string;
}

export interface QueryGetDocumentHashHistoryResponse {
  history: QueryGetDocumentHashResponse[];
}

const baseQueryGetTokenCopiesRequest: object = { index: "" };

export const QueryGetTokenCopiesRequest = {
  encode(
    message: QueryGetTokenCopiesRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.index !== "") {
      writer.uint32(10).string(message.index);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetTokenCopiesRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetTokenCopiesRequest
    } as QueryGetTokenCopiesRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenCopiesRequest {
    const message = {
      ...baseQueryGetTokenCopiesRequest
    } as QueryGetTokenCopiesRequest;
    if (object.index !== undefined && object.index !== null) {
      message.index = String(object.index);
    } else {
      message.index = "";
    }
    return message;
  },

  toJSON(message: QueryGetTokenCopiesRequest): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetTokenCopiesRequest>
  ): QueryGetTokenCopiesRequest {
    const message = {
      ...baseQueryGetTokenCopiesRequest
    } as QueryGetTokenCopiesRequest;
    if (object.index !== undefined && object.index !== null) {
      message.index = object.index;
    } else {
      message.index = "";
    }
    return message;
  }
};

const baseQueryGetTokenCopiesResponse: object = {};

export const QueryGetTokenCopiesResponse = {
  encode(
    message: QueryGetTokenCopiesResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.TokenCopies !== undefined) {
      TokenCopies.encode(
        message.TokenCopies,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetTokenCopiesResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetTokenCopiesResponse
    } as QueryGetTokenCopiesResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenCopies = TokenCopies.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenCopiesResponse {
    const message = {
      ...baseQueryGetTokenCopiesResponse
    } as QueryGetTokenCopiesResponse;
    if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
      message.TokenCopies = TokenCopies.fromJSON(object.TokenCopies);
    } else {
      message.TokenCopies = undefined;
    }
    return message;
  },

  toJSON(message: QueryGetTokenCopiesResponse): unknown {
    const obj: any = {};
    message.TokenCopies !== undefined &&
    (obj.TokenCopies = message.TokenCopies
      ? TokenCopies.toJSON(message.TokenCopies)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetTokenCopiesResponse>
  ): QueryGetTokenCopiesResponse {
    const message = {
      ...baseQueryGetTokenCopiesResponse
    } as QueryGetTokenCopiesResponse;
    if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
      message.TokenCopies = TokenCopies.fromPartial(object.TokenCopies);
    } else {
      message.TokenCopies = undefined;
    }
    return message;
  }
};

const baseQueryAllTokenCopiesRequest: object = {};

export const QueryAllTokenCopiesRequest = {
  encode(
    message: QueryAllTokenCopiesRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllTokenCopiesRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryAllTokenCopiesRequest
    } as QueryAllTokenCopiesRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenCopiesRequest {
    const message = {
      ...baseQueryAllTokenCopiesRequest
    } as QueryAllTokenCopiesRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllTokenCopiesRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllTokenCopiesRequest>
  ): QueryAllTokenCopiesRequest {
    const message = {
      ...baseQueryAllTokenCopiesRequest
    } as QueryAllTokenCopiesRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseQueryAllTokenCopiesResponse: object = {};

export const QueryAllTokenCopiesResponse = {
  encode(
    message: QueryAllTokenCopiesResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.TokenCopies) {
      TokenCopies.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllTokenCopiesResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryAllTokenCopiesResponse
    } as QueryAllTokenCopiesResponse;
    message.TokenCopies = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenCopies.push(TokenCopies.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenCopiesResponse {
    const message = {
      ...baseQueryAllTokenCopiesResponse
    } as QueryAllTokenCopiesResponse;
    message.TokenCopies = [];
    if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
      for (const e of object.TokenCopies) {
        message.TokenCopies.push(TokenCopies.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllTokenCopiesResponse): unknown {
    const obj: any = {};
    if (message.TokenCopies) {
      obj.TokenCopies = message.TokenCopies.map((e) =>
        e ? TokenCopies.toJSON(e) : undefined
      );
    } else {
      obj.TokenCopies = [];
    }
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageResponse.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllTokenCopiesResponse>
  ): QueryAllTokenCopiesResponse {
    const message = {
      ...baseQueryAllTokenCopiesResponse
    } as QueryAllTokenCopiesResponse;
    message.TokenCopies = [];
    if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
      for (const e of object.TokenCopies) {
        message.TokenCopies.push(TokenCopies.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseQueryGetDocumentTokenMapperRequest: object = { index: "" };

export const QueryGetDocumentTokenMapperRequest = {
  encode(
    message: QueryGetDocumentTokenMapperRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.index !== "") {
      writer.uint32(10).string(message.index);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentTokenMapperRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetDocumentTokenMapperRequest
    } as QueryGetDocumentTokenMapperRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDocumentTokenMapperRequest {
    const message = {
      ...baseQueryGetDocumentTokenMapperRequest
    } as QueryGetDocumentTokenMapperRequest;
    if (object.index !== undefined && object.index !== null) {
      message.index = String(object.index);
    } else {
      message.index = "";
    }
    return message;
  },

  toJSON(message: QueryGetDocumentTokenMapperRequest): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetDocumentTokenMapperRequest>
  ): QueryGetDocumentTokenMapperRequest {
    const message = {
      ...baseQueryGetDocumentTokenMapperRequest
    } as QueryGetDocumentTokenMapperRequest;
    if (object.index !== undefined && object.index !== null) {
      message.index = object.index;
    } else {
      message.index = "";
    }
    return message;
  }
};

const baseQueryGetDocumentTokenMapperResponse: object = {};

export const QueryGetDocumentTokenMapperResponse = {
  encode(
    message: QueryGetDocumentTokenMapperResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.DocumentTokenMapper !== undefined) {
      DocumentTokenMapper.encode(
        message.DocumentTokenMapper,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentTokenMapperResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetDocumentTokenMapperResponse
    } as QueryGetDocumentTokenMapperResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.DocumentTokenMapper = DocumentTokenMapper.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDocumentTokenMapperResponse {
    const message = {
      ...baseQueryGetDocumentTokenMapperResponse
    } as QueryGetDocumentTokenMapperResponse;
    if (
      object.DocumentTokenMapper !== undefined &&
      object.DocumentTokenMapper !== null
    ) {
      message.DocumentTokenMapper = DocumentTokenMapper.fromJSON(
        object.DocumentTokenMapper
      );
    } else {
      message.DocumentTokenMapper = undefined;
    }
    return message;
  },

  toJSON(message: QueryGetDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    message.DocumentTokenMapper !== undefined &&
    (obj.DocumentTokenMapper = message.DocumentTokenMapper
      ? DocumentTokenMapper.toJSON(message.DocumentTokenMapper)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetDocumentTokenMapperResponse>
  ): QueryGetDocumentTokenMapperResponse {
    const message = {
      ...baseQueryGetDocumentTokenMapperResponse
    } as QueryGetDocumentTokenMapperResponse;
    if (
      object.DocumentTokenMapper !== undefined &&
      object.DocumentTokenMapper !== null
    ) {
      message.DocumentTokenMapper = DocumentTokenMapper.fromPartial(
        object.DocumentTokenMapper
      );
    } else {
      message.DocumentTokenMapper = undefined;
    }
    return message;
  }
};

const baseQueryAllDocumentTokenMapperRequest: object = {};

export const QueryAllDocumentTokenMapperRequest = {
  encode(
    message: QueryAllDocumentTokenMapperRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllDocumentTokenMapperRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryAllDocumentTokenMapperRequest
    } as QueryAllDocumentTokenMapperRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllDocumentTokenMapperRequest {
    const message = {
      ...baseQueryAllDocumentTokenMapperRequest
    } as QueryAllDocumentTokenMapperRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllDocumentTokenMapperRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllDocumentTokenMapperRequest>
  ): QueryAllDocumentTokenMapperRequest {
    const message = {
      ...baseQueryAllDocumentTokenMapperRequest
    } as QueryAllDocumentTokenMapperRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseQueryAllDocumentTokenMapperResponse: object = {};

export const QueryAllDocumentTokenMapperResponse = {
  encode(
    message: QueryAllDocumentTokenMapperResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.DocumentTokenMapper) {
      DocumentTokenMapper.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllDocumentTokenMapperResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryAllDocumentTokenMapperResponse
    } as QueryAllDocumentTokenMapperResponse;
    message.DocumentTokenMapper = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.DocumentTokenMapper.push(
            DocumentTokenMapper.decode(reader, reader.uint32())
          );
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllDocumentTokenMapperResponse {
    const message = {
      ...baseQueryAllDocumentTokenMapperResponse
    } as QueryAllDocumentTokenMapperResponse;
    message.DocumentTokenMapper = [];
    if (
      object.DocumentTokenMapper !== undefined &&
      object.DocumentTokenMapper !== null
    ) {
      for (const e of object.DocumentTokenMapper) {
        message.DocumentTokenMapper.push(DocumentTokenMapper.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    if (message.DocumentTokenMapper) {
      obj.DocumentTokenMapper = message.DocumentTokenMapper.map((e) =>
        e ? DocumentTokenMapper.toJSON(e) : undefined
      );
    } else {
      obj.DocumentTokenMapper = [];
    }
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageResponse.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllDocumentTokenMapperResponse>
  ): QueryAllDocumentTokenMapperResponse {
    const message = {
      ...baseQueryAllDocumentTokenMapperResponse
    } as QueryAllDocumentTokenMapperResponse;
    message.DocumentTokenMapper = [];
    if (
      object.DocumentTokenMapper !== undefined &&
      object.DocumentTokenMapper !== null
    ) {
      for (const e of object.DocumentTokenMapper) {
        message.DocumentTokenMapper.push(DocumentTokenMapper.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseQueryGetDocumentHashRequest: object = { id: "" };

export const QueryGetDocumentHashRequest = {
  encode(
    message: QueryGetDocumentHashRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetDocumentHashRequest
    } as QueryGetDocumentHashRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDocumentHashRequest {
    const message = {
      ...baseQueryGetDocumentHashRequest
    } as QueryGetDocumentHashRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: QueryGetDocumentHashRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetDocumentHashRequest>
  ): QueryGetDocumentHashRequest {
    const message = {
      ...baseQueryGetDocumentHashRequest
    } as QueryGetDocumentHashRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseQueryGetDocumentHashResponse: object = {
  hash: "",
  hashFunction: "",
  metadata: "",
  creator: "",
  timestamp: ""
};

export const QueryGetDocumentHashResponse = {
  encode(
    message: QueryGetDocumentHashResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.hash !== "") {
      writer.uint32(10).string(message.hash);
    }
    if (message.hashFunction !== "") {
      writer.uint32(18).string(message.hashFunction);
    }
    if (message.metadata !== "") {
      writer.uint32(26).string(message.metadata);
    }
    if (message.creator !== "") {
      writer.uint32(34).string(message.creator);
    }
    if (message.timestamp !== "") {
      writer.uint32(42).string(message.timestamp);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetDocumentHashResponse
    } as QueryGetDocumentHashResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.hash = reader.string();
          break;
        case 2:
          message.hashFunction = reader.string();
          break;
        case 3:
          message.metadata = reader.string();
          break;
        case 4:
          message.creator = reader.string();
          break;
        case 5:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDocumentHashResponse {
    const message = {
      ...baseQueryGetDocumentHashResponse
    } as QueryGetDocumentHashResponse;
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = String(object.hash);
    } else {
      message.hash = "";
    }
    if (object.hashFunction !== undefined && object.hashFunction !== null) {
      message.hashFunction = String(object.hashFunction);
    } else {
      message.hashFunction = "";
    }
    if (object.metadata !== undefined && object.metadata !== null) {
      message.metadata = String(object.metadata);
    } else {
      message.metadata = "";
    }
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = "";
    }
    return message;
  },

  toJSON(message: QueryGetDocumentHashResponse): unknown {
    const obj: any = {};
    message.hash !== undefined && (obj.hash = message.hash);
    message.hashFunction !== undefined &&
    (obj.hashFunction = message.hashFunction);
    message.metadata !== undefined && (obj.metadata = message.metadata);
    message.creator !== undefined && (obj.creator = message.creator);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetDocumentHashResponse>
  ): QueryGetDocumentHashResponse {
    const message = {
      ...baseQueryGetDocumentHashResponse
    } as QueryGetDocumentHashResponse;
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = object.hash;
    } else {
      message.hash = "";
    }
    if (object.hashFunction !== undefined && object.hashFunction !== null) {
      message.hashFunction = object.hashFunction;
    } else {
      message.hashFunction = "";
    }
    if (object.metadata !== undefined && object.metadata !== null) {
      message.metadata = object.metadata;
    } else {
      message.metadata = "";
    }
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = "";
    }
    return message;
  }
};

const baseQueryAllTokenByIdRequest: object = { id: "" };

export const QueryAllTokenByIdRequest = {
  encode(
    message: QueryAllTokenByIdRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllTokenByIdRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryAllTokenByIdRequest
    } as QueryAllTokenByIdRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenByIdRequest {
    const message = {
      ...baseQueryAllTokenByIdRequest
    } as QueryAllTokenByIdRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllTokenByIdRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.pagination !== undefined &&
    (obj.pagination = message.pagination
      ? PageRequest.toJSON(message.pagination)
      : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllTokenByIdRequest>
  ): QueryAllTokenByIdRequest {
    const message = {
      ...baseQueryAllTokenByIdRequest
    } as QueryAllTokenByIdRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  }
};

const baseQueryAllTokenResponse: object = {};

export const QueryAllTokenResponse = {
  encode(
    message: QueryAllTokenResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.Token) {
      Token.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.HashTokens) {
      Token1.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllTokenResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllTokenResponse } as QueryAllTokenResponse;
    message.Token = [];
    message.HashTokens = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token.push(Token.decode(reader, reader.uint32()));
          break;
        case 2:
          message.HashTokens.push(Token1.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenResponse {
    const message = { ...baseQueryAllTokenResponse } as QueryAllTokenResponse;
    message.Token = [];
    message.HashTokens = [];
    if (object.Token !== undefined && object.Token !== null) {
      for (const e of object.Token) {
        message.Token.push(Token.fromJSON(e));
      }
    }
    if (object.HashTokens !== undefined && object.HashTokens !== null) {
      for (const e of object.HashTokens) {
        message.HashTokens.push(Token1.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: QueryAllTokenResponse): unknown {
    const obj: any = {};
    if (message.Token) {
      obj.Token = message.Token.map((e) => (e ? Token.toJSON(e) : undefined));
    } else {
      obj.Token = [];
    }
    if (message.HashTokens) {
      obj.HashTokens = message.HashTokens.map((e) =>
        e ? Token1.toJSON(e) : undefined
      );
    } else {
      obj.HashTokens = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllTokenResponse>
  ): QueryAllTokenResponse {
    const message = { ...baseQueryAllTokenResponse } as QueryAllTokenResponse;
    message.Token = [];
    message.HashTokens = [];
    if (object.Token !== undefined && object.Token !== null) {
      for (const e of object.Token) {
        message.Token.push(Token.fromPartial(e));
      }
    }
    if (object.HashTokens !== undefined && object.HashTokens !== null) {
      for (const e of object.HashTokens) {
        message.HashTokens.push(Token1.fromPartial(e));
      }
    }
    return message;
  }
};

const baseQueryGetDocumentHashHistoryRequest: object = { id: "" };

export const QueryGetDocumentHashHistoryRequest = {
  encode(
    message: QueryGetDocumentHashHistoryRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashHistoryRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetDocumentHashHistoryRequest
    } as QueryGetDocumentHashHistoryRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDocumentHashHistoryRequest {
    const message = {
      ...baseQueryGetDocumentHashHistoryRequest
    } as QueryGetDocumentHashHistoryRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    return message;
  },

  toJSON(message: QueryGetDocumentHashHistoryRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetDocumentHashHistoryRequest>
  ): QueryGetDocumentHashHistoryRequest {
    const message = {
      ...baseQueryGetDocumentHashHistoryRequest
    } as QueryGetDocumentHashHistoryRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    return message;
  }
};

const baseQueryGetDocumentHashHistoryResponse: object = {};

export const QueryGetDocumentHashHistoryResponse = {
  encode(
    message: QueryGetDocumentHashHistoryResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.history) {
      QueryGetDocumentHashResponse.encode(
        v!,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashHistoryResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetDocumentHashHistoryResponse
    } as QueryGetDocumentHashHistoryResponse;
    message.history = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.history.push(
            QueryGetDocumentHashResponse.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDocumentHashHistoryResponse {
    const message = {
      ...baseQueryGetDocumentHashHistoryResponse
    } as QueryGetDocumentHashHistoryResponse;
    message.history = [];
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(QueryGetDocumentHashResponse.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: QueryGetDocumentHashHistoryResponse): unknown {
    const obj: any = {};
    if (message.history) {
      obj.history = message.history.map((e) =>
        e ? QueryGetDocumentHashResponse.toJSON(e) : undefined
      );
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetDocumentHashHistoryResponse>
  ): QueryGetDocumentHashHistoryResponse {
    const message = {
      ...baseQueryGetDocumentHashHistoryResponse
    } as QueryGetDocumentHashHistoryResponse;
    message.history = [];
    if (object.history !== undefined && object.history !== null) {
      for (const e of object.history) {
        message.history.push(QueryGetDocumentHashResponse.fromPartial(e));
      }
    }
    return message;
  }
};

/** Query defines the gRPC querier service. */
export interface Query {
  /** this line is used by starport scaffolding # 2 */
  DocumentHash(
    request: QueryGetDocumentHashRequest
  ): Promise<QueryGetDocumentHashResponse>;

  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/token-manager/TokenManager/BusinessLogic/hash/{id}/history"; */
  DocumentHashHistory(
    request: QueryGetDocumentHashHistoryRequest
  ): Promise<QueryGetDocumentHashHistoryResponse>;

  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/token-manager/TokenManager/BusinessLogic/tokensBySegmentId/{id}"; */
  TokensBySegmentId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse>;

  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/token-manager/TokenManager/BusinessLogic/tokensByWalletId/{id}"; */
  TokensByWalletId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse>;
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }

  DocumentHash(
    request: QueryGetDocumentHashRequest
  ): Promise<QueryGetDocumentHashResponse> {
    const data = QueryGetDocumentHashRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query",
      "DocumentHash",
      data
    );
    return promise.then((data) =>
      QueryGetDocumentHashResponse.decode(new Reader(data))
    );
  }

  DocumentHashHistory(
    request: QueryGetDocumentHashHistoryRequest
  ): Promise<QueryGetDocumentHashHistoryResponse> {
    const data = QueryGetDocumentHashHistoryRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query",
      "DocumentHashHistory",
      data
    );
    return promise.then((data) =>
      QueryGetDocumentHashHistoryResponse.decode(new Reader(data))
    );
  }

  TokensBySegmentId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse> {
    const data = QueryAllTokenByIdRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query",
      "TokensBySegmentId",
      data
    );
    return promise.then((data) =>
      QueryAllTokenResponse.decode(new Reader(data))
    );
  }

  TokensByWalletId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse> {
    const data = QueryAllTokenByIdRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query",
      "TokensByWalletId",
      data
    );
    return promise.then((data) =>
      QueryAllTokenResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;
