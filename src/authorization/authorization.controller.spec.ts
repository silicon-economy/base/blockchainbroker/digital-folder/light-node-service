// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Test, TestingModule } from "@nestjs/testing";
import { AuthorizationController } from "./authorization.controller";
import { AuthorizationService } from "./authorization.service";
import { BlockchainService } from "../blockchain/blockchain.service";

describe("AuthorizationController", () => {
  let controller: AuthorizationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthorizationController],
      providers: [AuthorizationService, BlockchainService]
    }).compile();

    controller = module.get<AuthorizationController>(AuthorizationController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
