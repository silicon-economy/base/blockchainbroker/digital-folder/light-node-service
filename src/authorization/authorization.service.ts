// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { EncodeObject, GeneratedType, Registry } from "@cosmjs/proto-signing";
import { Injectable } from "@nestjs/common";
import { BlockchainService } from "../blockchain/blockchain.service";
import {
  MsgCreateApplicationRole,
  MsgCreateBlockchainAccount,
  MsgDeactivateApplicationRole,
  MsgDeleteApplicationRole,
  MsgFetchAllApplicationRole,
  MsgFetchAllBlockchainAccount,
  MsgFetchApplicationRole,
  MsgFetchBlockchainAccount,
  MsgGrantAppRoleToBlockchainAccount,
  MsgRevokeAppRoleFromBlockchainAccount,
  MsgUpdateApplicationRole,
  MsgUpdateConfiguration
} from "../models/authorization/tx";
import { CreateApplicationRoleDto } from "./dto/create-application-role.dto";
import { CreateBlockchainAccountDto } from "./dto/create-blockchain-account.dto";
import { DeactivateApplicationRoleDto } from "./dto/deactivate-application-role.dto";
import { DeleteApplicationRoleDto } from "./dto/delete-application-role.dto";
import { FetchAllApplicationRoleDto } from "./dto/fetch-all-application-role.dto";
import { FetchAllBlockchainAccountDto } from "./dto/fetch-all-blockchain-account.dto";
import { FetchApplicationRoleDto } from "./dto/fetch-application-role.dto";
import { FetchBlockchainAccountDto } from "./dto/fetch-blockchain-account.dto";
import { GrantAppRoleToBlockchainAccountDto } from "./dto/grant-app-role-to-blockchain-account.dto";
import { RevokeAppRoleFromBlockchainAccountDto } from "./dto/revoke-app-role-from-blockchain-account.dto";
import { UpdateApplicationRoleDto } from "./dto/update-application-role.dto";
import { UpdateConfigurationDto } from "./dto/update-configuration.dto";

@Injectable()
export class AuthorizationService {
  types = [
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateBlockchainAccount",
      MsgCreateBlockchainAccount
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchAllBlockchainAccount",
      MsgFetchAllBlockchainAccount
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchBlockchainAccount",
      MsgFetchBlockchainAccount
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateApplicationRole",
      MsgCreateApplicationRole
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchAllApplicationRole",
      MsgFetchAllApplicationRole
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchApplicationRole",
      MsgFetchApplicationRole
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateApplicationRole",
      MsgUpdateApplicationRole
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeactivateApplicationRole",
      MsgDeactivateApplicationRole
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeleteApplicationRole",
      MsgDeleteApplicationRole
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgGrantAppRoleToBlockchainAccount",
      MsgGrantAppRoleToBlockchainAccount
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgRevokeAppRoleFromBlockchainAccount",
      MsgRevokeAppRoleFromBlockchainAccount
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateConfiguration",
      MsgUpdateConfiguration
    ]
  ];

  registry = new Registry(<Iterable<[string, GeneratedType]>>this.types);

  constructor(private readonly blockchainService: BlockchainService) {
  }

  createBlockchainAccount(createBlockchainAccountDto: CreateBlockchainAccountDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateBlockchainAccount",
      value: createBlockchainAccountDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  fetchAllBlockchainAccount(fetchAllBlockchainAccountDto: FetchAllBlockchainAccountDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchAllBlockchainAccount",
      value: fetchAllBlockchainAccountDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  fetchBlockchainAccount(fetchBlockchainAccountDto: FetchBlockchainAccountDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchBlockchainAccount",
      value: fetchBlockchainAccountDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  createApplicationRole(createApplicationRoleDto: CreateApplicationRoleDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateApplicationRole",
      value: createApplicationRoleDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  fetchAllApplicationRole(fetchAllApplicationRoleDto: FetchAllApplicationRoleDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchAllApplicationRole",
      value: fetchAllApplicationRoleDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  fetchApplicationRole(fetchApplicationRoleDto: FetchApplicationRoleDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchApplicationRole",
      value: fetchApplicationRoleDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  updateApplicationRole(updateApplicationRoleDto: UpdateApplicationRoleDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateApplicationRole",
      value: updateApplicationRoleDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  deactivateApplicationRole(deactivateApplicationRoleDto: DeactivateApplicationRoleDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeactivateApplicationRole",
      value: deactivateApplicationRoleDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  deleteApplicationRole(deleteApplicationRole: DeleteApplicationRoleDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeleteApplicationRole",
      value: deleteApplicationRole
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  grantAppRoleToBlockchainAccount(grantAppRoleToBlockchainAccountDto: GrantAppRoleToBlockchainAccountDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgGrantAppRoleToBlockchainAccount",
      value: grantAppRoleToBlockchainAccountDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  revokeAppRoleFromBlockchainAccount(revokeAppRoleFromBlockchainAccountDto: RevokeAppRoleFromBlockchainAccountDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgRevokeAppRoleFromBlockchainAccount",
      value: revokeAppRoleFromBlockchainAccountDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  updateConfiguration(updateConfigurationDto: UpdateConfigurationDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateConfiguration",
      value: updateConfigurationDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }
}
