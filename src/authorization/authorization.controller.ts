// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Controller } from "@nestjs/common";
import { MessagePattern, Payload } from "@nestjs/microservices";
import { AuthorizationService } from "./authorization.service";
import { CreateApplicationRoleDto } from "./dto/create-application-role.dto";
import { CreateBlockchainAccountDto } from "./dto/create-blockchain-account.dto";
import { DeactivateApplicationRoleDto } from "./dto/deactivate-application-role.dto";
import { DeleteApplicationRoleDto } from "./dto/delete-application-role.dto";
import { FetchAllApplicationRoleDto } from "./dto/fetch-all-application-role.dto";
import { FetchAllBlockchainAccountDto } from "./dto/fetch-all-blockchain-account.dto";
import { FetchApplicationRoleDto } from "./dto/fetch-application-role.dto";
import { FetchBlockchainAccountDto } from "./dto/fetch-blockchain-account.dto";
import { GrantAppRoleToBlockchainAccountDto } from "./dto/grant-app-role-to-blockchain-account.dto";
import { RevokeAppRoleFromBlockchainAccountDto } from "./dto/revoke-app-role-from-blockchain-account.dto";
import { UpdateApplicationRoleDto } from "./dto/update-application-role.dto";
import { UpdateConfigurationDto } from "./dto/update-configuration.dto";

@Controller()
export class AuthorizationController {
  constructor(private readonly authorizationService: AuthorizationService) {
  }

  @MessagePattern("createBlockchainAccount")
  createBlockchainAccount(@Payload() createBlockchainAccountDto: CreateBlockchainAccountDto) {
    return this.authorizationService.createBlockchainAccount(createBlockchainAccountDto);
  }

  @MessagePattern("fetchAllBlockchainAccount")
  fetchAllBlockchainAccount(@Payload() fetchAllBlockchainAccountDto: FetchAllBlockchainAccountDto) {
    return this.authorizationService.fetchAllBlockchainAccount(fetchAllBlockchainAccountDto);
  }

  @MessagePattern("fetchBlockchainAccount")
  fetchBlockchainAccount(@Payload() fetchBlockchainAccountDto: FetchBlockchainAccountDto) {
    return this.authorizationService.fetchBlockchainAccount(fetchBlockchainAccountDto);
  }

  @MessagePattern("createApplicationRole")
  createApplicationRole(@Payload() createApplicationRoleDto: CreateApplicationRoleDto) {
    return this.authorizationService.createApplicationRole(createApplicationRoleDto);
  }

  @MessagePattern("fetchAllApplicationRole")
  fetchAllApplicationRole(@Payload() fetchAllApplicationRoleDto: FetchAllApplicationRoleDto) {
    return this.authorizationService.fetchAllApplicationRole(fetchAllApplicationRoleDto);
  }

  @MessagePattern("fetchApplicationRole")
  fetchApplicationRole(@Payload() fetchApplicationRoleDto: FetchApplicationRoleDto) {
    return this.authorizationService.fetchApplicationRole(fetchApplicationRoleDto);
  }

  @MessagePattern("updateApplicationRole")
  updateApplicationRole(@Payload() updateApplicationRoleDto: UpdateApplicationRoleDto) {
    return this.authorizationService.updateApplicationRole(updateApplicationRoleDto);
  }

  @MessagePattern("deactivateApplicationRole")
  deactivateApplicationRole(@Payload() deactivateApplicationRoleDto: DeactivateApplicationRoleDto) {
    return this.authorizationService.deactivateApplicationRole(deactivateApplicationRoleDto);
  }

  @MessagePattern("deleteApplicationRole")
  deleteApplicationRole(@Payload() deleteApplicationRole: DeleteApplicationRoleDto) {
    return this.authorizationService.deleteApplicationRole(deleteApplicationRole);
  }

  @MessagePattern("grantAppRoleToBlockchainAccount")
  grantAppRoleToBlockchainAccount(@Payload() grantAppRoleToBlockchainAccountDto: GrantAppRoleToBlockchainAccountDto) {
    return this.authorizationService.grantAppRoleToBlockchainAccount(grantAppRoleToBlockchainAccountDto);
  }

  @MessagePattern("revokeAppRoleFromBlockchainAccount")
  revokeAppRoleFromBlockchainAccount(@Payload() revokeAppRoleFromBlockchainAccountDto: RevokeAppRoleFromBlockchainAccountDto) {
    return this.authorizationService.revokeAppRoleFromBlockchainAccount(revokeAppRoleFromBlockchainAccountDto);
  }

  @MessagePattern("updateConfiguration")
  updateConfiguration(@Payload() updateConfigurationDto: UpdateConfigurationDto) {
    return this.authorizationService.updateConfiguration(updateConfigurationDto);
  }
}
