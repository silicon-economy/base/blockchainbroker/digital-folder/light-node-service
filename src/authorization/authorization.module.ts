// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Module } from "@nestjs/common";
import { AuthorizationService } from "./authorization.service";
import { AuthorizationController } from "./authorization.controller";
import { BlockchainService } from "../blockchain/blockchain.service";

@Module({
  controllers: [AuthorizationController],
  providers: [AuthorizationService, BlockchainService]
})
export class AuthorizationModule {
}
