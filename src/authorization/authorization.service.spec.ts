// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Test, TestingModule } from "@nestjs/testing";
import { AuthorizationService } from "./authorization.service";
import { BlockchainService } from "../blockchain/blockchain.service";
import { CreateBlockchainAccountDto } from "./dto/create-blockchain-account.dto";
import { FetchAllBlockchainAccountDto } from "./dto/fetch-all-blockchain-account.dto";
import { FetchBlockchainAccountDto } from "./dto/fetch-blockchain-account.dto";
import { CreateApplicationRoleDto } from "./dto/create-application-role.dto";
import { FetchAllApplicationRoleDto } from "./dto/fetch-all-application-role.dto";
import { FetchApplicationRoleDto } from "./dto/fetch-application-role.dto";
import { UpdateApplicationRoleDto } from "./dto/update-application-role.dto";
import { DeactivateApplicationRoleDto } from "./dto/deactivate-application-role.dto";
import { DeleteApplicationRoleDto } from "./dto/delete-application-role.dto";
import { GrantAppRoleToBlockchainAccountDto } from "./dto/grant-app-role-to-blockchain-account.dto";
import { RevokeAppRoleFromBlockchainAccountDto } from "./dto/revoke-app-role-from-blockchain-account.dto";
import { UpdateConfigurationDto } from "./dto/update-configuration.dto";

describe("AuthorizationService", () => {
  let service: AuthorizationService;
  let bcService: BlockchainService;
  const bcIdentity: string = "A4C656846C";
  const accountId: string = "0x23FD19";
  const roleId: string = "A123B";

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthorizationService, BlockchainService]
    }).compile();

    service = module.get<AuthorizationService>(AuthorizationService);
    bcService = module.get<BlockchainService>(BlockchainService);
  });

  it("should create a blockchain account", async () => {
    const createBlockchainAccountDto: CreateBlockchainAccountDto = new CreateBlockchainAccountDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    createBlockchainAccountDto.creator = bcIdentity;
    createBlockchainAccountDto.id = accountId;
    await service.createBlockchainAccount(createBlockchainAccountDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all blockchain accounts", async () => {
    const fetchAllBlockchainAccountDto: FetchAllBlockchainAccountDto = new FetchAllBlockchainAccountDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchAllBlockchainAccountDto.creator = bcIdentity;
    await service.fetchAllBlockchainAccount(fetchAllBlockchainAccountDto);
    expect(spy).toBeCalled();
  });

  it("should fetch a blockchain account", async () => {
    const fetchBlockchainAccountDto: FetchBlockchainAccountDto = new FetchBlockchainAccountDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchBlockchainAccountDto.creator = bcIdentity;
    await service.fetchBlockchainAccount(fetchBlockchainAccountDto);
    expect(spy).toBeCalled();
  });

  it("should create an application role", async () => {
    const createApplicationRoleDto: CreateApplicationRoleDto = new CreateApplicationRoleDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    createApplicationRoleDto.creator = bcIdentity;
    createApplicationRoleDto.id = roleId;
    createApplicationRoleDto.description = "a dummy application role description";
    await service.createApplicationRole(createApplicationRoleDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all application roles", async () => {
    const fetchAllApplicationRoleDto: FetchAllApplicationRoleDto = new FetchAllApplicationRoleDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchAllApplicationRoleDto.creator = bcIdentity;
    await service.fetchAllApplicationRole(fetchAllApplicationRoleDto);
    expect(spy).toBeCalled();
  });

  it("should fetch an application role", async () => {
    const fetchApplicationRoleDto: FetchApplicationRoleDto = new FetchApplicationRoleDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchApplicationRoleDto.creator = bcIdentity;
    fetchApplicationRoleDto.id = roleId;
    await service.fetchApplicationRole(fetchApplicationRoleDto);
    expect(spy).toBeCalled();
  });

  it("should update an application role", async () => {
    const updateApplicationRoleDto: UpdateApplicationRoleDto = new UpdateApplicationRoleDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    updateApplicationRoleDto.creator = bcIdentity;
    updateApplicationRoleDto.id = roleId;
    updateApplicationRoleDto.applicationRoleStates = new Array(3);
    await service.updateApplicationRole(updateApplicationRoleDto);
    expect(spy).toBeCalled();
  });

  it("should deactivate an application role", async () => {
    const deactivateApplicationRoleDto: DeactivateApplicationRoleDto = new DeactivateApplicationRoleDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    deactivateApplicationRoleDto.creator = bcIdentity;
    deactivateApplicationRoleDto.id = roleId;
    await service.deactivateApplicationRole(deactivateApplicationRoleDto);
    expect(spy).toBeCalled();
  });

  it("should delete an application role", async () => {
    const deleteApplicationRoleDto: DeleteApplicationRoleDto = new DeleteApplicationRoleDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    deleteApplicationRoleDto.creator = bcIdentity;
    deleteApplicationRoleDto.id = roleId;
    await service.deleteApplicationRole(deleteApplicationRoleDto);
    expect(spy).toBeCalled();
  });

  it("should grant an application role to a blockchain account", async () => {
    const grantAppRoleToBlockchainAccountDto: GrantAppRoleToBlockchainAccountDto = new GrantAppRoleToBlockchainAccountDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    grantAppRoleToBlockchainAccountDto.creator = bcIdentity;
    grantAppRoleToBlockchainAccountDto.user = "my-user";
    grantAppRoleToBlockchainAccountDto.roleID = roleId;
    await service.grantAppRoleToBlockchainAccount(grantAppRoleToBlockchainAccountDto);
    expect(spy).toBeCalled();
  });

  it("should revoke an application role from a blockchain account", async () => {
    const revokeAppRoleFromBlockchainAccountDto: RevokeAppRoleFromBlockchainAccountDto = new RevokeAppRoleFromBlockchainAccountDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    revokeAppRoleFromBlockchainAccountDto.creator = bcIdentity;
    revokeAppRoleFromBlockchainAccountDto.account = accountId;
    revokeAppRoleFromBlockchainAccountDto.roleID = roleId;
    await service.revokeAppRoleFromBlockchainAccount(revokeAppRoleFromBlockchainAccountDto);
    expect(spy).toBeCalled();
  });

  it("should update configuration", async () => {
    const updateConfigurationDto: UpdateConfigurationDto = new UpdateConfigurationDto();
    const spy = jest.spyOn(bcService, "signAndBroadcast")
      .mockImplementation(() => Promise.resolve(new Uint8Array));

    updateConfigurationDto.creator = bcIdentity;
    updateConfigurationDto.id = 123;
    updateConfigurationDto.PermissionCheck = true;
    await service.updateConfiguration(updateConfigurationDto);
    expect(spy).toBeCalled();
  });
});
