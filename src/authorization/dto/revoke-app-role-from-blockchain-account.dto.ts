// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

export class RevokeAppRoleFromBlockchainAccountDto {
  creator: string;
  account: string;
  roleID: string; // the token manager spells id in uppercase
}
