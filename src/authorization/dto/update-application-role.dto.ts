// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

export class UpdateApplicationRoleDto {
  creator: string;
  id: string;
  applicationRoleStates: ApplicationRoleState[];
}

export class ApplicationRoleState {
  creator: string;
  id: string;
  applicationRoleId: string;
  description: string;
  valid: boolean;
  timeStamp: string; // Typo is intentional here because the tokenmanager also spells it like this
}
