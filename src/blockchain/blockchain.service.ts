// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Injectable } from "@nestjs/common";
import { DirectSecp256k1HdWallet, EncodeObject, Registry } from "@cosmjs/proto-signing";
import { BroadcastTxResponse, isBroadcastTxSuccess, SigningStargateClient } from "@cosmjs/stargate";
import { ADDRESS_PREFIX, BLOCKCHAIN_RPC_ENDPOINT, DEFAULT_FEE, MNEMONIC } from "../config/config";
import Utils from "../util/utils";

@Injectable()
export class BlockchainService {
  private maxTimeout = 3;
  private timeoutMs = 1000;

  async signAndBroadcast(tx: EncodeObject, registry: Registry) {

    const wallet = await DirectSecp256k1HdWallet.fromMnemonic(
      MNEMONIC,
      undefined,
      ADDRESS_PREFIX
    );
    const [account] = await wallet.getAccounts();
    tx.value.creator = account.address;

    const stargateClient = SigningStargateClient;
    const client = await stargateClient.connectWithSigner(
      BLOCKCHAIN_RPC_ENDPOINT,
      wallet,
      {
        registry
      }
    );

    for (let i = 0; i < this.maxTimeout; i++) {
      const response: BroadcastTxResponse = await client.signAndBroadcast(
        account.address,
        [tx],
        DEFAULT_FEE,
        ""
      );

      if (isBroadcastTxSuccess(response)) {
        return response.data[0].data;
      }

      await Utils.delay(this.timeoutMs);
    }
  }
}
