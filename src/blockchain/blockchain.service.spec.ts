// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Test, TestingModule } from "@nestjs/testing";
import { BlockchainService } from "./blockchain.service";

describe("BlockchainService", () => {
  let service: BlockchainService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BlockchainService, BlockchainService]
    }).compile();

    service = module.get<BlockchainService>(BlockchainService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
