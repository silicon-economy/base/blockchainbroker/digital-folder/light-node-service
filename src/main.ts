// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { Transport } from "@nestjs/microservices";
import { INestMicroservice } from "@nestjs/common";
import config, { Queues } from "./config/config";

/**
 * Bootstraps the application with mqtt transport protocol.
 */
async function bootstrap() {
  const appContext = await NestFactory.createApplicationContext(
    ConfigModule.forRoot({
      load: [config]
    })
  );
  const configService = appContext.get(ConfigService);

  const microservice: INestMicroservice = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [configService.get("amqpUrl" as never)],
      queue: configService.get<string>("queuePrefix" as never) + Queues.TENDERMINT_SERVICE,
      queueOptions: {
        durable: false
      }
    }
  });

  await appContext.close();
  await microservice.listenAsync();
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
bootstrap();
