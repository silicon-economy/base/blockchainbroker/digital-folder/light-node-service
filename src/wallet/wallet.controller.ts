// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { FetchWalletDto } from "./dto/fetch-wallet.dto";
import { FetchTokenHistoryGlobalDto } from "./dto/fetch-token-history-global.dto";
import { Controller } from "@nestjs/common";
import { MessagePattern, Payload } from "@nestjs/microservices";
import { WalletService } from "./wallet.service";
import { CreateWalletDto } from "./dto/create-wallet.dto";
import { UpdateWalletDto } from "./dto/update-wallet.dto";
import { CreateSegmentWithIdDto } from "./dto/create-segment-with-id.dto";
import { CreateSegmentDto } from "./dto/create-segment.dto";
import { CreateWalletWithIdDto } from "./dto/create-wallet-with-id.dto";
import { FetchAllSegmentHistoryDto } from "./dto/fetch-all-segment-history.dto";
import { FetchAllSegmentDto } from "./dto/fetch-all-segment.dto";
import { FetchAllTokenHistoryGlobalDto } from "./dto/fetch-all-token-history-global.dto";
import { FetchAllWalletHistoryDto } from "./dto/fetch-all-wallet-history.dto";
import { FetchAllWalletDto } from "./dto/fetch-all-wallet.dto";
import { UpdateSegmentDto } from "./dto/update-segment.dto";
import { FetchSegmentHistoryDto } from "./dto/fetch-segment-history.dto";
import { FetchSegmentDto } from "./dto/fetch-segment.dto";
import { FetchWalletHistoryDto } from "./dto/fetch-wallet-history.dto";

@Controller()
export class WalletController {
  constructor(private readonly walletService: WalletService) {
  }

  @MessagePattern("createWallet")
  createWallet(@Payload() createWalletDto: CreateWalletDto) {
    return this.walletService.createWallet(createWalletDto);
  }

  @MessagePattern("createWalletWithId")
  createWalletWithId(@Payload() creatWalletWithIdDto: CreateWalletWithIdDto) {
    return this.walletService.createWalletWithId(creatWalletWithIdDto);
  }

  @MessagePattern("fetchAllWallet")
  fetchAllWallet(@Payload() fetchAllWalletDto: FetchAllWalletDto) {
    return this.walletService.fetchAllWallet(fetchAllWalletDto);
  }

  @MessagePattern("fetchWallet")
  fetchWallet(@Payload() fetchWalletDto: FetchWalletDto) {
    return this.walletService.fetchWallet(fetchWalletDto);
  }

  @MessagePattern("fetchAllWalletHistory")
  fetchAllWalletHistory(@Payload() fetchAllWalletHistoryDto: FetchAllWalletHistoryDto) {
    return this.walletService.fetchAllWalletHistory(fetchAllWalletHistoryDto);
  }

  @MessagePattern("fetchWalletHistory")
  fetchWalletHistory(@Payload() fetchWalletHistoryDto: FetchWalletHistoryDto) {
    return this.walletService.fetchWalletHistory(fetchWalletHistoryDto);
  }

  @MessagePattern("updateWallet")
  updateWallet(@Payload() updateWalletDto: UpdateWalletDto) {
    return this.walletService.updateWallet(updateWalletDto);
  }

  @MessagePattern("createSegment")
  createSegment(@Payload() createSegment: CreateSegmentDto) {
    return this.walletService.createSegment(createSegment);
  }

  @MessagePattern("createSegmentWithId")
  createSegmentWithId(@Payload() createSegmentWithIdDto: CreateSegmentWithIdDto) {
    return this.walletService.createSegmentWithId(createSegmentWithIdDto);
  }

  @MessagePattern("fetchAllSegment")
  fetchAllSegment(@Payload() fetchAllSegmentDto: FetchAllSegmentDto) {
    return this.walletService.fetchAllSegment(fetchAllSegmentDto);
  }

  @MessagePattern("fetchSegment")
  fetchSegment(@Payload() fetchSegmentDto: FetchSegmentDto) {
    return this.walletService.fetchSegment(fetchSegmentDto);
  }

  @MessagePattern("fetchAllSegmentHistory")
  fetchAllSegmentHistory(@Payload() fetchAllSegmentHistoryDto: FetchAllSegmentHistoryDto) {
    return this.walletService.fetchAllSegmentHistory(fetchAllSegmentHistoryDto);
  }

  @MessagePattern("fetchSegmentHistory")
  fetchSegmentHistory(@Payload() fetchSegmentHistoryDto: FetchSegmentHistoryDto) {
    return this.walletService.fetchSegmentHistory(fetchSegmentHistoryDto);
  }

  @MessagePattern("updateSegment")
  updateSegment(@Payload() updateSegmentDto: UpdateSegmentDto) {
    return this.walletService.updateSegment(updateSegmentDto);
  }

  @MessagePattern("fetchAllTokenHistoryGlobal")
  fetchAllTokenHistoryGlobal(@Payload() fetchAllTokenHistoryGlobalDto: FetchAllTokenHistoryGlobalDto) {
    return this.walletService.fetchAllTokenHistoryGlobal(fetchAllTokenHistoryGlobalDto);
  }

  @MessagePattern("fetchTokenHistoryGlobal")
  fetchTokenHistoryGlobal(@Payload() fetchTokenHistoryGlobalDto: FetchTokenHistoryGlobalDto) {
    return this.walletService.fetchTokenHistoryGlobal(fetchTokenHistoryGlobalDto);
  }
}
