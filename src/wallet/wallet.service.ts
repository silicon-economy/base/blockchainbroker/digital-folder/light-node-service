// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { EncodeObject, GeneratedType, Registry } from "@cosmjs/proto-signing";
import { Injectable } from "@nestjs/common";
import { BlockchainService } from "../blockchain/blockchain.service";
import {
  MsgCreateSegment,
  MsgCreateSegmentWithId,
  MsgCreateWallet,
  MsgCreateWalletWithId,
  MsgEmptyResponse,
  MsgFetchAllSegment,
  MsgFetchAllSegmentHistory,
  MsgFetchAllTokenHistoryGlobal,
  MsgFetchAllWallet,
  MsgFetchAllWalletHistory,
  MsgFetchGetSegment,
  MsgFetchGetTokenHistoryGlobal,
  MsgFetchGetWallet,
  MsgFetchGetWalletHistory,
  MsgFetchSegmentHistory,
  MsgUpdateSegment,
  MsgUpdateWallet
} from "../models/businesslogic/tx";
import {
  MsgFetchAllSegmentHistoryResponse,
  MsgFetchAllSegmentResponse,
  MsgFetchAllTokenHistoryGlobalResponse,
  MsgFetchAllWalletHistoryResponse,
  MsgFetchAllWalletResponse,
  MsgFetchGetSegmentHistoryResponse,
  MsgFetchGetSegmentResponse,
  MsgFetchGetTokenHistoryGlobalResponse,
  MsgFetchGetWalletHistoryResponse,
  MsgFetchGetWalletResponse,
  MsgIdResponse
} from "../models/wallet/tx";
import { CreateSegmentWithIdDto } from "./dto/create-segment-with-id.dto";
import { CreateSegmentDto } from "./dto/create-segment.dto";
import { CreateWalletWithIdDto } from "./dto/create-wallet-with-id.dto";
import { CreateWalletDto } from "./dto/create-wallet.dto";
import { FetchAllSegmentHistoryDto } from "./dto/fetch-all-segment-history.dto";
import { FetchAllSegmentDto } from "./dto/fetch-all-segment.dto";
import { FetchAllTokenHistoryGlobalDto } from "./dto/fetch-all-token-history-global.dto";
import { FetchAllWalletHistoryDto } from "./dto/fetch-all-wallet-history.dto";
import { FetchAllWalletDto } from "./dto/fetch-all-wallet.dto";
import { FetchSegmentHistoryDto } from "./dto/fetch-segment-history.dto";
import { FetchSegmentDto } from "./dto/fetch-segment.dto";
import { FetchTokenHistoryGlobalDto } from "./dto/fetch-token-history-global.dto";
import { FetchWalletHistoryDto } from "./dto/fetch-wallet-history.dto";
import { FetchWalletDto } from "./dto/fetch-wallet.dto";
import { UpdateSegmentDto } from "./dto/update-segment.dto";
import { UpdateWalletDto } from "./dto/update-wallet.dto";

@Injectable()
export class WalletService {
  types = [
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWallet",
      MsgCreateWallet
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWalletWithId",
      MsgCreateWalletWithId
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWallet",
      MsgFetchAllWallet
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletResponse",
      MsgFetchAllWalletResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWallet",
      MsgFetchGetWallet
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletResponse",
      MsgFetchGetWalletResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletHistory",
      MsgFetchAllWalletHistory
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletHistoryResponse",
      MsgFetchAllWalletHistoryResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletHistory",
      MsgFetchGetWalletHistory
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletHistoryResponse",
      MsgFetchGetWalletHistoryResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateWallet",
      MsgUpdateWallet
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegment",
      MsgCreateSegment
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegmentWithId",
      MsgCreateSegmentWithId
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegment",
      MsgFetchAllSegment
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentResponse",
      MsgFetchAllSegmentResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegment",
      MsgFetchGetSegment
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegmentResponse",
      MsgFetchGetSegmentResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentHistory",
      MsgFetchAllSegmentHistory
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentHistoryResponse",
      MsgFetchAllSegmentHistoryResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchSegmentHistory",
      MsgFetchSegmentHistory
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegmentHistoryResponse",
      MsgFetchGetSegmentHistoryResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateSegment",
      MsgUpdateSegment
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllTokenHistoryGlobal",
      MsgFetchAllTokenHistoryGlobal
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllTokenHistoryGlobalResponse",
      MsgFetchAllTokenHistoryGlobalResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetTokenHistoryGlobal",
      MsgFetchGetTokenHistoryGlobal
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetTokenHistoryGlobalResponse",
      MsgFetchGetTokenHistoryGlobalResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      MsgIdResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgEmptyResponse",
      MsgEmptyResponse
    ]
  ];
  registry = new Registry(<Iterable<[string, GeneratedType]>>this.types);

  constructor(private readonly blockchainService: BlockchainService) {
  }

  async createWallet(createWalletDto: CreateWalletDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWallet",
      value: createWalletDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async createWalletWithId(createWalletWithIdDto: CreateWalletWithIdDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWalletWithId",
      value: createWalletWithIdDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchAllWallet(findAllWalletDto: FetchAllWalletDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWallet",
      value: findAllWalletDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchWallet(findWalletDto: FetchWalletDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWallet",
      value: findWalletDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchAllWalletHistory(findAllWalletHistoryDto: FetchAllWalletHistoryDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletHistory",
      value: findAllWalletHistoryDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletHistoryResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchWalletHistory(findWalletHistoryDto: FetchWalletHistoryDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletHistory",
      value: findWalletHistoryDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletHistoryResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async updateWallet(updateWalletDto: UpdateWalletDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateWallet",
      value: updateWalletDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgEmptyResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async createSegment(createSegmentDto: CreateSegmentDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegment",
      value: createSegmentDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async createSegmentWithId(createSegmentWithIdDto: CreateSegmentWithIdDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegmentWithId",
      value: createSegmentWithIdDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchAllSegment(findAllSegmentDto: FetchAllSegmentDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegment",
      value: findAllSegmentDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchSegment(findSegmentDto: FetchSegmentDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegment",
      value: findSegmentDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegmentResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchAllSegmentHistory(findAllSegmentHistoryDto: FetchAllSegmentHistoryDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentHistory",
      value: findAllSegmentHistoryDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentHistoryResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchSegmentHistory(findSegmentHistoryDto: FetchSegmentHistoryDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchSegmentHistory",
      value: findSegmentHistoryDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegmentHistoryResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async updateSegment(updateSegmentDto: UpdateSegmentDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateSegment",
      value: updateSegmentDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgEmptyResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchAllTokenHistoryGlobal(
    findAllTokenHistoryGlobalDto: FetchAllTokenHistoryGlobalDto
  ) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllTokenHistoryGlobal",
      value: findAllTokenHistoryGlobalDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllTokenHistoryGlobalResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchTokenHistoryGlobal(findTokenHistoryGlobalDto: FetchTokenHistoryGlobalDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetTokenHistoryGlobal",
      value: findTokenHistoryGlobalDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetTokenHistoryGlobalResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }
}
