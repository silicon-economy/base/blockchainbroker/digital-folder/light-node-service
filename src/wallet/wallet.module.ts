// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Module } from "@nestjs/common";
import { WalletService } from "./wallet.service";
import { WalletController } from "./wallet.controller";
import { BlockchainService } from "../blockchain/blockchain.service";

@Module({
  controllers: [WalletController],
  providers: [WalletService, BlockchainService]
})
export class WalletModule {
}
