// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Test, TestingModule } from "@nestjs/testing";
import { WalletController } from "./wallet.controller";
import { WalletService } from "./wallet.service";
import { CreateWalletDto } from "./dto/create-wallet.dto";
import { CreateWalletWithIdDto } from "./dto/create-wallet-with-id.dto";
import { FetchAllWalletDto } from "./dto/fetch-all-wallet.dto";
import { FetchWalletDto } from "./dto/fetch-wallet.dto";
import { FetchAllWalletHistoryDto } from "./dto/fetch-all-wallet-history.dto";
import { FetchWalletHistoryDto } from "./dto/fetch-wallet-history.dto";
import { UpdateWalletDto } from "./dto/update-wallet.dto";
import { CreateSegmentDto } from "./dto/create-segment.dto";
import { CreateSegmentWithIdDto } from "./dto/create-segment-with-id.dto";
import { FetchAllSegmentDto } from "./dto/fetch-all-segment.dto";
import { FetchSegmentDto } from "./dto/fetch-segment.dto";
import { FetchAllSegmentHistoryDto } from "./dto/fetch-all-segment-history.dto";
import { FetchSegmentHistoryDto } from "./dto/fetch-segment-history.dto";
import { UpdateSegmentDto } from "./dto/update-segment.dto";
import { FetchAllTokenHistoryGlobalDto } from "./dto/fetch-all-token-history-global.dto";
import { FetchTokenHistoryGlobalDto } from "./dto/fetch-token-history-global.dto";
import { BlockchainService } from "../blockchain/blockchain.service";

describe("WalletController", () => {
  let controller: WalletController;
  let walletService: WalletService;
  const bcIdentity: string = "A4C656846C";
  const walletId: string = "A4C5234AA";
  const segmentId: string = "F4C5234CAA";

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WalletController],
      providers: [WalletService, BlockchainService]
    }).compile();

    walletService = module.get<WalletService>(WalletService);
    controller = module.get<WalletController>(WalletController);
  });

  it("should sign and broadcast creation of wallet", async() =>{
    const createWalletDto: CreateWalletDto = new CreateWalletDto();
    const spy = jest.spyOn(walletService, "createWallet")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    createWalletDto.creator=bcIdentity;
    createWalletDto.name="Test Wallet";
    await controller.createWallet(createWalletDto);
    expect(spy).toBeCalled();
  });

  it("should sign and broadcast creation of wallet with given ID", async() =>{
    const createWalletWithIdDto: CreateWalletWithIdDto = new CreateWalletWithIdDto();
    const spy = jest.spyOn(walletService, "createWalletWithId")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    createWalletWithIdDto.id=walletId;
    createWalletWithIdDto.creator=bcIdentity;
    createWalletWithIdDto.name="Test Wallet";
    await controller.createWalletWithId(createWalletWithIdDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all existing wallets", async() =>{
    const fetchAllWalletDto: FetchAllWalletDto = new FetchAllWalletDto();
    const spy = jest.spyOn(walletService, "fetchAllWallet")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    fetchAllWalletDto.creator=bcIdentity;
    await controller.fetchAllWallet(fetchAllWalletDto);
    expect(spy).toBeCalled();
  });

  it("should fetch an existing wallet by Id", async() =>{
    const findWalletDto: FetchWalletDto = new FetchWalletDto();
    const spy = jest.spyOn(walletService, "fetchWallet")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findWalletDto.creator=bcIdentity;
    findWalletDto.id=walletId;
    await controller.fetchWallet(findWalletDto);
    expect(spy).toBeCalled();
  });

  it("should fetch the transaction history of an existing wallet", async() =>{
    const findAllWalletHistoryDto: FetchAllWalletHistoryDto = new FetchAllWalletHistoryDto();
    const spy = jest.spyOn(walletService, "fetchAllWalletHistory")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findAllWalletHistoryDto.creator=bcIdentity;
    await controller.fetchAllWalletHistory(findAllWalletHistoryDto);
    expect(spy).toBeCalled();
  });

  it("should fetch a paginated transaction history of an existing wallet", async() =>{
    const findWalletHistoryDto: FetchWalletHistoryDto = new FetchWalletHistoryDto();
    const spy = jest.spyOn(walletService, "fetchWalletHistory")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findWalletHistoryDto.id=walletId;
    findWalletHistoryDto.creator=bcIdentity;
    await controller.fetchWalletHistory(findWalletHistoryDto);
    expect(spy).toBeCalled();
  });

  it("should update the wallet information", async() =>{
    const updateWalletDto: UpdateWalletDto = new UpdateWalletDto();
    const spy = jest.spyOn(walletService, "updateWallet")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    updateWalletDto.id=walletId;
    updateWalletDto.creator=bcIdentity;
    updateWalletDto.name="Test Wallet";
    await controller.updateWallet(updateWalletDto);
    expect(spy).toBeCalled();
  });

  it("should create a new segment within a given wallet", async() =>{
    const createSegmentDto: CreateSegmentDto = new CreateSegmentDto();
    const spy = jest.spyOn(walletService, "createSegment")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    createSegmentDto.creator=bcIdentity;
    createSegmentDto.name="Test Wallet";
    createSegmentDto.walletId=walletId;
    createSegmentDto.info="Some Information";
    await controller.createSegment(createSegmentDto);
    expect(spy).toBeCalled();
  });

  it("should create a new segment with a specified Id within a given wallet", async() =>{
    const createSegmentWithIdDto: CreateSegmentWithIdDto = new CreateSegmentWithIdDto();
    const spy = jest.spyOn(walletService, "createSegmentWithId")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    createSegmentWithIdDto.creator=bcIdentity;
    createSegmentWithIdDto.name="Test Wallet";
    createSegmentWithIdDto.walletId=walletId;
    createSegmentWithIdDto.info="Some Information";
    createSegmentWithIdDto.id=segmentId;
    await controller.createSegmentWithId(createSegmentWithIdDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all segments of a given blockchain identity", async() =>{
    const findAllSegmentDto: FetchAllSegmentDto = new FetchAllSegmentDto();
    const spy = jest.spyOn(walletService, "fetchAllSegment")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findAllSegmentDto.creator=bcIdentity;
    await controller.fetchAllSegment(findAllSegmentDto);
    expect(spy).toBeCalled();
  });

  it("should fetch segments of a given blockchain identity by Id", async() =>{
    const findSegmentDto: FetchSegmentDto = new FetchSegmentDto();
    const spy = jest.spyOn(walletService, "fetchSegment")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findSegmentDto.creator=bcIdentity;
    findSegmentDto.id=segmentId;
    await controller.fetchSegment(findSegmentDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all segment history of a blockchain identity", async() =>{
    const findAllSegmentHistoryDto: FetchAllSegmentHistoryDto = new FetchAllSegmentHistoryDto();
    const spy = jest.spyOn(walletService, "fetchAllSegmentHistory")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findAllSegmentHistoryDto.creator=bcIdentity;
    await controller.fetchAllSegmentHistory(findAllSegmentHistoryDto);
    expect(spy).toBeCalled();
  });

  it("should fetch the history from a given segment", async() =>{
    const findSegmentHistoryDto: FetchSegmentHistoryDto = new FetchSegmentHistoryDto();
    const spy = jest.spyOn(walletService, "fetchSegmentHistory")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findSegmentHistoryDto.creator=bcIdentity;
    findSegmentHistoryDto.id=segmentId;
    await controller.fetchSegmentHistory(findSegmentHistoryDto);
    expect(spy).toBeCalled();
  });

  it("should update information of a given segment", async() =>{
    const updateSegmentDto: UpdateSegmentDto = new UpdateSegmentDto();
    const spy = jest.spyOn(walletService, "updateSegment")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    updateSegmentDto.creator=bcIdentity;
    updateSegmentDto.id=segmentId;
    updateSegmentDto.name="Segement name";
    updateSegmentDto.info="Segment description"
    await controller.updateSegment(updateSegmentDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all tokens of a given blockchain identity with history", async() =>{
    const findAllTokenHistoryGlobalDto: FetchAllTokenHistoryGlobalDto = new FetchAllTokenHistoryGlobalDto();
    const spy = jest.spyOn(walletService, "fetchAllTokenHistoryGlobal")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findAllTokenHistoryGlobalDto.creator=bcIdentity;
    findAllTokenHistoryGlobalDto.id=segmentId;
    await controller.fetchAllTokenHistoryGlobal(findAllTokenHistoryGlobalDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all tokens of a given blockchain identity", async() =>{
    const findTokenHistoryGlobalDto: FetchTokenHistoryGlobalDto = new FetchTokenHistoryGlobalDto();
    const spy = jest.spyOn(walletService, "fetchTokenHistoryGlobal")
        .mockImplementation(() => Promise.resolve(new Uint8Array));
    findTokenHistoryGlobalDto.creator=bcIdentity;
    findTokenHistoryGlobalDto.id=segmentId;
    await controller.fetchTokenHistoryGlobal(findTokenHistoryGlobalDto);
    expect(spy).toBeCalled();
  });
});
