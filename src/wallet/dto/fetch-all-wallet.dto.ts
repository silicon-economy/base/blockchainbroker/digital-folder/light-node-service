// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { PageRequest } from "src/models/cosmos/pagination";

export class FetchAllWalletDto {
  creator: string;
  pagination: PageRequest;
}
