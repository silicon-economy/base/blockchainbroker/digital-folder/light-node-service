// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Module } from "@nestjs/common";
import { TokenModule } from "./token/token.module";
import { WalletModule } from "./wallet/wallet.module";
import { AuthorizationModule } from "./authorization/authorization.module";
import { ConfigModule } from "@nestjs/config";
import { BlockchainModule } from "./blockchain/blockchain.module";
import config from "./config/config";

@Module({
  imports: [
    TokenModule,
    WalletModule,
    AuthorizationModule,
    ConfigModule.forRoot({ isGlobal: true, load: [config] }),
    BlockchainModule
  ],
  controllers: [],
  providers: []
})
export class AppModule {
}
