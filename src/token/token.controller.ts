// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Controller } from "@nestjs/common";
import { MessagePattern, Payload } from "@nestjs/microservices";
import { TokenService } from "./token.service";
import { CreateTokenDto } from "./dto/create-token.dto";
import { UpdateTokenDto } from "./dto/update-token.dto";
import { ActivateTokenDto } from "./dto/activate-token.dto";
import { CreateHashTokenDto } from "./dto/create-hash-token.dto";
import { DeactivateTokenDto } from "./dto/deactive-token.dto";
import { FetchDocumentHashDto } from "./dto/fetch-document-hash.dto";
import {
  FetchTokenDto,
  FetchTokenHistoryDto,
  FetchTokensBySegmentIdDto,
  FetchTokensByWalletIdDto
} from "./dto/fetch-token.dto";
import { MoveTokenToWalletDto } from "./dto/move-token.dto";
import { UpdateTokenInformationDto } from "./dto/update-token-information.dto";
import { MsgFetchTokenHistoryResponse } from "../models/businesslogic/tx";

@Controller()
export class TokenController {
  constructor(private readonly tokenService: TokenService) {
  }

  @MessagePattern("createToken")
  createToken(@Payload() createTokenDto: CreateTokenDto) {
    return this.tokenService.createToken(createTokenDto);
  }

  @MessagePattern("fetchToken")
  fetchToken(@Payload() fetchTokenDto: FetchTokenDto) {
    return this.tokenService.fetchToken(fetchTokenDto);
  }

  @MessagePattern("fetchTokensByWalletId")
  fetchTokenByWalletId(@Payload() fetchTokensByWalletIdDto: FetchTokensByWalletIdDto) {
    return this.tokenService.fetchTokensByWalletId(fetchTokensByWalletIdDto);
  }

  @MessagePattern("fetchTokensBySegmentId")
  fetchTokensBySegmentId(@Payload() fetchTokensBySegmentIdDto: FetchTokensBySegmentIdDto) {
    return this.tokenService.fetchTokensBySegmentId(fetchTokensBySegmentIdDto);
  }

  @MessagePattern("fetchTokenHistory")
  fetchTokenHistory(@Payload() fetchTokenHistoryDto: FetchTokenHistoryDto): Promise<MsgFetchTokenHistoryResponse> {
    return this.tokenService.fetchTokenHistory(fetchTokenHistoryDto);
  }

  @MessagePattern("updateToken")
  updateToken(@Payload() updateTokenDto: UpdateTokenDto) {
    return this.tokenService.updateToken(updateTokenDto);
  }

  @MessagePattern("updateTokenInformation")
  updateTokenInformation(@Payload() updateTokenInformationDto: UpdateTokenInformationDto) {
    return this.tokenService.updateTokenInformation(updateTokenInformationDto);
  }

  @MessagePattern("activateToken")
  activateToken(@Payload() activateTokenDto: ActivateTokenDto) {
    return this.tokenService.activateToken(activateTokenDto);
  }

  @MessagePattern("deactivateToken")
  deactivateToken(@Payload() deactivateTokenDto: DeactivateTokenDto) {
    return this.tokenService.deactivateToken(deactivateTokenDto);
  }

  @MessagePattern("moveTokenToWallet")
  moveTokenToWallet(@Payload() moveTokenToWalletDto: MoveTokenToWalletDto) {
    return this.tokenService.moveTokenToWallet(moveTokenToWalletDto);
  }

  @MessagePattern("createHashToken")
  createHashToken(@Payload() createHashTokenDto: CreateHashTokenDto) {
    return this.tokenService.createHashToken(createHashTokenDto);
  }

  @MessagePattern("fetchDocumentHash")
  fetchDocumentHash(@Payload() fetchDocumentHashDto: FetchDocumentHashDto) {
    return this.tokenService.fetchDocumentHash(fetchDocumentHashDto);
  }
}
