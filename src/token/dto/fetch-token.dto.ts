// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

export class FetchTokensByWalletIdDto {
  Creator: string;
  Id: string;
}

export class FetchTokensBySegmentIdDto {
  Creator: string;
  Id: string;
}

export class FetchTokenDto {
  Creator: string;
  Id: string;
}

export class FetchTokenHistoryDto {
  Creator: string;
  Id: string;
}
