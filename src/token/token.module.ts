// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Module } from "@nestjs/common";
import { TokenService } from "./token.service";
import { TokenController } from "./token.controller";
import { BlockchainService } from "../blockchain/blockchain.service";

@Module({
  controllers: [TokenController],
  providers: [TokenService, BlockchainService]
})
export class TokenModule {
}
