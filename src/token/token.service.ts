// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { EncodeObject, GeneratedType, Registry } from "@cosmjs/proto-signing";
import { Injectable } from "@nestjs/common";
import { BlockchainService } from "../blockchain/blockchain.service";
import { QueryAllTokenResponse, QueryGetDocumentHashResponse } from "../models/businesslogic/query";
import {
  MsgActivateToken,
  MsgCreateHashToken,
  MsgCreateToken,
  MsgDeactivateToken,
  MsgEmptyResponse,
  MsgFetchDocumentHash,
  MsgFetchToken,
  MsgFetchTokenHistory,
  MsgFetchTokenHistoryResponse,
  MsgFetchTokenResponse,
  MsgFetchTokensBySegmentId,
  MsgFetchTokensByWalletId,
  MsgUpdateToken,
  MsgUpdateTokenInformation
} from "../models/businesslogic/tx";
import { MsgIdResponse, MsgMoveTokenToWallet } from "../models/wallet/tx";
import { ActivateTokenDto } from "./dto/activate-token.dto";
import { CreateHashTokenDto } from "./dto/create-hash-token.dto";
import { CreateTokenDto } from "./dto/create-token.dto";
import { DeactivateTokenDto } from "./dto/deactive-token.dto";
import { FetchDocumentHashDto } from "./dto/fetch-document-hash.dto";
import {
  FetchTokenDto,
  FetchTokenHistoryDto,
  FetchTokensBySegmentIdDto,
  FetchTokensByWalletIdDto
} from "./dto/fetch-token.dto";
import { MoveTokenToWalletDto } from "./dto/move-token.dto";
import { UpdateTokenInformationDto } from "./dto/update-token-information.dto";
import { UpdateTokenDto } from "./dto/update-token.dto";

@Injectable()
export class TokenService {
  types = [
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateToken",
      MsgCreateToken
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchToken",
      MsgFetchToken
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenResponse",
      MsgFetchTokenResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensByWalletId",
      MsgFetchTokensByWalletId
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensBySegmentId",
      MsgFetchTokensBySegmentId
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.QueryAllTokenResponse",
      QueryAllTokenResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenHistory",
      MsgFetchTokenHistory
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenHistoryResponse",
      MsgFetchTokenHistoryResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateToken",
      MsgUpdateToken
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateTokenInformation",
      MsgUpdateTokenInformation
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgActivateToken",
      MsgActivateToken
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeactivateToken",
      MsgDeactivateToken
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgMoveTokenToWallet",
      MsgMoveTokenToWallet
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateHashToken",
      MsgCreateHashToken
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchDocumentHash",
      MsgFetchDocumentHash
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.QueryGetDocumentHashResponse",
      QueryGetDocumentHashResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      MsgIdResponse
    ],
    [
      "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgEmptyResponse",
      MsgEmptyResponse
    ]
  ];

  registry = new Registry(<Iterable<[string, GeneratedType]>>this.types);

  constructor(private readonly blockchainService: BlockchainService) {
  }

  async createToken(createTokenDto: CreateTokenDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateToken",
      value: createTokenDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchToken(fetchTokenDto: FetchTokenDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchToken",
      value: fetchTokenDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchTokensByWalletId(fetchTokensByWalletIdDto: FetchTokensByWalletIdDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensByWalletId",
      value: fetchTokensByWalletIdDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.QueryAllTokenResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchTokensBySegmentId(fetchTokensBySegmentIdDto: FetchTokensBySegmentIdDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensBySegmentId",
      value: fetchTokensBySegmentIdDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.QueryAllTokenResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchTokenHistory(fetchTokenHistoryDto: FetchTokenHistoryDto): Promise<MsgFetchTokenHistoryResponse> {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenHistory",
      value: fetchTokenHistoryDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenHistoryResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async updateToken(updateTokenDto: UpdateTokenDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateToken",
      value: updateTokenDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  async updateTokenInformation(updateTokenInformationDto: UpdateTokenInformationDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateTokenInformation",
      value: updateTokenInformationDto
    };
    return this.blockchainService.signAndBroadcast(tx, this.registry);
  }

  async activateToken(activateTokenDto: ActivateTokenDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgActivateToken",
      value: activateTokenDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgEmptyResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async deactivateToken(deactivateTokenDto: DeactivateTokenDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeactivateToken",
      value: deactivateTokenDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgEmptyResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async moveTokenToWallet(moveTokenToWalletDto: MoveTokenToWalletDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgMoveTokenToWallet",
      value: moveTokenToWalletDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgEmptyResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async createHashToken(createHashTokenDto: CreateHashTokenDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateHashToken",
      value: createHashTokenDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgIdResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }

  async fetchDocumentHash(fetchDocumentHashDto: FetchDocumentHashDto) {
    const tx: EncodeObject = {
      typeUrl:
        "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchDocumentHash",
      value: fetchDocumentHashDto
    };
    return this.registry.decode({
      typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.QueryGetDocumentHashResponse",
      value: await this.blockchainService.signAndBroadcast(tx, this.registry)
    });
  }
}
