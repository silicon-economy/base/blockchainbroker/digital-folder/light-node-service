// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import { Test, TestingModule } from "@nestjs/testing";
import { TokenController } from "./token.controller";
import { TokenService } from "./token.service";
import { BlockchainService } from "../blockchain/blockchain.service";
import { CreateTokenDto } from "./dto/create-token.dto";
import {
  FetchTokenDto,
  FetchTokenHistoryDto,
  FetchTokensBySegmentIdDto,
  FetchTokensByWalletIdDto
} from "./dto/fetch-token.dto";
import { UpdateTokenDto } from "./dto/update-token.dto";
import { UpdateTokenInformationDto } from "./dto/update-token-information.dto";
import { ActivateTokenDto } from "./dto/activate-token.dto";
import { DeactivateTokenDto } from "./dto/deactive-token.dto";
import { MoveTokenToWalletDto } from "./dto/move-token.dto";
import { CreateHashTokenDto } from "./dto/create-hash-token.dto";
import { FetchDocumentHashDto } from "./dto/fetch-document-hash.dto";

describe("TokenController", () => {
  let controller: TokenController;
  let tokenService: TokenService;
  const bcIdentity: string = "A4C656846C";
  const walletId: string = "A4C5234AA";
  const segmentId: string = "F4C5234CAA";
  const tokenId: string = "F4C5234CAA";


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TokenController],
      providers: [TokenService, BlockchainService]
    }).compile();

    tokenService = module.get<TokenService>(TokenService);
    controller = module.get<TokenController>(TokenController);
  });

  it("should sign and broadcast creation of a token", async() =>{
    const createTokenDto: CreateTokenDto = new CreateTokenDto();
    const spy = jest.spyOn(tokenService, "createToken")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    createTokenDto.creator=bcIdentity;
    createTokenDto.segmentId=segmentId;
    createTokenDto.changeMessage="created";
    await controller.createToken(createTokenDto);
    expect(spy).toBeCalled();
  });

  it("should fetch a token by identifier", async() =>{
    const fetchTokenDto: FetchTokenDto = new FetchTokenDto();
    const spy = jest.spyOn(tokenService, "fetchToken")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchTokenDto.Creator=bcIdentity;
    fetchTokenDto.Id=tokenId;
    await controller.fetchToken(fetchTokenDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all token by wallet identifier", async() =>{
    const fetchTokensByWalletIdDto: FetchTokensByWalletIdDto = new FetchTokensByWalletIdDto();
    const spy = jest.spyOn(tokenService, "fetchTokensByWalletId")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchTokensByWalletIdDto.Creator=bcIdentity;
    fetchTokensByWalletIdDto.Id=walletId;
    await controller.fetchTokenByWalletId(fetchTokensByWalletIdDto);
    expect(spy).toBeCalled();
  });

  it("should fetch all token by segment identifier", async() =>{
    const fetchTokensBySegmentIdDto: FetchTokensBySegmentIdDto = new FetchTokensBySegmentIdDto();
    const spy = jest.spyOn(tokenService, "fetchTokensBySegmentId")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchTokensBySegmentIdDto.Creator=bcIdentity;
    fetchTokensBySegmentIdDto.Id=segmentId;
    await controller.fetchTokensBySegmentId(fetchTokensBySegmentIdDto);
    expect(spy).toBeCalled();
  });

  it("should fetch history of token by token identifier", async() =>{
    const fetchTokenHistoryDto: FetchTokenHistoryDto = new FetchTokenHistoryDto();
    const spy = jest.spyOn(tokenService, "fetchTokenHistory")
        .mockImplementation(() => Promise.resolve(null));

    fetchTokenHistoryDto.Creator=bcIdentity;
    fetchTokenHistoryDto.Id=segmentId;
    await controller.fetchTokenHistory(fetchTokenHistoryDto);
    expect(spy).toBeCalled();
  });

  it("should update token metadata by token identifier", async() =>{
    const updateTokenDto: UpdateTokenDto = new UpdateTokenDto();
    const spy = jest.spyOn(tokenService, "updateToken")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    updateTokenDto.creator=bcIdentity;
    updateTokenDto.segmentId=segmentId;
    updateTokenDto.changeMessage="Update token";
    updateTokenDto.tokenRefId=tokenId;
    updateTokenDto.tokenType="Token";
    await controller.updateToken(updateTokenDto);
    expect(spy).toBeCalled();
  });

  it("should update token information by token identifier", async() =>{
    const updateTokenInformationDto: UpdateTokenInformationDto = new UpdateTokenInformationDto();
    const spy = jest.spyOn(tokenService, "updateTokenInformation")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    updateTokenInformationDto.tokenId=tokenId;
    updateTokenInformationDto.data="New token data";
    await controller.updateTokenInformation(updateTokenInformationDto);
    expect(spy).toBeCalled();
  });

  it("should set the status of a token to active by token identifier", async() =>{
    const activateTokenDto: ActivateTokenDto = new ActivateTokenDto();
    const spy = jest.spyOn(tokenService, "activateToken")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    activateTokenDto.creator=bcIdentity;
    activateTokenDto.segmentId=segmentId;
    activateTokenDto.id=tokenId;
    await controller.activateToken(activateTokenDto);
    expect(spy).toBeCalled();
  });

  it("should set the status of a token to inactive by token identifier", async() =>{
    const deactivateTokenDto: DeactivateTokenDto = new DeactivateTokenDto();
    const spy = jest.spyOn(tokenService, "deactivateToken")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    deactivateTokenDto.creator=bcIdentity;
    deactivateTokenDto.id=tokenId;
    await controller.deactivateToken(deactivateTokenDto);
    expect(spy).toBeCalled();
  });

  it("should move a token between two segments by token identifier", async() =>{
    const moveTokenToWalletDto: MoveTokenToWalletDto = new MoveTokenToWalletDto();
    const spy = jest.spyOn(tokenService, "moveTokenToWallet")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    moveTokenToWalletDto.creator=bcIdentity;
    moveTokenToWalletDto.tokenRefId=tokenId;
    moveTokenToWalletDto.sourceSegmentId=segmentId;
    moveTokenToWalletDto.targetSegmentId=segmentId;
    await controller.moveTokenToWallet(moveTokenToWalletDto);
    expect(spy).toBeCalled();
  });

  it("should create a new hash-token ", async() =>{
    const createHashTokenDto: CreateHashTokenDto = new CreateHashTokenDto();
    const spy = jest.spyOn(tokenService, "createHashToken")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    createHashTokenDto.creator=bcIdentity;
    createHashTokenDto.changeMessage="create";
    createHashTokenDto.segmentId=segmentId;
    createHashTokenDto.hash="A234890712BCDEF123";
    createHashTokenDto.hashFunction="SHA256";
    createHashTokenDto.document="document.pdf";
    createHashTokenDto.metadata="Metadata";
    await controller.createHashToken(createHashTokenDto);
    expect(spy).toBeCalled();
  });

  it("should fetch a hash value by document identifier", async() =>{
    const fetchDocumentHashDto: FetchDocumentHashDto = new FetchDocumentHashDto();
    const spy = jest.spyOn(tokenService, "fetchDocumentHash")
        .mockImplementation(() => Promise.resolve(new Uint8Array));

    fetchDocumentHashDto.creator=bcIdentity;
    fetchDocumentHashDto.id="A32344FFE";
    await controller.fetchDocumentHash(fetchDocumentHashDto);
    expect(spy).toBeCalled();
  });
});
